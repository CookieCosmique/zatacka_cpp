/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivContainer.cpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:03:32 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivContainer.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Div/DivFactory.hpp"
#include "Game/Div/DivGroup.hpp"
#include "Game/Div/DivSet.hpp"
#include "Utility/FlatBox.hpp"

using namespace agl;

template <typename T>
DivContainer<T>::DivContainer(const Vec2 &dimensions, DivFactory<T> *factory) : AbstractDivContainer(dimensions)
//DivContainer<T>::DivContainer(const Vec2 &dimensions)
{
	_factory = factory;
	_dimensions = dimensions;
	_format = FlatBox(Vec2(0.0,0.0), _dimensions);
}

template <typename T>
DivContainer<T>::~DivContainer(void)
{
	if(_factory)
		delete _factory;
}

/*template <typename T>
Vec2	DivContainer<T>::getDimensions(void) const
{
	return (_dimensions);
}

template <typename T>
FlatBox	DivContainer<T>::getFormat(void) const
{
	return (_format);
}

template <typename T>
FlatBox	DivContainer<T>::getZone(void) const
{
	return (_zone);
}*/

template <typename T>
DivGroup<T>	DivContainer<T>::getGroup(const FlatBox &box)
{
	return getGroup(box, getAnchor());
}

template <typename T>
DivGroup<T>	DivContainer<T>::getGroup(const FlatBox &box, const DivIterator<T> &anchor)
{
	FlatBox	new_box = box % _format;
	return (DivGroup<T>(*this, new_box, getAnchor(new_box.getDownLeft(), anchor), getAnchor(new_box.getUpRight(), anchor)));
}

template <typename T>
DivSet<T>	*DivContainer<T>::getEmptySet(DivToDo<T> *toDoNew, DivToDo<T> *toDoOld)
{
	return (new DivSet<T>(*this, toDoNew, toDoOld));
}

template <typename T>
DivSet<T>	*DivContainer<T>::getNewSet(DivToDo<T> *toDoNew, DivToDo<T> *toDoOld, const FlatBox &zone)
{
	return (new DivSet<T>(*this, toDoNew, toDoOld, zone));
}

template class DivContainer<Div>;
template class DivContainer<ChunkDiv>;
