/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   RectangleCollider.cpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:57:51 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:57:51 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Collider/RectangleCollider.hpp"

RectangleCollider::RectangleCollider(const Rectangle &t) : _t(t)
{
}

RectangleCollider::~RectangleCollider(void)
{
}

/*void		TriangleCollider::collide(const Polygon &p) const
{
	collision::collision(this->_t, p);
}

void		TriangleCollider::collide(const Circle &c) const
{
	collision::collision(this->_t, c);
}*/

bool		RectangleCollider::collide(const Rectangle &t) const
{
	return (collision::collision(this->_t, t));
}

bool		RectangleCollider::collide(const Triangle &t) const
{
	return (collision::collision(this->_t, t));
}

bool		RectangleCollider::collide(const EmptyShape &t) const
{
	return (collision::collision(this->_t, t));
}


