/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LineShaderProgram.cpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-24 21:27:38 by Bastien             |\________\          */
/*   Updated: 2016/06/14 17:41:23 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OpenGL/ShaderProgram/LineShaderProgram.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

LineShaderProgram::LineShaderProgram(void) throw(InvalidShaderProgramException) :
		CameraShaderProgram(LINE_VERTEX, LINE_GEOMETRY, LINE_FRAGMENT)
{

}

LineShaderProgram::~LineShaderProgram(void)
{
}

// Print

void		LineShaderProgram::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "LineShaderProgram" << C_RESET << ": {" << logger::tab();
	CameraShaderProgram::objectPrint(o);
	o << logger::tab(-1) << "}";
}
