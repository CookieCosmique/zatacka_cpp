/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TextComponent.cpp                               |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-18 17:22:50 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:56:13 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Scene/Interface/TextComponent.hpp"
#include "Area.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

TextComponent::TextComponent(Scene &scene, Area *area, const FontTexture &font,
		TextShaderProgram &shaderProgram,
		const std::string &str, const int color, const int emphasis,
		const Vec2 &position, const Vec2 &size,
		const PositionType pos_type, const PositionType size_type,
		const int level) :
	SceneComponent(scene, area, position, size, pos_type, size_type),
	Renderable(scene.getRender(), scene.getCamera(), level),
	_font(font), _shaderProgram(shaderProgram),
	_emphasis(emphasis), _color(color)
{
	this->_vao = new TextVAO(this->_shaderProgram.getID(), this->_font, str,
			color, this->_emphasis);
	this->_size = this->_vao->getSize() * this->_size.y;
	this->setPosition(position, pos_type);
	logger::sslog(LOG_DEBUG) << *this << logger::endlog();
}

TextComponent::~TextComponent(void)
{
	delete this->_vao;
}

// Member Functions

void		TextComponent::onModif(void)
{

}

void		TextComponent::render(void) const
{
	this->_shaderProgram.use();
	this->_shaderProgram.updateFont(this->_font);
	this->_shaderProgram.updateCamera(this->_camera);
	this->_shaderProgram.updatePosition(this->_position);
	this->_shaderProgram.updateSize(this->_size.y);
	this->_shaderProgram.updateEmphasis(this->_emphasis);
	this->_vao->render();
}

void		TextComponent::setString(const std::string &str)
{
	this->_vao->setString(str, this->_color, this->_emphasis);
}

// Print

void		TextComponent::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "TextComponent" << C_RESET << ": {" << logger::tab(1);
	SceneComponent::objectPrint(o);
	o << ",\n";
	Renderable::objectPrint(o);
	o << ",\n";
	o << C_VAR << "shader program" << C_RESET << " = " << this->_shaderProgram << ",\n";
	o << C_VAR << "vao" << C_RESET << " = " << *this->_vao;
	o << logger::tab(-1) << "}";
}
