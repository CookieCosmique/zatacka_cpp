/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   MemIterator.hpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:08:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MEMITERATOR_HPP
# define MEMITERATOR_HPP

# include "Utility/SmartArray.hpp"
# include "Vec2.hpp"

class LinesContainer;

class MemIterator
{
private:
	bool				_valid;
	size_t 				_index;
	SmartArray<float>	*_posData;
	SmartArray<int>		*_numData;

	LinesContainer		*_container;
public:
	MemIterator(void);
	MemIterator(size_t index, SmartArray<float> &posData, SmartArray<int> &numData, LinesContainer &container);
	MemIterator(const MemIterator &ite);

	MemIterator	&move(const agl::Vec2 &newPos);
	MemIterator	&set(const agl::Vec2 &newPos, int newNum);
	MemIterator	&set(const MemIterator &ite);

	agl::Vec2	getPos(void) const;
	int			getNum(void) const;
	size_t		getIndex(void) const;
	bool		isValid(void) const;

	void	free(void);

	MemIterator	&operator=(const MemIterator &ite);
};

#endif
