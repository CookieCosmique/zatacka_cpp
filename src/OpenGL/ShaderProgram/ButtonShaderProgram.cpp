/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ButtonShaderProgram.cpp                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-24 21:27:38 by Bastien             |\________\          */
/*   Updated: 2016/06/14 17:41:05 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OpenGL/ShaderProgram/ButtonShaderProgram.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

ButtonShaderProgram::ButtonShaderProgram(void) throw(InvalidShaderProgramException) :
		CameraShaderProgram(BUTTON_VERTEX, BUTTON_GEOMETRY, BUTTON_FRAGMENT)
{
	this->_id_statut = glGetUniformLocation(this->_id, "statut");
}

ButtonShaderProgram::~ButtonShaderProgram(void)
{
}

// Public functions

void	ButtonShaderProgram::setStatut(const int statut) const
{
	glUniform1i(this->_id_statut, statut);
}

// Print

void	ButtonShaderProgram::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "ButtonShaderProgram" << C_RESET << ": {" << logger::tab(1);
	CameraShaderProgram::objectPrint(o);
	o << ",\n";
	o << C_VAR << "id_statut" << C_RESET << " = " << this->_id_statut;
	o << logger::tab(-1) << "}";
}
