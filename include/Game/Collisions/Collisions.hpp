/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Collisions.hpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:20:28 by Bastien             |\________\          */
/*   Updated: 2016-03-22 21:20:28 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLLISIONS_HPP
# define COLLISIONS_HPP

# include "Game/Collisions/Shape/Shape.hpp"

namespace collision
{
//	bool	collision(const Polygon &, const Polygon &);
//	bool	collision(const Polygon &, const Circle &);
//	bool	collision(const Polygon &, const Triangle &);
//	bool	collision(const Circle &, const Polygon &);
//	bool	collision(const Circle &, const Circle &);
//	bool	collision(const Circle &, const Triangle &);
//	bool	collision(const Triangle &, const Polygon &);
//	bool	collision(const Triangle &, const Circle &);
	bool	collision(const Triangle &, const Triangle &);
	bool	collision(const Triangle &, const Rectangle &);
	bool	collision(const Triangle &, const EmptyShape &);

	bool	collision(const Rectangle &, const Triangle &);
	bool	collision(const Rectangle &, const Rectangle &);
	bool	collision(const Rectangle &, const EmptyShape &);

	bool	collision(const EmptyShape &, const Triangle &);
	bool	collision(const EmptyShape &, const Rectangle &);
	bool	collision(const EmptyShape &, const EmptyShape &);

	bool	collide(const Shape &a,const Shape &b);
}

#endif
