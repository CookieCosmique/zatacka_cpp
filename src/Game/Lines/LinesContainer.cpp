/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LinesContainer.cpp                              |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:15:14 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Lines/LinesContainer.hpp"
#include "Game/Lines/Lines.hpp"
#include "Game/Div/ChunkDiv.hpp"

using namespace agl;

LinesContainer::LinesContainer(Game &game, Camera &camera, ChunkDiv &div,
			const LineShaderProgram &shader_program) :
		Renderable(game.getRender(), camera, 1),
		_shaderProgram(shader_program),
		_div(div),
		_mem(*this)
{
	this->_vao = new LineVAO(this->_shaderProgram.getID(), this->_mem);
}

LinesContainer::~LinesContainer(void)
{
	while (!this->_allLines.empty())
	{
		if (this->_allLines.front())
			delete (this->_allLines.front());	//dont need to pop it, the lines remember its position and remove itself
		else
			this->_allLines.pop_front();
	}	

	delete this->_vao;
}

Lines	*LinesContainer::newLines(const Vec2 &start, const Vec2 &end, int num, void *arrow)
{
	return (new Lines(this, start, end, num, arrow));
}

Lines	*LinesContainer::newLines(const Vec2 &outerStart, const Vec2 &start, const Vec2 &end, int num, void *arrow)
{
	return (new Lines(this, outerStart, start, end, num, arrow));
}

Lines	*LinesContainer::newLines(const Vec2 &newPos, const PreviousLines &oldLines)
{
	if( oldLines.getLines()->getContainer() == this)
		return (new Lines(this, newPos, oldLines));
	else
		return (newLines(oldLines.getLines()->getEnd(), newPos, oldLines.getLines()->getNum(), oldLines.getLines()->getParentKey()));
}

Lines	*LinesContainer::newLines(const Vec2 &newPos, const NextLines &nextLines)
{
	if( nextLines.getLines()->getContainer() == this)
		return (new Lines(this, newPos, nextLines));
	else
		return (newLines(newPos, nextLines.getLines()->getStart(), nextLines.getLines()->getNum(), nextLines.getLines()->getParentKey()));
}

LinesMemory			&LinesContainer::getMemory(void)
{
	return (_mem);
}

std::list<Lines*>	*LinesContainer::getAllLines(void)
{
	return (&_allLines);
}

ChunkDiv			&LinesContainer::getDiv(void) const
{
	return (_div);
}

void				LinesContainer::deleteLines(std::list<Lines*>::iterator &ite)
{
	if(*ite)
		delete (*ite);
}

void				LinesContainer::updateVAO(void)
{
	this->_vao->updateAll(_mem);
}

void				LinesContainer::updatePos(void)
{
	this->_vao->updatePos(_mem);
}

void				LinesContainer::updateNum(void)
{
	this->_vao->updateNum(_mem);
}

void				LinesContainer::updateIndex(void)
{
	this->_vao->updateIndex(_mem);
}

void				LinesContainer::render(void) const
{
	this->_shaderProgram.use();
	this->_shaderProgram.updateCamera(this->_camera);
//	this->_vao->updateAll(this->_mem);
//	this->_vao->updatePos(this->_mem);
//	this->_vao->updateNum(this->_mem);
//	this->_vao->updateIndex(this->_mem);
	this->_vao->render();
}

void				LinesContainer::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "LinesContainer" << C_RESET << "{" << logger::tab(1);
	o << C_VAR << "_mem" << C_RESET << " : " << this->_mem << logger::tab(-1) << "}";
}
