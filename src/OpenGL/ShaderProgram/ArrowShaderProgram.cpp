/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ArrowShaderProgram.cpp                          |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-21 13:09:44 by Bastien             |\________\          */
/*   Updated: 2016/06/14 17:40:49 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OpenGL/ShaderProgram/ArrowShaderProgram.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

ArrowShaderProgram::ArrowShaderProgram(void) throw(InvalidShaderProgramException) :
		CameraShaderProgram(ARROW_VERTEX, ARROW_GEOMETRY, ARROW_FRAGMENT)
{
	logger::sslog(LOG_FINE) << "Initializing shader program uniforms.." << logger::endlog(1);
	this->_id_color = glGetUniformLocation(this->_id, "color");
	this->_id_speed = glGetUniformLocation(this->_id, "speed");
	this->_id_size = glGetUniformLocation(this->_id, "size");
	this->_id_skin = glGetUniformLocation(this->_id, "skin");
	logger::sslog(LOG_FINE, -1) << "Uniforms initialized" << logger::endlog();
}

ArrowShaderProgram::~ArrowShaderProgram(void)
{
}

// Public functions

void		ArrowShaderProgram::updateColor(const int color) const
{
	glUniform3f(this->_id_color, ((color & 0xFF0000) >> 16) / 255.0f,
								 ((color & 0x00FF00) >> 8) / 255.0f,
								 ((color & 0x0000FF) >> 0) / 255.0f);
}

void		ArrowShaderProgram::updateSpeed(const float speed) const
{
	glUniform1f(this->_id_speed, speed);
}

void		ArrowShaderProgram::updateSize(const float size) const
{
	glUniform1f(this->_id_size, size);
}

void		ArrowShaderProgram::updateSkin(const int skin) const
{
	glUniform1i(this->_id_skin, skin);
}

// Print

void		ArrowShaderProgram::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "ArrowShaderProgram" << C_RESET << ": {" << logger::tab();
	CameraShaderProgram::objectPrint(o);
	o << "\n";
	o << C_VAR << "id_color" << C_RESET << " = " << this->_id_color << ",\n";
	o << C_VAR << "id_speed" << C_RESET << " = " << this->_id_speed << ",\n";
	o << C_VAR << "id_size" << C_RESET << " = " << this->_id_size << ",\n";
	o << C_VAR << "id_skin" << C_RESET << " = " << this->_id_skin << "\n";
	o << logger::tab(-1) << "}";
}
