/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToCollideStrech.hpp                          |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:14:05 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVTOCOLLIDESTRECH_HPP
# define DIVTOCOLLIDESTRECH_HPP

# include "Game/Div/DivToDo/DivToDo.hpp"
# include "Vec2.hpp"

class ChunkDiv;
class ColliderComponent;

class DivToCollideStrech : public DivToDo<ChunkDiv>
{
private:
	ColliderComponent	&_component;
	agl::Vec2			_delta;
public:
	DivToCollideStrech(ColliderComponent &component, const agl::Vec2 &delta);
	virtual	~DivToCollideStrech(void) {};

	virtual void 	doTo(ChunkDiv &div);
};

#endif





