#version 400 core

out vec4 out_color;

uniform int	statut;

void	main(void)
{
	float	color;
	if (statut == 0)
		color = 0.75f;
	else if (statut == 1)
		color = 0.30f;
	else if (statut == 2)
		color = 0.80f;
	out_color = vec4(color, color, color, 1.0f);
}
