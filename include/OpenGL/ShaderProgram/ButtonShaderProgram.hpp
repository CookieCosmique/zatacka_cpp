/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ButtonShaderProgram.hpp                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-12 19:57:09 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:25:44 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUTTON_SHADER_PROGRAM_HPP
# define BUTTON_SHADER_PROGRAM_HPP

# include "Camera.hpp"
# include "CameraShaderProgram.hpp"

# define BUTTON_VERTEX "shaders/button/vertex.glsl"
# define BUTTON_GEOMETRY "shaders/button/geometry.glsl"
# define BUTTON_FRAGMENT "shaders/button/fragment.glsl"

class	ButtonShaderProgram : public agl::CameraShaderProgram
{
private:
	GLuint		_id_statut;

public:
	ButtonShaderProgram(void) throw(InvalidShaderProgramException);
	~ButtonShaderProgram(void);

	void	setStatut(const int statut) const;

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
