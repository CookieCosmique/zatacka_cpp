/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LinesContainer.hpp                              |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:54:29 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINESCONTAINER_HPP
# define LINESCONTAINER_HPP

# include "Renderable.hpp"
# include "Camera.hpp"
# include "Game/Lines/LinesMemory/LinesMemory.hpp"
# include "OpenGL/ShaderProgram/LineShaderProgram.hpp"
# include "OpenGL/VAO/LineVAO.hpp"
# include <list>

class Lines;
class Game;
class PreviousLines;
class NextLines;
class ChunkDiv;

///everytime one of the _mem smart array is modified, i should update the multiple buffer object (vbo and ebo) -> 
///_mem ref to container and call the function ? (refresh veo 1,2 /refresh ebo)

class LinesContainer : public agl::Renderable
{
private:
	const LineShaderProgram		&_shaderProgram;
	LineVAO						*_vao;
	ChunkDiv					&_div;
	LinesMemory					_mem;
	std::list<Lines*>			_allLines;

public:
	LinesContainer(Game &game, agl::Camera &camera, ChunkDiv &div,
			const LineShaderProgram &shader_program);
	~LinesContainer(void);

	Lines	*newLines(const agl::Vec2 &start, const agl::Vec2 &end, int num,
			void *parent);
	Lines	*newLines(const agl::Vec2 &outerStart, const agl::Vec2 &start,
			const agl::Vec2 &end, int num, void *parent);
	Lines	*newLines(const agl::Vec2 &newPos, const PreviousLines &oldLines);
	Lines	*newLines(const agl::Vec2 &newPos, const NextLines &nextLines);

	LinesMemory			&getMemory(void);
	std::list<Lines*>	*getAllLines(void);
	ChunkDiv			&getDiv(void) const;

	void				deleteLines(std::list<Lines*>::iterator &ite);

	void				updateVAO(void);
	void				updatePos(void);
	void				updateNum(void);
	void				updateIndex(void);

	void				render(void) const;

	void				objectPrint(std::ostream &o) const;

};

#endif
