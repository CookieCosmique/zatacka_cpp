/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Actor.hpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef ACTOR_HPP
# define ACTOR_HPP

class ColliderComponent;

class Actor
{
public:
	virtual void onCollision(ColliderComponent &other) {(void) other;}
};

#endif