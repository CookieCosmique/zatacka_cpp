/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ManualCamera.hpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 03:39:04 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:22:49 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MANUAL_CAMERA_HPP
# define MANUAL_CAMERA_HPP

# include "Camera.hpp"
# include "Updatable.hpp"
# include "KeyboardControllable.hpp"

class	ManualCamera;

class	ManualCamera : public agl::Camera, public agl::Updatable,
	agl::KeyboardControllable<ManualCamera>
{
private:
	enum	e_state
	{
		MOVE_UP = 1,
		MOVE_DOWN = 2,
		MOVE_LEFT = 4,
		MOVE_RIGHT = 8,
		ZOOM_IN = 16,
		ZOOM_OUT = 32
	};
	const agl::Vec2	_defaultPosition;
	const agl::Vec2	_defaultSize;
	float			_speed;
	int				_state;

public:
	ManualCamera(const agl::Vec2 &position, const agl::Vec2 &size, float speed,
			agl::Inputs &inputs, agl::Update &update);
	~ManualCamera(void);

	virtual void	update(const agl::GameTime &game_time);

	void			startMoveUp(void);
	void			startMoveDown(void);
	void			startMoveLeft(void);
	void			startMoveRight(void);
	void			startZoomIn(void);
	void			startZoomOut(void);

	void			stopMoveUp(void);
	void			stopMoveDown(void);
	void			stopMoveLeft(void);
	void			stopMoveRight(void);
	void			stopZoomIn(void);
	void			stopZoomOut(void);

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
