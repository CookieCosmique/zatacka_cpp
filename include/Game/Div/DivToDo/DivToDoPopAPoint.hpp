/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoPopAPoint.hpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:00:51 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVTODOPOPAPOINT_HPP
# define DIVTODOPOPAPOINT_HPP

#include "Game/Div/DivToDo/DivToDo.hpp"
#include "Vec2.hpp"

class Arrow;
class ChunkDiv;

class DivToDoPopAPoint: public DivToDo<ChunkDiv>
{
private:
	Arrow	&_arrow;
	agl::Vec2	_newPos;
public:
	DivToDoPopAPoint(const agl::Vec2 &newPos, Arrow &arrow);
	virtual	~DivToDoPopAPoint(void) {};

	virtual void 	doTo(ChunkDiv &div);
};

#endif



