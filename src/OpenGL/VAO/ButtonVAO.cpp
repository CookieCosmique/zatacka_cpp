/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ButtonVAO.cpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-12 21:52:19 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:24:49 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OpenGL/VAO/ButtonVAO.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

ButtonVAO::ButtonVAO(const GLuint program_id, const Vec2 &position,
			const Vec2 &size) :
	SquareVAO(program_id, position, size)
{

}

ButtonVAO::~ButtonVAO(void)
{

}

// Print

void		ButtonVAO::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "ButtonVAO" << C_RESET << ": {" << logger::tab(1);
	SquareVAO::objectPrint(o);
	o << logger::tab(-1) << "}";
}
