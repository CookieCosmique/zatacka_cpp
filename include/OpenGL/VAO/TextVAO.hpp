/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TextVAO.hpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 18:02:49 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:26:18 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXT_VAO_HPP
# define TEXT_VAO_HPP

# include "VAO.hpp"
# include "Vec2.hpp"
# include "FontTexture.hpp"

class	TextVAO : public agl::VAO
{
private:
	const agl::FontTexture	&_font;
	std::string			_str;
	GLfloat				*_ids;
	GLfloat				*_colors;
	GLfloat				*_coordinates;
	GLfloat				*_char_x;
	GLuint				*_indices;
	agl::Vec2			_size;

	GLuint				_ebo;
	GLuint				_vbo_ids;
	GLuint				_vbo_coordinates;
	GLuint				_vbo_char_x;
	GLuint				_vbo_colors;

	void				delete_arrays(void);
	void				create_arrays(const unsigned int size);
	void				resize_arrays(const unsigned int size);
	void				update_buffers(void) const;

public:
	TextVAO(const GLuint program_id, const agl::FontTexture &font,
		const std::string &str, const int color, const int emphasis = 0);
	~TextVAO(void);

	virtual void		render(void) const;

	void				setString(const std::string &str, const int color, const int emphasis = 0);
	void				setColor(const float r, const float g, const float b);
	void				setColor(const int color);
	const agl::Vec2		&getSize(void) const;
	const std::string	&getString(void) const;

	virtual void		objectPrint(std::ostream &o) const;
};

#endif
