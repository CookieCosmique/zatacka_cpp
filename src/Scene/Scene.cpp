/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Scene.cpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-06 13:20:08 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:00:22 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Scene/Scene.hpp"
#include "Scene/Update.hpp"
#include "Scene/Render.hpp"
#include "Game/GameTime.hpp"
#include "Scene/Camera.hpp"
#include "Utility/Logger.hpp"

// Constructors - Destructors

Scene::Scene(GLFWwindow *window) : _window(window)
{
	this->_update = new Update();
	this->_render = new Render();
	this->_time = new GameTime();
	this->_sceneCamera = new Camera(Vec2(0, 0), Vec2(1280, 720));
	this->_running = false;
}

Scene::Scene(GLFWwindow *window, Update *update, Render *render) : _window(window)
{
	if (!(this->_update = update))
		this->_update = new Update();
	if (!(this->_render = render))
		this->_render = new Render();
	this->_sceneCamera = new Camera(Vec2(0, 0), Vec2(1280, 720));
	this->_running = false;
}

Scene::~Scene(void)
{
	delete this->_update;
	delete this->_render;
	delete this->_time;
	this->_sceneCamera->kill();
}

// Member Functions

int			Scene::run(void)
{
	this->_running = true;
	this->_time->reset();
	while (this->_running)
	{
		glfwPollEvents();
		this->_time->update();
		this->_update->update(*(this->_time));
		this->_render->render(this->_window);
	}
	return (0);
}

void		Scene::stop(void)
{
	this->_running = false;
}

// Accessors

bool		Scene::isRunning(void) const
{
	return (this->_running);
}

Update		&Scene::getUpdate(void) 
{
	return (*(this->_update));
}

Render		&Scene::getRender(void)
{
	return (*(this->_render));
}

GameTime	&Scene::getTime(void)
{
	return (*(this->_time));
}

Camera		&Scene::getCamera(void)
{
	return (*(this->_sceneCamera));
}

// Print

void		Scene::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Scene" << C_RESET << ": {" << logger::tab(1);
	o << C_VAR << "running" << C_RESET << " = " << this->_running << ",\n";
	o << C_VAR << "update" << C_RESET << " = " << *(this->_update) << ",\n";
	o << C_VAR << "render" << C_RESET << " = " << *(this->_render) << ",\n";
	o << C_VAR << "time" << C_RESET << " = " << this->_time << ",\n";
	o << C_VAR << "camera" << C_RESET << " = " << this->_sceneCamera << ",\n";
	o << logger::tab(-1) << "}";
}
