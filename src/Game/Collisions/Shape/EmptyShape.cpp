/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   EmptyShape.hpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:11:08 by Bastien             |\________\          */
/*   Updated: 2016-03-22 21:11:08 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Shape/EmptyShape.hpp"

EmptyShape::EmptyShape(void) : _collider(*this)
{
}

EmptyShape::~EmptyShape(void)
{
}

const Collider		&EmptyShape::getCollider(void) const
{
	return (this->_collider);
}

bool				EmptyShape::collide(const Collider &c) const
{
	return (c.collide(*this));
}