/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Updatable.cpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Utility/Spawnable.hpp"
#include "Scene/Component/Updatable.hpp"
#include "Scene/Update.hpp"


// Constructor - Destructor

Updatable::Updatable(Update &update, const int level)
{
	update.addUpdatable(this, level);
}

Updatable::~Updatable(void)
{

}

// Print

void		Updatable::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Updatable" << C_RESET << ": ";
	Spawnable::objectPrint(o);
}
