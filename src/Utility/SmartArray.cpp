/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SmartArray.cpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/04 16:51:14 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.hpp"
#include "Utility/SmartArray.hpp"

// Exceptions 

template <typename T>
SmartArray<T>::OverSizedException::OverSizedException(size_t size, size_t maxSize)
{
	_size = size;
	_maxSize = maxSize;
}

template <typename T>
const char		*SmartArray<T>::OverSizedException::what(void) const throw()
{
	std::stringstream	ss;

	ss << "Tried to build a SmartArray of size " << this->_size << " when max size is : " << this->_maxSize;
	return (ss.str().c_str());
}

template <typename T>
SmartArray<T>::WrongParametersNumberException::WrongParametersNumberException(size_t size, size_t tried)
{
	_size = size;
	_tried = tried;
}

template <typename T>
const char		*SmartArray<T>::WrongParametersNumberException::what(void) const throw()
{
	std::stringstream	ss;

	ss << "Tried add " << this->_tried << " new vals when the tab size is : " << this->_size;
	return (ss.str().c_str());
}

// Contructor and Destructor 

template <typename T>
SmartArray<T>::SmartArray(size_t size, T (*initializer) (void)) throw(OverSizedException)
{
	if(size > MAX_SIZE)
		throw(OverSizedException(size, MAX_SIZE));
	_size = size;
	_offset = 0;
	_maxSize = 0;
	_data = NULL;
	_indexStack.push(0);
	_initializer = initializer;
}

template <typename T>
SmartArray<T>::SmartArray(size_t size, size_t offset, T (*initializer) (void)) throw(OverSizedException) : SmartArray(size, initializer)
{
	size_t	i;

	_offset = offset;

	if(_offset > 0)
		_data = new T[size * _offset];

	if(_initializer)
	{
		i = 0;
		while(i < (size * _offset))
		{
			_data[i] = _initializer();
			i++;
		}
	}
}

template <typename T>
SmartArray<T>::~SmartArray(void)
{
	if (_data)
		delete[] _data;
}

//Functions 

template <typename T>
size_t	SmartArray<T>::newVal(const T *newData)
{
	size_t	newIndex;
	size_t	oldMaxSize;
	size_t	i;	
	T	*tmpData;

	newIndex = _indexStack.top();
	_indexStack.pop();
	if (_indexStack.empty())
		_indexStack.push(newIndex + 1);

	if (newIndex >= _maxSize)
	{
		oldMaxSize = _maxSize;
		if(_maxSize == 0)
			_maxSize = 1;
		else
			_maxSize = 2 * _maxSize;

		tmpData = new T[(_offset + _maxSize) * _size];

		i = 0;
		while (i < (oldMaxSize * _size))
		{
			tmpData[(_offset * _size) + i] = _data[(_offset * _size) + i]; 
			i++;
		}
		if(_initializer)
		{
			while (i < (_maxSize * _size))
			{
				tmpData[(_offset * _size) + i] = _initializer();
				i++;
			}
		}

		if (_data)
			delete[] _data;
		_data = tmpData;
	}

	i = 0;
	while( i < _size)
	{
		_data[(newIndex + _offset) * _size + i] = newData[i];
		i++;
	}

	return (newIndex);
}

template <typename T>
size_t	SmartArray<T>::newValTmp(const T **newData)
{
	size_t 	newIndex;
	size_t	oldMaxSize;
	size_t	i;	
	T	*tmpData;

	newIndex = _indexStack.top();
	_indexStack.pop();
	if (_indexStack.empty())
		_indexStack.push(newIndex + 1);

	if (newIndex >= _maxSize)
	{
		oldMaxSize = _maxSize;
		if(_maxSize == 0)
			_maxSize = 1;
		else
			_maxSize = 2 * _maxSize;

		tmpData = new T[(_offset + _maxSize) * _size];

		i = 0;
		while (i < (oldMaxSize * _size))
		{
			tmpData[(_offset * _size) + i] = _data[(_offset * _size) + i]; 
			i++;
		}
		if(_initializer)
		{
			while (i < (_maxSize * _size))
			{
				tmpData[(_offset * _size) + i] = _initializer();
				i++;
			}
		}

		if (_data)
			delete[] _data;
		_data = tmpData;
	}

	i = 0;
	while( i < _size)
	{
		_data[(newIndex + _offset) * _size + i] = (*newData[i]);
		i++;
	}

	return (newIndex);
}

template <typename T>
size_t	SmartArray<T>::newVal(const T &one) throw(WrongParametersNumberException)
{
	const T *temp[1];

	if(_size != 1)
		throw(WrongParametersNumberException(_size, 1));

	temp[0] = &one;

	return (newValTmp(temp));
}

template <typename T>
size_t	SmartArray<T>::newVal(const T &one, const T &two) throw(WrongParametersNumberException)
{
	const T *temp[2];

	if(_size != 2)
		throw(WrongParametersNumberException(_size, 2));

	temp[0] = &one;
	temp[1] = &two;

	return (newValTmp(temp));
}

template <typename T>
size_t	SmartArray<T>::newVal(const T &one, const T &two, const T &three) throw(WrongParametersNumberException)
{
	const T *temp[3];

	if(_size != 3)
		throw(WrongParametersNumberException(_size, 3));

	temp[0] = &one;
	temp[1] = &two;
	temp[2] = &three;

	return (newValTmp(temp));
}

template <typename T>
size_t	SmartArray<T>::newVal(const T &one, const T &two, const T &three, const T &four) throw(WrongParametersNumberException)
{
	const T *temp[4];

	if(_size != 4)
		throw(WrongParametersNumberException(_size, 4));

	temp[0] = &one;
	temp[1] = &two;
	temp[2] = &three;
	temp[3] = &four;

	return (newValTmp(temp));
}

template <typename T>
size_t	SmartArray<T>::getSize(void) const
{
	return (_size);
}

template <typename T>
size_t	SmartArray<T>::getOffset(void) const
{
	return (_offset);
}

template <typename T>
size_t	SmartArray<T>::getMaxSize(void) const
{
	return (_maxSize);
}

template <typename T>
size_t	SmartArray<T>::getSizeData(void) const
{
	return ((_maxSize + _offset) * _size);
}

template <typename T>
void	SmartArray<T>::freeVal(size_t index)
{
	size_t	i;

	_indexStack.push(index);
	
	if(_initializer)
	{
		i = 0;
		while (i < _size)
		{
			_data[i + ((index + _offset) * _size)] = _initializer();
			i++;
		}
	}
}

template <typename T>
T	*SmartArray<T>::getData(void) const
{
	return (_data);
}

template <typename T>
T	*SmartArray<T>::operator[](size_t i) const
{
	return (_data + ((i + _offset) * _size));
}

template <typename T>
void	SmartArray<T>::objectPrint(std::ostream &o) const
{
	size_t	i;
	size_t	j;

	o << C_OBJ << "SmartArray" << C_RESET << " : " << C_VAR << "size" << C_RESET << " : " << this->_size << " ; "
	<< C_VAR << "maxsize" << C_RESET << " : " << this->_maxSize << " ; "
	<< C_VAR << "offset" << C_RESET << " : " << this->_offset;
	o << "\n content : \n";
	i = 0;
	while (i < this->_maxSize)
	{
		o << "{";
		j = 0;
		while(j < this->_size)
		{
			o << this->_data[j + ((this->_offset + i) * this->_size)];
			if(j < this->_size - 1)
				o << ", ";
			j++;
		}
		o << "} ";
		i++;
	}
}

template class SmartArray<float>;
template class SmartArray<int>;
template class SmartArray<GLuint>;

//initializer function

int		zeroInt(void)
{
	return (0);
}

GLuint	zeroGLuint(void)
{
	return (0);
}

float	zeroFloat(void)
{
	return (0);
}
