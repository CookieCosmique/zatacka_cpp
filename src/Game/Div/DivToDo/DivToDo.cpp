/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDo.hpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivToDo/DivToDo.hpp"
#include "Game/Div/DivGroup.hpp"
#include "Game/Div/GroupDivIterator.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"

template <typename T>
void	DivToDo<T>::forEach(DivGroup<T> grp)
{
	if(!grp.isEmpty())
	{
		GroupDivIterator<T>	ite = grp.start();
		bool				keep = true;

		while (keep == true)
		{
			doTo(*ite);

			if(!ite++)
				keep = false;
		}
	}
}

template class DivToDo<Div>;
template class DivToDo<ChunkDiv>;