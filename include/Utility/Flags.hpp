/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Flags.hpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-15 21:36:58 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:20:54 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLAGS_HPP
# define FLAGS_HPP

# include <map>
# include "Object.hpp"

/** \brief Calculate Flags from char **arg
*
*	The constructor take de ac/av main
*	parameter and set the private variable "_flags"
*	to the desired value (a bitmask of the flags)
*/
class Flags : public agl::Object
{
private:
	std::map<char, int>		_flagsList; /**< \brief a list of all the flags */
	int						_flags; /**< \brief the sum of the flags (bit mask)*/
	/**	\brief Called if an uknown flag is found
	*
	*	The function will call log on LOG_WARNING mode
	*	\param c the character of this uknown flag
	*	\param path the path to where this flag was called
	*/
	void	printUnknownFlag(const char c, const char *path) const;

	/** \brief Used to add flags to _flags member
	*
	*	\param s is the list of flag to treat ( of form -abcd )
	*	\param path	is the path to where those flags come from
	*/
	void	addFlags(const char *s, const char *path);

public:
	/**	\brief Flag constructor
	*	\param ac the main ac number of argument
	*	\param av char chains of size \c ac
	*/
	Flags(int ac, char **av);
	/** \brief A simple message printer destructor
	*/
	virtual	~Flags(void);
	/**	\brief Flag comparison
	*
	*	Return \c true if flag is an active flag, \c false otherwise
	*	\param flag the flag to look up in all_flag
	*/
	bool 	hasFlag(int flag) const;

	/** \brief Print the object */
	virtual void	objectPrint(std::ostream &o) const;

	/** \brief enum of every flags that can be used*/
	enum			e_flags
	{
		F_LOG_DEBUG = 1,		/**< Print debug messages */
		F_LOG_TIME = 2,			/**< Print date before messages */
		F_ANTI_ALIASING = 4,	/**< Enable GLFW's anti-aliasing */
		F_FIXED_FPS = 8			/**< Fix fps */
	};
};

#endif
