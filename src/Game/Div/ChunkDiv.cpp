/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ChunkDiv.cpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:06:59 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/ChunkDiv.hpp"
#include "Game/Entity/Arrow.hpp"
#include "Game/Game.hpp"
#include "Game/Lines/LinesContainer.hpp"
#include "Game/Lines/Lines.hpp"
#include "Game/Collisions/ColliderComponent/ColliderComponent.hpp"

using namespace agl;

//ChunkDiv::ChunkDiv(DivContainer<ChunkDiv> &container, const Vec2 &downLeft) : Div(container, downLeft)
ChunkDiv::ChunkDiv(Game &game, const FlatBox &zone) : Div(zone), _game(game)
{
	_linesCont = new LinesContainer(game, game.getGameCamera(), *this, this->_game.getLineShaderProgram());
}

ChunkDiv::~ChunkDiv(void)
{
	_linesCont->kill();
}

void	ChunkDiv::addArrow(Arrow *arrow)
{
	_arrows[(void *) arrow] = arrow;
	addColliderComponent(arrow->getColliderComponent());
//	_arrowsLines[(void *) arrow] = _linesCont.newLines()
}

void	ChunkDiv::removeArrow(Arrow *arrow)
{
	_arrows.erase(arrow);
	removeColliderComponent(arrow->getColliderComponent());
}

void	ChunkDiv::removeArrowLines(Arrow *arrow)
{
	_arrowsLines.erase(arrow);
}

void	ChunkDiv::removePotentialLines(Lines *lines)
{
	logger::sslog(LOG_DEBUG) << "Remove potential lines" << logger::endlog();
	if(_arrowsLines.count(lines->getParentKey()))
	{
		logger::sslog(LOG_DEBUG) << "getParentKey" << logger::endlog();
		if(_arrowsLines[lines->getParentKey()] == lines)
			_arrowsLines.erase(lines->getParentKey());
	}
}

void	ChunkDiv::addColliderComponent(ColliderComponent &other)
{
	_components.emplace(&other, other);
}

void	ChunkDiv::removeColliderComponent(ColliderComponent &other)
{
	_components.erase(&other);
}

Lines	*ChunkDiv::newLines(const Vec2 &newPos, Arrow *arrow)
{
	if(_arrowsLines.count(arrow))
	{
		_arrowsLines[arrow] = _linesCont->newLines(newPos, PreviousLines(_arrowsLines[arrow]));
		return (_arrowsLines[arrow]);
	}
	else
	{
		return (this->newLines(newPos, newPos, arrow));
	}
}

Lines	*ChunkDiv::newLines(const Vec2 &oldPos, const Vec2 &newPos, Arrow *arrow)
{
	_arrowsLines[arrow] = _linesCont->newLines(oldPos, newPos, arrow->getNum(), arrow);
	return (_arrowsLines[arrow]);
}

Lines	*ChunkDiv::newLines(const Vec2 &veryOldPos, const Vec2 &oldPos, const Vec2 &newPos, Arrow *arrow)
{
	_arrowsLines[arrow] = _linesCont->newLines(veryOldPos, oldPos, newPos, arrow->getNum(), arrow);
	return (_arrowsLines[arrow]);
}

bool	ChunkDiv::moveLines(const Vec2 & newPos, Arrow *arrow)
{
	if(_arrowsLines.count(arrow))
	{
		_arrowsLines[arrow]->moveEnd(newPos);
		return (true);
	}
	else
		return (false);
}

bool	ChunkDiv::resetTimer(Arrow *arrow)
{
	if(_arrowsLines.count(arrow))
	{
		_arrowsLines[arrow]->resetTimer();
		return (true);
	}
	else
		return (false);
}

void	ChunkDiv::collide(ColliderComponent &component)
{
	std::map< void*, ColliderComponent& >::iterator ite = _components.begin();
	while (ite != _components.end())
	{
		collideComponent(component, ite->second);
		ite++;
	}
}

void	ChunkDiv::collideStrech(ColliderComponent &component, const Vec2 &delta)
{
	std::map< void*, ColliderComponent& >::iterator ite = _components.begin();
	while (ite != _components.end())
	{
		component.collideComponentStretch(ite->second, delta);
		ite++;
	}
}

Game	&ChunkDiv::getGame(void) const
{
	return (_game);
}

void	ChunkDiv::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "ChunkDiv" << C_RESET << " : " << " {" << logger::tab(1);
	o << C_VAR << "nbr players" << C_RESET << ": " << _arrows.size() << ",\n";
	o << C_VAR << "lines container" << C_RESET << ": " << (*_linesCont) << ",\n";
	Div::objectPrint(o);
	o << logger::tab(-1) << "}";
}
