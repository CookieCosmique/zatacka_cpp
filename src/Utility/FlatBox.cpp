/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   FlatBox.cpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:32:40 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Utility/FlatBox.hpp"
#include "Vec2.hpp"
#include "Logger.hpp"

using namespace agl;

//Constructors

FlatBox::FlatBox() : _downLeft(0.0, 0.0), _upRight(0.0, 0.0)
{
}

FlatBox::FlatBox(const Vec2 &downLeft,const Vec2 &upRight) : _downLeft(downLeft), _upRight(upRight)
{
}

FlatBox::FlatBox(const FlatBox &other) : _downLeft(other._downLeft), _upRight(other._upRight)
{
}

//Getters and Setters

bool	FlatBox::isEmpty(void) const
{	
	if (_downLeft.x > _upRight.x || _downLeft.y > _upRight.y)
		return true;
	else if ((_downLeft.x == _upRight.x) || (_downLeft.y == _upRight.y))
		return true;
	return false;
}

Vec2	FlatBox::getDownLeft(void) const
{
	return _downLeft;
}

Vec2	FlatBox::getUpRight(void) const
{
	return _upRight;
}

Vec2	FlatBox::getDimensions(void) const
{
	return (_upRight - _downLeft);
}

void	FlatBox::setDownLeft(const Vec2 &downLeft)
{
	_downLeft = downLeft;
}
void	FlatBox::setUpRight(const Vec2 &upRight)
{
	_upRight = upRight;
}

//Operators

FlatBox	FlatBox::operator|(const FlatBox &other) const
{
	FlatBox	res;

	if (this->isEmpty())
		res = other;
	else if (other.isEmpty())
		res = *this;
	else
	{
		if (other._downLeft.y < _downLeft.y)
			res._downLeft.setY(other._downLeft.y);
		else
			res._downLeft.setY(_downLeft.y);

		if (other._downLeft.x < _downLeft.x)
			res._downLeft.setX(other._downLeft.x);
		else
			res._downLeft.setX(_downLeft.x);

		if (other._upRight.y > _upRight.y)
			res._upRight.setY(other._upRight.y);
		else
			res._upRight.setY(_upRight.y);

		if (other._upRight.x > _upRight.x)
			res._upRight.setX(other._upRight.x);
		else
			res._upRight.setX(_upRight.x);
	}


	return res;
}

FlatBox	FlatBox::operator&(const FlatBox &other) const
{
	FlatBox	res;

	if (!this->isEmpty() && !other.isEmpty())
	{
		if (other._downLeft.y > _downLeft.y)
			res._downLeft.setY(other._downLeft.y);
		else
			res._downLeft.setY(_downLeft.y);

		if (other._downLeft.x > _downLeft.x)
			res._downLeft.setX(other._downLeft.x);
		else
			res._downLeft.setX(_downLeft.x);

		if (other._upRight.y < _upRight.y)
			res._upRight.setY(other._upRight.y);
		else
			res._upRight.setY(_upRight.y);

		if (other._upRight.x < _upRight.x)
			res._upRight.setX(other._upRight.x);
		else
			res._upRight.setX(_upRight.x);
	}

	if(res.isEmpty())
		res = FlatBox();

	return res;
}

FlatBox FlatBox::operator%(const FlatBox &format) const
{
	FlatBox	res;
	Vec2	dim;

	if (format.isEmpty())
	{
		return (*this);
	}
	else if (isEmpty())
	{
		return FlatBox();
	}
	else
	{
		dim = format.getDimensions();
		res._downLeft = ( (_downLeft - format._downLeft) % dim) + format._downLeft;
		res._upRight = ( - ((- _upRight + format._downLeft) % dim)) + format._downLeft;

		return res;
	}
}

bool	FlatBox::operator&&(const FlatBox &other) const
{
	/*if (other._downLeft.x > _upRight.x || other._downLeft.y > _upRight.y)
		return false;
	if (_downLeft.x > other._upRight.x || _downLeft.y > other._upRight.y)
		return false;
	if(*this == other)
		return false;*/

	return (!(*this & other).isEmpty());
}

bool	FlatBox::operator==(const FlatBox &other) const
{
	return (_downLeft == other._downLeft && _upRight == other._upRight);
}

FlatBox	&FlatBox::operator=(const FlatBox &other)
{
	_downLeft = other._downLeft;
	_upRight = other._upRight;

	return (*this);
}

FlatBox	&FlatBox::operator|=(const FlatBox &other)
{
	FlatBox tmp = *this | other;
	_downLeft = tmp._downLeft;
	_upRight = tmp._upRight;

	return (*this);
}

FlatBox	&FlatBox::operator&=(const FlatBox &other)
{
	FlatBox tmp = *this & other;
	_downLeft = tmp._downLeft;
	_upRight = tmp._upRight;

	return (*this);
}

FlatBox	operator+(const Vec2 &vec, const FlatBox &flat)
{
	return FlatBox(flat._downLeft + vec, flat._upRight + vec);
}
FlatBox	operator+(const FlatBox &flat, const Vec2 &vec)
{
	return FlatBox(flat._downLeft + vec, flat._upRight + vec);
}
FlatBox	operator-(const FlatBox &flat, const Vec2 &vec)
{
	return FlatBox(flat._downLeft - vec, flat._upRight - vec);
}

FlatBox	&FlatBox::operator+=(const Vec2&vec)
{
	_downLeft += vec;
	_upRight += vec;

	return (*this);
}
FlatBox	&FlatBox::operator-=(const Vec2&vec)
{
	_downLeft -= vec;
	_upRight -= vec;

	return (*this);
}

//Operator << for the logger

std::ostream	&operator<<(std::ostream &o, const FlatBox &flat)
{
	o << C_OBJ << "Flatbox" << C_RESET << " : { " << 
	C_VAR << "down left" << C_RESET << " : " << flat._downLeft << ", " <<
	C_VAR << "up right " << C_RESET << ": " << flat._upRight << "}";
	return o;
}
