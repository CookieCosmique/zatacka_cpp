/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Menu.cpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 09:43:35 by Bastien             |\________\          */
/*   Updated: 2016/06/14 15:11:51 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Environment.hpp"
#include "Menu/Menu.hpp"
#include "Scene/Interface/Button.hpp"

using namespace agl;

// Constructor - Destructor

Menu::Menu(Environment &env) :
	Scene(env.getWindow()),
	KeyboardControllable(env.getInputs(), *this)
{
	linkInput(GLFW_KEY_ESCAPE, GLFW_PRESS, std::string("StopMenu"), &Menu::stop);
	this->_mainArea = new MenuMainArea(*this, env.getInputs());
	this->_logo = new TexturedComponent(*this, NULL, "assets/Zotocko.bmp",
			Vec2(0.5f, 0.85f), Vec2(200, 200),
			SceneComponent::PositionType::POS_RATIO);
	try
	{
		this->_background = new TexturedComponent(*this, NULL,
				"assets/background.bmp", this->_sceneCamera->getPosition(),
				this->_sceneCamera->getSize(),
				SceneComponent::PositionType::POS_NORMAL,
				SceneComponent::PositionType::POS_NORMAL, 0);
	}
	catch (std::exception &e)
	{
		logger::sslog(LOG_ERROR) << e.what() << logger::endlog();
	}
	this->_returnValue = 0;
}

Menu::~Menu(void)
{
	logger::sslog(LOG_DEBUG) << "Delete menu" << logger::endlog();
	this->_mainArea->kill();
	this->_background->kill();
	this->_logo->kill();
	unlinkInput(GLFW_KEY_ESCAPE, GLFW_PRESS, std::string("StopMenu"));
	logger::sslog(LOG_DEBUG) << "Menu deleted" << logger::endlog();
}

// Member Functions

int		Menu::run(void)
{
	Scene::run();
	return (this->_returnValue);
}

void	Menu::play(void)
{
	this->stop();
	this->_returnValue = 1;
}

void	Menu::options(void)
{

}

void	Menu::quit(void)
{
	Scene::stop();
	this->_returnValue = 0;
}

// Print

void	Menu::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Menu" << C_RESET << ": {" << logger::tab(1);
	Scene::objectPrint(o);
	o << ",\n";
	KeyboardControllable<Menu>::objectPrint(o);
	o << logger::tab(-1) << "}";
}
