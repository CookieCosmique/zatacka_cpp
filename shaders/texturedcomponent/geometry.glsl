#version 400 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec2			vUV[];

out vec2		uv;

uniform vec2	cam_size;
uniform vec2	cam_pos;

void	emit_points(vec2 pos, vec2 in_uv)
{
	uv = in_uv;
	gl_Position = vec4((pos.x - cam_pos.x) / cam_size.x * 2 - 1.0f,
						(pos.y - cam_pos.y) / cam_size.y * 2 - 1.0f, 0.0f, 1.0f);
	EmitVertex();
}

void	main(void)
{
	for (int i = 0; i < 3; i++)
		emit_points(gl_in[i].gl_Position.xy, vUV[i]);
	EndPrimitive();
}
