/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TestEntity.hpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-30 22:14:30 by Bastien             |\________\          */
/*   Updated: 2016-03-30 22:14:30 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_ENTITY_HPP
# define TEST_ENTITY_HPP

# include "Game/Entity/Entity.hpp"

class	TestEntity : public Entity
{
private:
	const float		_life;
	float			_compt;
	
public:
	TestEntity(Game &game, Camera &camera, const float life);
	~TestEntity(void);

	virtual void	render(void) const;
	virtual void	update(const GameTime &game_time);

	/** \brief Print object */
	virtual void	objectPrint(std::ostream &o) const;
};

#endif
