#version 400 core

layout(location = 1) in vec2	position;
layout(location = 2) in vec2	direction;

out vec2	out_dir;

void	main(void)
{
	out_dir = direction;
	gl_Position = vec4(position, 0.0f, 1.0f);
}