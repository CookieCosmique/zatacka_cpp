/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivIterator.hpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivIterator.hpp"
#include "Game/Div/DivTypeIterator.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"

template <typename T>
DivIterator<T>::DivIterator(DivTypeIterator<T> *typeIterator)
{
	_typeIterator = typeIterator;
}

template <typename T>
DivIterator<T>::DivIterator(const DivIterator<T> &other)
{
	_typeIterator = other._typeIterator->copy();
}

template <typename T>
DivIterator<T>::~DivIterator(void)
{
	if (_typeIterator)
		delete _typeIterator;
}

template <typename T>
void	DivIterator<T>::right(void)
{
	if(_typeIterator)
		_typeIterator->right();
}

template <typename T>
void	DivIterator<T>::left(void)
{
	if(_typeIterator)
		_typeIterator->left();
}

template <typename T>
void	DivIterator<T>::up(void)
{
	if(_typeIterator)
		_typeIterator->up();
}

template <typename T>
void	DivIterator<T>::down(void)
{
	if(_typeIterator)
		_typeIterator->down();
}

template <typename T>
void	DivIterator<T>::rightN(const int n)
{
	if(_typeIterator)
		_typeIterator->rightN(n);
}

template <typename T>
void	DivIterator<T>::leftN(const int n)
{
	if(_typeIterator)
		_typeIterator->leftN(n);
}

template <typename T>
void	DivIterator<T>::upN(const int n)
{
	if(_typeIterator)
		_typeIterator->upN(n);
}

template <typename T>
void	DivIterator<T>::downN(const int n)
{
	if(_typeIterator)
		_typeIterator->downN(n);
}

template <typename T>
T		&DivIterator<T>::operator*(void)
{
	return (_typeIterator->getDiv());
}

template class DivIterator<Div>;
template class DivIterator<ChunkDiv>;