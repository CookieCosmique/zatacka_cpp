/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Polygon.hpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:11:24 by Bastien             |\________\          */
/*   Updated: 2016-03-22 21:11:24 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef POLYGON_HPP
# define POLYGON_HPP

# include "Game/Collisions/Shape/Shape.hpp"
# include "Game/Collisions/Collider/PolygonCollider.hpp"

class	Polygon : public Shape
{
private:
	const PolygonCollider	_collider;
public:
	Polygon(void);
	virtual	~Polygon(void);
	virtual const Collider	&getCollider(void) const;
	virtual void			collide(const Collider &c) const;
};

#endif
