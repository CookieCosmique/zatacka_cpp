/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Logger.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 01:18:14 by Bastien             |\________\          */
/*   Updated: 2016/04/28 17:39:50 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iomanip>
#include "Utility/Logger.hpp"
//#include "Environment/Environment.hpp"

// Static variable definition

const Logger::t_log_format		Logger::_format[5] = {
		{"ERROR", C_BRED},
		{"FINE", C_GREEN},
		{"INFO", ""},
		{"WARNING", C_YELLOW},
		{"DEBUG", C_CYAN}
};

// Constructors - Destructors

Logger::Logger(void) : _max_tabs(8)
{
	this->_flags = 0;
	this->_tabs = 0;
}

Logger::Logger(const int flags) : _max_tabs(8)
{
	this->_flags = flags;
	this->_tabs = 0;
}

Logger::~Logger(void)
{
}

// Member Functions

bool		Logger::hasFlag(const int flag) const
{
	return (this->_flags & flag);
}

std::string	Logger::getTimeString(void) const
{
	std::time_t	t = std::time(0);
	char		cstr[16];
	std::strftime(cstr, sizeof(cstr), "%I:%M:%S", std::localtime(&t));
	return (cstr);
}

void		Logger::print(const int type, const std::string &s) const
{
	std::string				str;
	std::stringstream		type_str;
	std::stringstream		ss;
	std::string::size_type	pos;
	int						i;

	if (hasFlag(F_LOG_TIME))
		ss << "[" << getTimeString() << "] ";
	type_str << "[" << this->_format[type].str << "]";
	ss << this->_format[type].color << std::setw(11) << std::setfill(' ') << std::left << type_str.str() << C_RESET;
	i = 0;
	while (i < this->currTab())
	{
		ss << "-    ";
		++i;
	}
	ss << s << C_RESET;
	str = ss.str();
	if ((pos = str.find_first_of('\n')) != std::string::npos)
	{
		std::cout << str.substr(0, pos) << std::endl;
		print(type, str.substr(pos + 1));
	}
	else
		std::cout << str << std::endl;
}

int			Logger::currTab(void) const
{
	if (this->_tabs > this->_max_tabs)
		return (this->_max_tabs);
	return (this->_tabs);
}

int			Logger::log(const int type, const std::string &s, const int tab)
{
	if ((type != LOG_DEBUG) || (this->_flags & F_LOG_DEBUG))
		this->print(type, s);
	this->addTab(tab);
	return (type);
}

void		Logger::addTab(const int tab)
{
	this->_tabs += tab;
	if (this->_tabs < 0)
		this->_tabs = 0;
}

void		Logger::setTab(const int tab)
{
	this->_tabs = tab;
	if (this->_tabs < 0)
		this->_tabs = 0;
}

void		Logger::addFlag(const int flag)
{
	this->_flags |= flag;
}

void		Logger::removeFlag(const int flag)
{
	this->_flags &= ~flag;
}

void		Logger::setFlags(const int flags)
{
	this->_flags = flags;
}

std::ostringstream	&Logger::sslog(const int type, const int tab)
{
	_bufferType = type;
	_bufferStream.str("");
	addTab(tab);
	return (_bufferStream);
}

Logger::EndLog		Logger::endlog(const int tab)
{
	return (EndLog(*this, tab));
}

Logger::TabLog		Logger::tablog(const int tab)
{
	return (TabLog(*this, tab));
}

int					Logger::getBufferType(void) const
{
	return (this->_bufferType);
}

std::ostringstream	&Logger::getBufferStream(void)
{
	return (this->_bufferStream);
}

std::ostream		&operator<<(std::ostream &o, Logger &logger)
{
	o << C_OBJ << "Logger" << C_RESET << ": {flags = " << logger._flags << ", tabs = " << logger._tabs;
	o << ", max_tabs = " << logger._max_tabs << "}";
	return (o);
}


// EndLog Class

Logger::EndLog::EndLog(Logger &logger, const int tab) :
		_logger(logger),
		_tab(tab)
{}

Logger	&Logger::EndLog::getLogger(void) const
{
	return (this->_logger);
}

int		Logger::EndLog::getTab(void) const
{
	return (this->_tab);
}

// TabLog Class

Logger::TabLog::TabLog(Logger &logger, const int tab) :
		_logger(logger),
		_tab(tab)
{}

Logger	&Logger::TabLog::getLogger(void) const
{
	return (this->_logger);
}

int		Logger::TabLog::getTab(void) const
{
	return (this->_tab);
}

// Operator overload

void				operator<<(const std::ostream&, const Logger::EndLog &el)
{
	el.getLogger().log(el.getLogger().getBufferType(), el.getLogger().getBufferStream().str(),el.getTab());
}

std::ostringstream	&operator<<(const std::ostream &os, const Logger::TabLog &el)
{
	os << el.getLogger().endlog(0);
	return (el.getLogger().sslog(el.getLogger().getBufferType(), el.getTab()));
}

// Logger namespace

namespace logger
{
	Logger		logger;
	
	int						log(const int type, const std::string &s, const int _tab)
	{
		return (logger.log(type, s, _tab));
	}
	
	std::ostringstream		&sslog(const int type, const int _tab)
	{
		return (logger.sslog(type, _tab));
	}

	Logger::TabLog			tab(const int _tab)
	{
		return (logger.tablog(_tab));
	}
	
	Logger::EndLog			endlog(const int _tab)
	{
		return (logger.endlog(_tab));
	}
	
	int						getBufferType(void)
	{
		return (logger.getBufferType());
	}
	
	std::ostringstream		&getBufferStream(void)
	{
		return (logger.getBufferStream());
	}
}
