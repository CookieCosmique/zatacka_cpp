/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Collisions.cpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:08:44 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:01:39 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Collisions.hpp"
#include "Logger.hpp"

using namespace agl;

/*bool	collision::collision(const Polygon &, const Polygon &)
{
	logger::sslog(LOG_DEBUG) << "Polygon - Polygon" << logger::endlog();
}

bool	collision::collision(const Polygon &, const Circle &)
{
	logger::sslog(LOG_DEBUG) << "Polygon - Cicle" << logger::endlog();
}

bool	collision::collision(const Polygon &, const Triangle &)
{
	logger::sslog(LOG_DEBUG) << "Polygon - Triangle" << logger::endlog();
}

bool	collision::collision(const Circle &, const Polygon &)
{
	logger::sslog(LOG_DEBUG) << "Circle - Polygon" << logger::endlog();
}

bool	collision::collision(const Circle &, const Circle &)
{
	logger::sslog(LOG_DEBUG) << "Circle - Circle" << logger::endlog();
}

bool	collision::collision(const Circle &, const Triangle &)
{
	logger::sslog(LOG_DEBUG) << "Circle - Triangle" << logger::endlog();
}

bool	collision::collision(const Triangle &, const Polygon &)
{
	logger::sslog(LOG_DEBUG) << "Triangle - Polygon" << logger::endlog();
}

bool	collision::collision(const Triangle &, const Circle &)
{
	logger::sslog(LOG_DEBUG) << "Triangle - Circle" << logger::endlog();
}*/

bool	collision::collision(const Triangle &, const Triangle &)
{
	logger::sslog(LOG_DEBUG) << "Triangle - Triangle" << logger::endlog();
	return (false);
}

bool	collision::collision(const Triangle &, const Rectangle &)
{
	logger::sslog(LOG_DEBUG) << "Triangle - Rectangle" << logger::endlog();
	return (false);
}

bool	collision::collision(const Triangle &, const EmptyShape &)
{
	logger::sslog(LOG_DEBUG) << "Triangle - Empty" << logger::endlog();
	return (false);
}

bool	collision::collision(const Rectangle &, const Triangle &)
{
	logger::sslog(LOG_DEBUG) << "Rectangle - Triangle" << logger::endlog();
	return (false);
}

bool	collision::collision(const Rectangle &, const Rectangle &)
{
	logger::sslog(LOG_DEBUG) << "Rectangle - Rectangle" << logger::endlog();
	return (false);
}

bool	collision::collision(const Rectangle &, const EmptyShape &)
{
	logger::sslog(LOG_DEBUG) << "Rectangle - Empty" << logger::endlog();
	return (false);
}

bool	collision::collision(const EmptyShape &, const Triangle &)
{
	logger::sslog(LOG_DEBUG) << "Triangle - Triangle" << logger::endlog();
	return (false);
}

bool	collision::collision(const EmptyShape &, const Rectangle &)
{
	logger::sslog(LOG_DEBUG) << "Empty - Rectangle" << logger::endlog();
	return (false);
}

bool	collision::collision(const EmptyShape &, const EmptyShape &)
{
	logger::sslog(LOG_DEBUG) << "Empty - Empty" << logger::endlog();
	return (false);
}

bool	collision::collide(const Shape &a, const Shape &b)
{
	return (b.collide(a.getCollider()));
}
