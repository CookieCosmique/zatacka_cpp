/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Rectangle.hpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:55:06 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:55:06 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef RECTANGLE_HPP
# define RECTANGLE_HPP

//# include "Game/Collisions/Shape/Polygon.hpp"
# include "Game/Collisions/Collider/RectangleCollider.hpp"

class	Rectangle
{
private:
	const RectangleCollider	_collider;
public:
	Rectangle(void);
	virtual	~Rectangle(void);
	virtual const Collider	&getCollider(void) const;
	virtual bool			collide(const Collider &c) const;
};

#endif


