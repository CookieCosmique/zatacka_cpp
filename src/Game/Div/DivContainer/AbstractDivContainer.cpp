/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   AbstractDivContainer.cpp                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:12:20 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivContainer/AbstractDivContainer.hpp"
#include "Utility/FlatBox.hpp"

AbstractDivContainer::AbstractDivContainer(const agl::Vec2 &dimensions)
{
	_dimensions = dimensions;
	_format = FlatBox(agl::Vec2(0.0,0.0), _dimensions);
}

AbstractDivContainer::~AbstractDivContainer(void)
{

}

agl::Vec2	AbstractDivContainer::getDimensions(void) const
{
	return (_dimensions);
}

FlatBox	AbstractDivContainer::getFormat(void) const
{
	return (_format);
}

FlatBox	AbstractDivContainer::getZone(void) const
{
	return (_zone);
}
