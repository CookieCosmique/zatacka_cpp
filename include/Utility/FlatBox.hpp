/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   FlatBox.hpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:38:12 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLATBOX_HPP
# define FLATBOX_HPP

# include "Vec2.hpp"

//# endif
/** \brief A class that represent an horizontal box in 2d space 
*
*	This class will be used in the map_div system to
*	keep track of what part of space an group of
*	div occupies
*/
class FlatBox
{
private:
	agl::Vec2	_downLeft; /**< The down left corner of the box */
	agl::Vec2	_upRight; /**< The up right corner of the box */
public:
	/** \brief Construct a (0.0,0.0), (0.0,0.0) object */
	FlatBox(void);
	/** \brief Construct a FlatBox with the given corners */
	FlatBox(const agl::Vec2 &downLeft, const agl::Vec2 &upRight);
	/** \brief Copy Constructor */
	FlatBox(const FlatBox &other);

	/** \brief Return True if the FlatBox is empty
	*
	*	-If the down left and top right corner are
	*	the same , the box is considered empty
	*
	*	-If the box is "reversed", the down left corner 
	*	is over the up right , or more on the right, 
	*	the box is considered empty
	*/
	bool	isEmpty(void) const;
	/** \brief Return the down left corner of the box */
	agl::Vec2	getDownLeft(void) const;
	/** \brief Return the up right corner of the box */
	agl::Vec2	getUpRight(void) const;
	/** \brief return UpRight - DownLeft; */
	agl::Vec2	getDimensions(void) const;
	/** \brief Allow to set the down left corner of the box */
	void	setDownLeft(const agl::Vec2 &downLeft);
	/** \brief Allow to set the down up right of the box */
	void	setUpRight(const agl::Vec2 &upRight);

	/** \brief Return the smallest box that contain both this and other 
	*
	*	Will not take into account empty boxes.
	*/
	FlatBox	operator|(const FlatBox &other) const;
	/** \brief Return the biggest box that is contained in both this and other 
	*
	*	Will return an empty box if at least one box is empty
	*/
	FlatBox	operator&(const FlatBox &other) const;
	/** \brief Return a box that can be expressed as a multiple of format offseted by format down left
	*
	*	Will return a box that down left is format down left and
	*	the dimension is a multiple of dimension (format dimensions)
	*/
	FlatBox	operator%(const FlatBox &format) const;
	/** \brief Return weither or not the two boxes tuch each other
	*
	*	Is the same as testing if (this & other) is empty (in theory)
	*/
	bool	operator&&(const FlatBox &other) const;
	/** \brief Simple per member tester */
	bool	operator==(const FlatBox &other) const;

	/** \brief Simple per member setter */
	FlatBox	&operator=(const FlatBox &other);
	/** \brief Same as |, but applicated to this */
	FlatBox &operator|=(const FlatBox &other);
	/** \brief Same as &=, but applicated to this */
	FlatBox &operator&=(const FlatBox &other);

	friend FlatBox	operator+(const agl::Vec2 &vec, const FlatBox &flat);
	friend FlatBox	operator+(const FlatBox &flat, const agl::Vec2 &vec);
	friend FlatBox	operator-(const FlatBox &flat, const agl::Vec2 &vec);

	FlatBox	&operator+=(const agl::Vec2&vec);
	FlatBox	&operator-=(const agl::Vec2&vec);
	
	/** \brief Logger operator << overload */
	friend std::ostream	&operator<<(std::ostream &o, const FlatBox &flat);
};

#endif
