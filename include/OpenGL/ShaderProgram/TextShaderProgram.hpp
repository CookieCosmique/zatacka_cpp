/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TextShaderProgram.hpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 17:51:34 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:27:41 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXT_SHADER_PROGRAM_HPP
# define TEXT_SHADER_PROGRAM_HPP

# include "CameraShaderProgram.hpp"
# include "FontTexture.hpp"
# include "Vec2.hpp"

# define TEXT_VERTEX "shaders/text/vertex.glsl"
# define TEXT_GEOMETRY "shaders/text/geometry.glsl"
# define TEXT_FRAGMENT "shaders/text/fragment.glsl"

class	TextShaderProgram : public agl::CameraShaderProgram
{
private:
	GLuint		_id_char_size;
	GLuint		_id_font_size;
	GLuint		_id_pos;
	GLuint		_id_size;
	GLuint		_id_emphasis;

public:
	TextShaderProgram(void) throw(InvalidShaderProgramException);
	~TextShaderProgram(void);

	void			updateFont(const agl::FontTexture &font) const;
	void			updateCharSize(const float width, const float height) const;
	void			updateFontSize(const float width, const float height) const;
	void			updatePosition(const agl::Vec2 &position) const;
	void			updateSize(const float size) const;
	void			updateEmphasis(const int emphasis) const;

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
