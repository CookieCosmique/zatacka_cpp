/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivGroupIterator.hpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVGROUPITERATOR_H
# define DIVGROUPITERATOR_H

# include "Utility/Logger.hpp"

class DivGroup;

class DivGroupIterator :
{
private:
	DivGroup	&_group;
	int			_row;
	int			_column;
public:
	DivGroupIterator(DivGroup &group, int row, int column);

	bool	ended();
	bool	operator++(int);
	bool	operator--(int);
	Div		&operator*(void);

	bool	operator==(const DivGroupIterator &other);
};

#endif