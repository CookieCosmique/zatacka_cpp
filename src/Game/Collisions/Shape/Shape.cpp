/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Shape.cpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:11:08 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:02:09 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Shape/Shape.hpp"
#include "Game/Collisions/Shape/EmptyShape.hpp"

Shape::Shape(void)
{
	_stretched = new EmptyShape();
}

Shape::~Shape(void)
{
	delete _stretched;
}
FlatBox		Shape::getZone(void) 
{
	return FlatBox();
}
const Shape	&Shape::stretch(const agl::Vec2 &delta)
{
	(void) delta;
//	return (EmptyShape()); //cette forme cause un warning, et donc peux pas compiler
	return (*_stretched);
}
