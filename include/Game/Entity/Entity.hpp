/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Entity.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-29 19:05:39 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:35:57 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENTITY_HPP
# define ENTITY_HPP

# include "Vec2.hpp"
# include "Game/Actor.hpp"
# include "Updatable.hpp"
# include "Renderable.hpp"

class	Game;

class	Entity : public agl::Updatable, public agl::Renderable, public Actor
{
protected:
	agl::Vec2		_position;

public:
	Entity(Game &game, agl::Camera &camera, const int level_update,
			const int level_render);
	virtual ~Entity(void);

	virtual void	render(void) const = 0;
	virtual void	update(const agl::GameTime &game_time) = 0;

	const agl::Vec2		&getPosition(void) const;

	/** \brief Print object */
	virtual void	objectPrint(std::ostream &o) const;
};

#endif
