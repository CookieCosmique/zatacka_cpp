/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoArrowOld.cpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivToDo/DivToDoArrowOld.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Entity/Arrow.hpp"

DivToDoArrowOld::DivToDoArrowOld(Arrow &arrow) : _arrow(arrow) {}

void	DivToDoArrowOld::doTo(ChunkDiv &div)
{
//	logger::sslog(LOG_DEBUG) << "Old Arrow div : " << div << logger::endlog();
	div.removeArrow(&_arrow);
	div.removeArrowLines(&_arrow);
//	logger::sslog(LOG_DEBUG) << "OLD OHO " << logger::endlog();
	_arrow.willPop();
}