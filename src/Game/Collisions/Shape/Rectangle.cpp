/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Rectangle.cpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:57:43 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:57:43 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Shape/Rectangle.hpp"

Rectangle::Rectangle(void) : _collider(*this)
{
}

Rectangle::~Rectangle(void)
{
}

const Collider		&Rectangle::getCollider(void) const
{
	return (this->_collider);
}

bool				Rectangle::collide(const Collider &c) const
{
	return (c.collide(*this));
}



