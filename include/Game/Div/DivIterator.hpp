/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivIterator.hpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVITERATOR_HPP
# define DIVITERATOR_HPP

# include "Game/Div/DivTypeIterator.hpp"

template <typename T>
class DivTypeIterator;

template <typename T>
class DivIterator
{
private:
	DivTypeIterator<T>	*_typeIterator;
public:
	DivIterator(DivTypeIterator<T> *typeIterator);
	DivIterator(const DivIterator<T> &other);
	~DivIterator(void);

	void	right(void);
	void	left(void);
	void	up(void);
	void	down(void);

	void	rightN(const int n);
	void	leftN(const int n);
	void	upN(const int n);
	void	downN(const int n);

	T		&operator*(void);
};

#endif