/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LinkIterator.hpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINKITERATOR_HPP
# define LINKITERATOR_HPP

# include "Utility/SmartArray.hpp"

class LinesContainer;

class LinkIterator
{
private:
	bool						_valid;
	size_t						_index;
	SmartArray<GLuint>			*_indexData;

	LinesContainer				*_container;
public:
	LinkIterator(void);
	LinkIterator(int index, SmartArray<unsigned int> &indexData, LinesContainer &container);
	LinkIterator(const LinkIterator &ite);

	int		getIndex(void) const;
	bool	isValid(void) const;

	void	free(void);

	LinkIterator	&operator=(const LinkIterator &ite);
};

#endif