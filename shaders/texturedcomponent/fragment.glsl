#version 400 core

in vec2				uv;

out vec4			out_color;

uniform sampler2D	texture_sampler;

void	main(void)
{
	out_color = vec4(texture(texture_sampler, uv).rgb, 1.0f);
}

