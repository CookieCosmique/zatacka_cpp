/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToCollideStrech.cpp                          |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:14:35 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivToDo/DivToCollideStrech.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Collisions/ColliderComponent/ColliderComponent.hpp"
#include "Vec2.hpp"

DivToCollideStrech::DivToCollideStrech(ColliderComponent &component, const agl::Vec2 &delta) : _component(component), _delta(delta) {}

void	DivToCollideStrech::doTo(ChunkDiv &div)
{
	div.collideStrech(_component, _delta); // div will call doOnCollision() de component;
}



