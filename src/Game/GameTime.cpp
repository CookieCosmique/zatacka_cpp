/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   GameTime.cpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-27 17:02:06 by Bastien             |\________\          */
/*   Updated: 2016/05/21 18:07:28 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iomanip>
#include "Game/GameTime.hpp"
#include "Environment/Environment.hpp"

// Constructor - Destructor

GameTime::GameTime(void)
{
	this->reset();
}

GameTime::~GameTime(void)
{
}

// Member Fuctions

void		GameTime::update(void)
{
	double	t;

	t = glfwGetTime();
	this->_time_diff = t - this->_last_time;
	this->_last_time = t;
	this->_time += this->_time_diff * this->_speed;

	this->_frames++;
	this->_time_diff_refresh += this->_time_diff;
	if (this->_time_diff_refresh >= this->_refresh)
	{
		logger::sslog(LOG_DEBUG) << "FPS: " << int(this->_frames / this->_refresh) << logger::endlog();
		logger::sslog(LOG_DEBUG) << *this << logger::endlog();
		this->_time_diff_refresh = 0;
		this->_frames = 0;
	}
}

void		GameTime::reset(void)
{
	this->_time = 0;
	this->_last_time = glfwGetTime();
	this->_speed = 1;
	this->_time_diff = 0;
	this->_time_diff_refresh = 0;
	this->_refresh = 1.0f;
	this->_frames = 0;
}

std::string	GameTime::timeToString(const double t) const
{
	std::stringstream	ss;
	int					min;
	int					sec;
	int					secdiv;

	min = int(t / 60);
	sec = int(t) % 60;
	secdiv = int(t * 100) % 100;
	ss << std::setw(2) << std::setfill('0') << min;
	ss << ":";
	ss << std::setw(2) << std::setfill('0') << sec;
	ss << ":";
	ss << std::setw(2) << std::setfill('0') << secdiv;
	return (ss.str());
}

// Accessors

double		GameTime::getTime(void) const
{
	return (this->_time);
}

std::string	GameTime::getTimeStr(void) const
{
	std::stringstream	ss;
	int					min;
	int					sec;

	min = int(this->_time / 60);
	sec = int(this->_time) % 60;
	ss << std::setw(2) << std::setfill('0') << min;
	ss << ":";
	ss << std::setw(2) << std::setfill('0') << sec;
	return (ss.str());
}

double		GameTime::getDiff(void) const
{
	return (this->_time_diff);
}

double		GameTime::getSpeed(void) const
{
	return (this->_speed);
}

void		GameTime::setSpeed(const double speed)
{
	this->_speed = speed;
}

// Print

void		GameTime::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "GameTime" << C_RESET << ": { " <<
			C_VAR << "time" << C_RESET << " = " << timeToString(this->_time) << ", " <<
			C_VAR << "game_speed" << C_RESET << " = " << this->_speed << ", " <<
			C_VAR << "time_diff" << C_RESET << " = " << this->_time_diff << C_RESET << ", " <<
			C_VAR << "time_diff_refresh" << C_RESET << " = " << this->_time_diff_refresh <<
			" }";
}
