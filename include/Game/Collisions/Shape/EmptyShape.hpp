/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   EmptyShape.hpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:11:08 by Bastien             |\________\          */
/*   Updated: 2016-03-22 21:11:08 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef EMPTYSHAPE_HPP
# define EMPTYSHAPE_HPP

#include "Game/Collisions/Shape/Shape.hpp"
#include "Game/Collisions/Collider/EmptyShapeCollider.hpp"

class EmptyShape : public Shape
{
private:
	EmptyShapeCollider	_collider;
public:
	EmptyShape(void);
	virtual	~EmptyShape(void);
	virtual const Collider	&getCollider(void) const;
	virtual bool		collide(const Collider &c) const;
};

#endif