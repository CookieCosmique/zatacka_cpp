/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivSet.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:00:05 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVSET_HPP
# define DIVSET_HPP

# include "Utility/FlatBox.hpp"

template <typename T>
class DivGroup;
template <typename T>
class DivContainer;
template <typename T>
class DivToDo; // A factory class that have a function do(Div &div) that do stuff, like a factory

class HitInfo;
class ColliderComponent;

template <typename T>
class DivSet
{
private:
	DivContainer<T>	&_container;
	DivGroup<T>		*_currGroup;
	DivGroup<T>		*_crossGroup;

	DivToDo<T>		*_toDoNew; // will be called for each new div		
	DivToDo<T>		*_toDoOld; // will be called for each old div
public:
	DivSet(DivContainer<T> &container, DivToDo<T> *toDoNew, DivToDo<T> *toDoOld);
	DivSet(DivContainer<T> &container, DivToDo<T> *toDoNew, DivToDo<T> *toDoOld, const FlatBox &newzone);
	~DivSet(); //will have to delete to Do new and to do old;
	
	void	setDoNew(DivToDo<T> *toDo);
	void	setDoOld(DivToDo<T> *toDo);

	void	update(const FlatBox &newzone); // will create a temporary group of old | new, go on each and look if its new, old or new then old
										// move from a div ? // container movable : have a div& with a max and a min row, can move up down etc
	void	forEachCurr(DivToDo<T> *toDo); // will delete toDo at the end;
	void	forEachCross(DivToDo<T> *toDo); // will delete toDo at the end;

	void	doNew(T &div);		//use doNew on div with a NULL safe test
	void	doOld(T &div);		//use doOld on div with a NULL safe test

//	void	collide(const ColliderComponent &comp);
//	void	collideCross(const ColliderComponent &comp, const Vec2 &delta);
};

#endif

