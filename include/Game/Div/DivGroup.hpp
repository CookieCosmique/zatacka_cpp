/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivGroup.hpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:57:33 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVGROUP_HPP
# define DIVGROUP_HPP

#include "Object.hpp"
#include "Utility/FlatBox.hpp"
#include "Game/Div/DivIterator.hpp"

template <typename T>
class DivContainer;
template <typename T>
class DivIterator;
template <typename T>
class GroupDivIterator;

template <typename T>
class DivGroup : public agl::Object
{
private:
	DivContainer<T>	&_container;	//

	DivIterator<T>	_begin;			//< down left begin iterator anchor
	DivIterator<T>	_end;			//<	up right end iterator anchor

	FlatBox			_zone;
	int				_rows;			//< number of rows of divs in this group
	int				_cols;			//< number of columns of divs in this group 
public:
	DivGroup(DivContainer<T> &container, const FlatBox &zone, const DivIterator<T> &begin, const DivIterator<T> &end);

	GroupDivIterator<T>	start(void);
	GroupDivIterator<T>	end(void);

	DivIterator<T>		getBegin(void);
	DivIterator<T>		getEnd(void);
		
	bool				isEmpty(void) const;
	int					getRows(void) const;
	int					getCols(void) const;
	const FlatBox		&getZone(void) const;

	bool				operator==(const DivGroup<T> &other);
	DivGroup<T>			&operator=(const DivGroup<T> &other);

	void				objectPrint(std::ostream &o) const;
};

#endif
