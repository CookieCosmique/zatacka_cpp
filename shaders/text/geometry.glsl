#version 400 core

layout(points) in;
layout(triangle_strip, max_vertices = 8) out;

in uint g_id[];
in vec2 g_char_x[];
in vec3	g_color[];

out vec2 uv;
out vec2 out_coords;
out float max_x;
out vec3 f_color;
out float f_alpha;

uniform vec2 cam_size;
uniform vec2 cam_pos;
uniform vec2 char_size;
uniform vec2 font_size;
uniform float size;
uniform int emphasis;
uniform vec2 pos;

vec2	apply_cam(vec2 p)
{
	return (vec2((p.x - cam_pos.x) / cam_size.x * 2 - 1.0f,
						(p.y - cam_pos.y) / cam_size.y * 2 - 1.0f));
}

void	emit_points(vec2 p, vec2 coords)
{
	gl_Position = vec4(apply_cam(p), 0.0f, 1.0f);
	out_coords = coords;
	EmitVertex();
}

void	emit_letter(vec2 p)
{
	emit_points(p, vec2(0, 0));
	emit_points(vec2(p.x + size, p.y), vec2(1, 0));
	if ((emphasis & 8) != 0)
	{
		emit_points(vec2(p.x + 0.35 * size, p.y + size), vec2(0, 1));
		emit_points(vec2(p.x + 1.35 * size, p.y + size), vec2(1, 1));
	}
	else
	{
		emit_points(vec2(p.x, p.y + size), vec2(0, 1));
		emit_points(vec2(p.x + size, p.y + size), vec2(1, 1));
	}
	EndPrimitive();
}

void	emit_char(vec2 p)
{
	emit_letter(pos + p * size);
}

vec2	get_uv(uint id)
{
	return (vec2(int(id * char_size.x) % int(font_size.x) + g_char_x[0].x,
			font_size.y - char_size.y - int(id * char_size.x / (font_size.x - 1)) * char_size.y));
}

void	main(void)
{
	vec2	position = gl_in[0].gl_Position.xy;

	uv = get_uv(g_id[0]);
	max_x = g_char_x[0].y;
	if ((emphasis & 4) != 0)
	{
		f_alpha = 0.25f;
		f_color = vec3(0.0f, 0.0f, 0.0f);
//		emit_char(position + vec2(0.5f / char_size.x, -0.5f / char_size.y));
		emit_char(position + vec2(1.0f / 16.0f, -1.0f / 16.0f));
	}
	f_color = g_color[0];
	f_alpha = 1.0f;
	emit_char(position);
}
