/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ChunkDivFactory.cpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/ChunkDivFactory.hpp"
#include "Game/Div/ChunkDiv.hpp"

ChunkDivFactory::ChunkDivFactory(Game &game) : _game(game) {}

ChunkDiv	*ChunkDivFactory::make(const FlatBox &zone)
{
	return (new ChunkDiv(_game, zone));
}