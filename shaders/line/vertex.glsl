#version 400 core

layout(location = 1) in vec2	position;
layout(location = 2) in int		id;

out int	in_num;

void	main(void)
{
	in_num = gl_VertexID;
	gl_Position = vec4(position, 0.0f, 1.0f);
}
