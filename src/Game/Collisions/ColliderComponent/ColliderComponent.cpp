/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ColliderComponent.cpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:02:34 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/ColliderComponent/ColliderComponent.hpp"
#include "Game/Collisions/Collisions.hpp"
#include "Game/Collisions/Shape/Shape.hpp"
#include "Game/Actor.hpp"

using namespace agl;

ColliderComponent::ColliderComponent(Actor &parent, int type, int toHit, COLactive active, Shape &shape) : _shape(shape), _parent(parent)
{
	this->_active = active;
	this->_type = type;
	this->_toHit = toHit;
}

void	ColliderComponent::addType(int	type)
{
	this->_type |= type;
}
void	ColliderComponent::removeType(int type)
{
	this->_type -= (type & this->_type);
}
void	ColliderComponent::setType(int	type)
{
	this->_type = type;
}

void	ColliderComponent::addToHit(int hit)
{
	this->_toHit |= hit;
}
void	ColliderComponent::removeToHit(int hit)
{
	this->_toHit -= (hit & this->_toHit);
}
void	ColliderComponent::setToHit(int hit)
{
	this->_toHit = hit;
}

Shape	&ColliderComponent::getShape(void)
{
	return (this->_shape);
}

FlatBox	ColliderComponent::getZone(void)
{
	return	(this->_shape.getZone());
}

Actor	&ColliderComponent::getParent(void)
{
	return (this->_parent);
}

void	ColliderComponent::doOnCollision(ColliderComponent &other)
{
	this->_parent.onCollision(other);
}

void	ColliderComponent::clearCollided(void)
{
	this->_collided.clear();
}

void	ColliderComponent::collideComponentStretch(ColliderComponent &other, const Vec2 &delta)
{
	collideComponentWithShape(*this, other, this->_shape.stretch(delta), other._shape);
	/*(this->_active && this->_collided.count(&other)) && !(other._active && other._collided.count(this)))
	{
		if(collision::collide(this->_shape.strech(delta), other._shape))	//difference is here
		{
			this->onCollision(other);
			other.onCollision(*this);
			if(this->_active)
				this->_collided.emplace(&other, other);
			if(other._active)
				other._collided.emplace(this, *this);
		}
	}*/
}

void	collideComponent(ColliderComponent &a, ColliderComponent &b)
{
	collideComponentWithShape(a, b, a._shape, b._shape);
	/*if (!(a._active && a._collided.count(&b)) && !(b._active && b._collided.count(&a)))
	{
		if(collision::collide(a._shape, b._shape))
		{
			a.doOnCollision(b);
			b.doOnCollision(a);
			if(a._active)
				a._collided.emplace(&b, b);
			if(b._active)
				b._collided.emplace(&a, a);
		}
	}*/
}

void	collideComponentWithShape(ColliderComponent &a, ColliderComponent &b, const Shape &shapeA, const Shape &shapeB)
{
	if (!(a._active && a._collided.count(&b)) && !(b._active && b._collided.count(&a)) && 
			((!a._active || (b._toHit & a._type)) || (!b._active || (a._toHit & b._type))))
	{
		if(collision::collide(shapeA, shapeB))
		{
			a.doOnCollision(b);
			b.doOnCollision(a);
			if(a._active)
				a._collided.emplace(&b, b);
			if(b._active)
				b._collided.emplace(&a, a);
		}
	}
}
