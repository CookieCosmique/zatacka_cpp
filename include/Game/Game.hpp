/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Game.hpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/04 16:36:09 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_HPP
# define GAME_HPP

# include <list>
# include "Scene.hpp"
# include "Environment.hpp"
# include "Game/Camera/ManualCamera.hpp"
# include "KeyboardControllable.hpp"

# include "OpenGL/ShaderProgram/ButtonShaderProgram.hpp"
# include "Scene/Interface/TextComponent.hpp"
# include "Game/HUD/TimerHUD.hpp"

template <typename T>
class	DivContainer;
class	ChunkDiv;
class	LineShaderProgram;

/** \brief The game class, it contains the main Run loop
*
*	It contains the Update object, and the Render object
*	and will call their update / render function once per frame
*	will call the update of the inputs
*/
class Game : public agl::Scene, public agl::KeyboardControllable<Game>
{
private:
	DivContainer<ChunkDiv>			*_container;
	agl::SmartPointer<ManualCamera>	_gameCamera;

	LineShaderProgram				*_lineShaderProgram;

	TextShaderProgram				_textShaderProgram;
	agl::FontTexture				*_font;
	agl::SmartPointer<TimerHUD>		_timer;

public:
	/** \brief Game constructor */
	Game(agl::Environment &env);
	/** \brief Game desctructor */
	~Game(void);

	/** \brief Contain the main loop
	*
	*	Call run to start running ,
	*	will end at the end of the game
	*/
	virtual int		run(void);

	/** \brief _container getter */
	DivContainer<ChunkDiv>	&getContainer(void);
	/** \brief _mainCam getter */
	ManualCamera			&getGameCamera(void);

	LineShaderProgram		&getLineShaderProgram(void);

	/** \brief print object */
	virtual void	objectPrint(std::ostream &o) const;
};

#endif
