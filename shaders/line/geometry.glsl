#version 400 core

#define M_PI 3.1415926535897932384626433832795f

layout(triangles) in;
layout(triangle_strip, max_vertices = 11) out;

in int			in_num[];

out float 		in_ratio;
out float 		in_expos;

//uniform float	size;
//uniform int		last_vertex;


uniform vec2	cam_size;
uniform vec2	cam_pos;

vec2	get_normal_r(void)
{
	if(gl_in[1].gl_Position == gl_in[2].gl_Position)
		return (normalize(vec2(gl_in[1].gl_Position.y - gl_in[0].gl_Position.y,
			-gl_in[1].gl_Position.x + gl_in[0].gl_Position.x)));
	else
		return (normalize(vec2(gl_in[2].gl_Position.y - gl_in[1].gl_Position.y,
			-gl_in[2].gl_Position.x + gl_in[1].gl_Position.x)));
}

vec2	get_normal_l(void)
{
	if(gl_in[0].gl_Position == gl_in[1].gl_Position)
		return (normalize(vec2(gl_in[2].gl_Position.y - gl_in[1].gl_Position.y,
			-gl_in[2].gl_Position.x + gl_in[1].gl_Position.x)));
	else
		return (normalize(vec2(gl_in[1].gl_Position.y - gl_in[0].gl_Position.y,
			-gl_in[1].gl_Position.x + gl_in[0].gl_Position.x)));
}

void	emit(vec2 pos, float rate, float expose)
{
	in_ratio = rate;
	in_expos = expose;
	gl_Position = vec4((pos.x - cam_pos.x) / cam_size.x * 2 - 1.0f,
						(pos.y - cam_pos.y) / cam_size.y * 2 - 1.0f, 0.0f, 1.0f);
	EmitVertex();
}

void	main(void)
{
	vec2	out_pos[8];
	vec2	out_pos_m;
	float	ratio_l;
	float	ratio_r;
	float	ratio_mix;

	float	size = 7;

	vec2	n_l;
	vec2	n_r;
	vec2	light = -normalize(vec2(1, 0));

	n_l = get_normal_l();
	n_r = get_normal_r();

	int nope;

	nope = 0;	

	out_pos[0] = vec2((gl_in[0].gl_Position.x + gl_in[1].gl_Position.x) / 2 + n_l.x * size / 2,
						(gl_in[0].gl_Position.y + gl_in[1].gl_Position.y) / 2 + n_l.y * size / 2);
	out_pos[1] = vec2((gl_in[0].gl_Position.x + gl_in[1].gl_Position.x) / 2 - n_l.x * size / 2,
						(gl_in[0].gl_Position.y + gl_in[1].gl_Position.y) / 2 - n_l.y * size / 2);

	out_pos[2] = vec2(gl_in[1].gl_Position.x + n_l.x * size / 2,
						gl_in[1].gl_Position.y + n_l.y * size / 2);
	out_pos[3] = vec2(gl_in[1].gl_Position.x - n_l.x * size / 2,
						gl_in[1].gl_Position.y - n_l.y * size / 2);
	out_pos[4] = vec2(gl_in[1].gl_Position.x + n_r.x * size / 2,
						gl_in[1].gl_Position.y + n_r.y * size / 2);
	out_pos[5] = vec2(gl_in[1].gl_Position.x - n_r.x * size / 2,
						gl_in[1].gl_Position.y - n_r.y * size / 2);


//	if(in_num[2] == last_vertex)
//	{
//		out_pos[6] = vec2(gl_in[2].gl_Position.x + n_r.x * size / 2,
//							gl_in[2].gl_Position.y + n_r.y * size / 2);
//		out_pos[7] = vec2(gl_in[2].gl_Position.x - n_r.x * size / 2,
//							gl_in[2].gl_Position.y - n_r.y * size / 2);
//	}
//	else
//	{	
		out_pos[6] = vec2((gl_in[1].gl_Position.x + gl_in[2].gl_Position.x) / 2 + n_r.x * size / 2,
							(gl_in[1].gl_Position.y + gl_in[2].gl_Position.y) / 2 + n_r.y * size / 2);
		out_pos[7] = vec2((gl_in[1].gl_Position.x + gl_in[2].gl_Position.x) / 2 - n_r.x * size / 2,
							(gl_in[1].gl_Position.y + gl_in[2].gl_Position.y) / 2 - n_r.y * size / 2);
//	}

	ratio_l = (dot(n_l, light) + 1) / 2;
	ratio_r = (dot(n_r, light) + 1) / 2;
	
	if(gl_in[0].gl_Position == gl_in[1].gl_Position)
		ratio_l = ratio_r;
	ratio_mix = (ratio_r + ratio_l) / 2;

	out_pos_m = vec2(gl_in[1].gl_Position.x, gl_in[1].gl_Position.y);

	vec2	dir1 = normalize(gl_in[1].gl_Position.xy - gl_in[0].gl_Position.xy);
	vec2	dir2 = normalize(gl_in[2].gl_Position.xy - gl_in[1].gl_Position.xy);

	if(gl_in[0].gl_Position == gl_in[1].gl_Position)
		dir1 = dir2;

	if(gl_in[1].gl_Position == gl_in[2].gl_Position)
		dir2 = dir1;

	float	sca = dot( vec2(-dir1.y,dir1.x), dir2);

	vec2	med;
	vec2	a;
	vec2	b;
	float	cross;

	cross = (dir1.y * dir2.x - dir1.x * dir2.y);
	med = out_pos_m;
	
	if (sca > 0)
	{
		a = out_pos[3];
		b = out_pos[5];

		if (abs(cross) < 0.1f && dot(dir1,dir2) > 0)
		{
			med = (a + b) / 2;

		}
		else if (cross != 0)
		{
			med.y =   (dir2.y * (dir1.y * a.x - dir1.x * a.y) - dir1.y * (dir2.y * b.x - dir2.x * b.y) ) / cross;
			med.x =   (dir2.x * (dir1.y * a.x - dir1.x * a.y) - dir1.x * (dir2.y * b.x - dir2.x * b.y) ) / cross;
		}

		if(length(med - ((out_pos[4] + out_pos[2]) / 2) ) > size*1.1)
			nope = 1;

	}
	else
	{

		a = out_pos[2];
		b = out_pos[4];

		if (abs(cross) < 0.1f && dot(dir1,dir2) > 0)
		{
			med = (a + b) / 2;
		}
		else if (cross != 0)
		{
			med.y =   (dir2.y * (dir1.y * a.x - dir1.x * a.y) - dir1.y * (dir2.y * b.x - dir2.x * b.y) ) / cross;
			med.x =   (dir2.x * (dir1.y * a.x - dir1.x * a.y) - dir1.x * (dir2.y * b.x - dir2.x * b.y) ) / cross;
		}

		if( dot(med - out_pos[6], dir2) > 0)
			nope = 1;

		if(length(med - ((out_pos[5] + out_pos[3]) / 2)) > size * 1.1)
			nope = 1;

	}

	if(in_num[0] != 0)
	{
		if( (abs(cross) >= 0.01f || dot(dir1,dir2) > 0 ) && nope == 0)
		{
			if(sca > 0)
			{
				emit(out_pos[0], 0, ratio_l);
				emit(out_pos[1], 1, ratio_l);
				emit(out_pos[2], 0, ratio_mix);
				emit(med, 1, ratio_mix);
				EndPrimitive();

				emit(out_pos[4], 0, ratio_mix);
				emit(med, 1, ratio_mix);
				emit(out_pos[6], 0, ratio_r);
				emit(out_pos[7], 1, ratio_r);
				EndPrimitive();

				emit(med, 1, ratio_mix); // ou 5 pareil
				emit(out_pos[2], 0, ratio_mix);
				emit(out_pos[4], 0, ratio_mix);
				EndPrimitive();
			}
			else
			{	
			
				emit(out_pos[0], 0, ratio_l);
				emit(out_pos[1], 1, ratio_l);
				emit(med, 0, ratio_mix);
				emit(out_pos[3], 1, ratio_mix);
				EndPrimitive();

				emit(med, 0, ratio_mix);
				emit(out_pos[5], 1, ratio_mix);
				emit(out_pos[6], 0, ratio_r);
				emit(out_pos[7], 1, ratio_r);
				EndPrimitive();

				emit(med, 0, ratio_mix); // ou 4 pareil
				emit(out_pos[3], 1, ratio_mix);
				emit(out_pos[5], 1, ratio_mix);
				EndPrimitive();
			}
		}
		else
		{
			emit(out_pos[0], 0, ratio_l);
			emit(out_pos[1], 1, ratio_l);
			emit(out_pos[2], 0, ratio_mix);
			emit(out_pos[3], 1, ratio_mix);
			EndPrimitive();

			emit(out_pos[4], 0, ratio_mix);
			emit(out_pos[5], 1, ratio_mix);
			emit(out_pos[6], 0, ratio_r);
			emit(out_pos[7], 1, ratio_r);
			EndPrimitive();

			if(sca > 0)
			{
				emit(out_pos[5], 1, ratio_mix);
				emit(out_pos[2], 1, ratio_mix);
				emit(out_pos[4], 0, ratio_mix);
				EndPrimitive();
			}
			else
			{
				emit(out_pos[4], 0, ratio_mix);
				emit(out_pos[3], 0, ratio_mix);
				emit(out_pos[5], 1, ratio_mix);
				EndPrimitive();
			}

		}
	}

}
