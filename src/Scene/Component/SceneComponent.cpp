/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SceneComponent.cpp                              |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 15:33:34 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:27:37 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Scene/Component/SceneComponent.hpp"
#include "Scene/Interface/Area.hpp"
#include "Camera.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

SceneComponent::SceneComponent(Scene &scene, Area *area,
		const Vec2 &position, const Vec2 &size,
		const PositionType pos_type, const PositionType size_type) :
	_area(area), _scene(scene)
{
	this->setSize(size, size_type);
	this->setPosition(position, pos_type);
}

SceneComponent::~SceneComponent(void)
{

}

// Member Functions

void			SceneComponent::scale(const float coef)
{
	this->_position -= (this->_size * coef - this->_size) / 2.0f;
	this->_size *= coef;
	this->onModif();
}

void			SceneComponent::scaleArea(const float coef)
{
	Vec2		ratio;
	Vec2		new_pos;

	if (this->_area != NULL)
	{
		ratio = (this->_position - this->_area->getPosition()) /
			this->_area->getSize();
		this->scale(coef);
		new_pos = (this->_area->getPosition() -
				(this->_area->getSize() * coef - this->_area->getSize()) / 2.0f) +
			Vec2(this->_area->getSize().x * ratio.x * coef,
					this->_area->getSize().y * ratio.y * coef);
		this->translate(new_pos - this->_position);
	}
	else
		this->scale(coef);
}

void			SceneComponent::translate(const float x, const float y)
{
	this->_position.x += x;
	this->_position.y += y;
	this->onModif();
}

void			SceneComponent::translate(const Vec2 &vec)
{
	this->translate(vec.x, vec.y);
}

Vec2			SceneComponent::getRatio(const Vec2 &ratio) const
{
	ratio.x = std::max(ratio.x, 0.0f);
	ratio.y = std::max(ratio.y, 0.0f);
	ratio.x = std::min(ratio.x, 1.0f);
	ratio.y = std::min(ratio.y, 1.0f);
	return (ratio);
}

void			SceneComponent::setPositionRatio(const Vec2 &position,
		const Vec2 &size, const Vec2 &ratio)
{
	float		start;
	float		end;

	start = position.x + this->_size.x / 2.0f;
	end = position.x + size.x - this->_size.x / 2.0f;
	this->_position.x = start + (end - start) * ratio.x - this->_size.x / 2.0f;

	start = position.y + this->_size.y / 2.0f;
	end = position.y + size.y - this->_size.y / 2.0f;
	this->_position.y = start + (end - start) * ratio.y - this->_size.y / 2.0f;
}

void			SceneComponent::setSize(const Vec2 &size,
		const PositionType type)
{
	if (type == PositionType::POS_NORMAL)
		this->_size = size;
	else if (type == PositionType::POS_RATIO)
	{
		Vec2 ratio = getRatio(size);
		if (this->_area)
		{
			this->_size.x = this->_area->getSize().x * ratio.x;
			this->_size.y = this->_area->getSize().y * ratio.y;
		}
		else
		{
			this->_size.x = this->_scene.getCamera().getSize().x * ratio.x;
			this->_size.y = this->_scene.getCamera().getSize().y * ratio.y;
		}
	}
}

void			SceneComponent::setPosition(const Vec2 &position,
		const PositionType type)
{
	if (type == PositionType::POS_NORMAL)
	{
		if (this->_area)
			this->_position = position + this->_area->getPosition();
		else
			this->_position = position;
	}
	else if (type == PositionType::POS_RATIO)
	{
		Vec2 ratio = getRatio(position);
		if (this->_area)
			this->setPositionRatio(this->_area->getPosition(),
					this->_area->getSize(), ratio);
		else
			this->setPositionRatio(this->_scene.getCamera().getPosition(),
					this->_scene.getCamera().getSize(), ratio);
	}
}

// Accessors

const Vec2		&SceneComponent::getPosition(void) const
{
	return (this->_position);
}

const Vec2		&SceneComponent::getSize(void) const
{
	return (this->_size);
}

// Print

void			SceneComponent::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "SceneComponent" << C_RESET << ": {" << logger::tab(1);
	Spawnable::objectPrint(o);
	o << ",\n";
	o << C_VAR << "position" << C_RESET << " = " << this->_position << ",\n";
	o << C_VAR << "size" << C_RESET << " = " << this->_size;
	o << logger::tab(-1) << "}";
}
