/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivArrayIterator.hpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:04:27 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivArrayIterator.hpp"
#include "Game/Div/DivContainerArray.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"

using namespace agl;

template <typename T>
DivArrayIterator<T>::DivArrayIterator(DivContainerArray<T> &container, int row, int col) : _container(container)
{
	_row = row;
	_col = col;
}

template <typename T>
DivTypeIterator<T>	*DivArrayIterator<T>::copy(void)
{
	return (new DivArrayIterator<T>(_container, _row, _col));
}

//Move

template <typename T>
void	DivArrayIterator<T>::right(void)
{
	_col++;
}

template <typename T>
void	DivArrayIterator<T>::left(void)
{
	_col--;
}

template <typename T>
void	DivArrayIterator<T>::up(void)
{
	_row ++;
}

template <typename T>
void	DivArrayIterator<T>::down(void)
{
	_row --;
}

//Move N

template <typename T>
void	DivArrayIterator<T>::rightN(const int n)
{
	_col += n;
}

template <typename T>
void	DivArrayIterator<T>::leftN(const int n)
{
	_col -= n;
}

template <typename T>
void	DivArrayIterator<T>::upN(const int n)
{
	_row += n;
}

template <typename T>
void	DivArrayIterator<T>::downN(const int n)
{
	_row -= n;
}

template <typename T>
T		&DivArrayIterator<T>::getDiv(void)
{
	return (_container.getDiv(_row, _col));
}

template class DivArrayIterator<Div>;
template class DivArrayIterator<ChunkDiv>;
