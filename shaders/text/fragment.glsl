#version 400 core

in vec2		uv;
in vec2		out_coords;
in float	max_x;
in vec3		f_color;
in float	f_alpha;

out vec4	out_color;

uniform sampler2D	texture_sampler;
uniform vec2		char_size;
uniform vec2		font_size;
uniform int			emphasis;

bool	getPxl(vec2 p)
{
	vec3 col;

	col = texture(texture_sampler, p).rgb;
	return (col.x != 1.0f || col.y != 1.0f || col.z != 1.0f);
}

void	main(void)
{
	float	alpha;
	vec2	coords;

	alpha = 1.0f;
	coords = out_coords;
	if ((emphasis & 2) != 0)
		coords.x -= 0.5f * int(int(coords.y * char_size.y - 1) / 2) / char_size.x;
	if (coords.x * char_size.x > max_x || coords.x < 0 || coords.y < 0)
		alpha = 0.0f;
	bool b1 = getPxl((coords * char_size + uv) / font_size);
	if ((emphasis & 1) != 0)
	{
		bool b2 = getPxl((coords * char_size + uv - vec2(1, 0)) / font_size);
		if (!b1 && !b2)
			alpha = 0.0f;
	}
	else if (!b1)
		alpha = 0.0f;
	out_color = vec4(f_color, alpha * f_alpha);
}
