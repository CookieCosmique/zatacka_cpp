/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   UpTest.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Game.hpp"
#include "Game/Spawnable/Updatable/UpTest.hpp"

//Constructor and destructor
UpTest::UpTest(Game &game, float life, bool kill_game) : Updatable(game, 0)
{
	_compt = 0;
	_life = life;
	_killGame = kill_game;
}

UpTest::~UpTest(void)
{
	logger::sslog(LOG_FINE) << "Destroying " << *this << logger::endlog();
}
//Update overwrite
void	UpTest::update(const GameTime &game_time)
{
	this->_compt += game_time.getDiff() * game_time.getSpeed();
//	if (game_time.isRefreshing())
//		logger::sslog(LOG_DEBUG) << "life: " << this->_life << ", compt: " << this->_compt << logger::endlog();
	if(this->_compt > this->_life)
	{
		kill();
		if(this->_killGame)
			this->_game.stop();
	}
}

//Getters
float	UpTest::getCompt(void) const
{
	return (_compt);
}

//operator << overwrite
void	UpTest::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "UpTest" << C_RESET << ": { " << logger::tab(1) <<
			C_VAR << "life" << C_RESET << " = " << this->_life << ", \n" <<
			C_VAR << "compt" << C_RESET << " = " << this->_compt << ", \n" <<
			C_VAR << "killGame" << C_RESET << " = " << this->_killGame << ", \n";
	Spawnable::objectPrint(o);
	o << logger::tab(-1) << "}";
}