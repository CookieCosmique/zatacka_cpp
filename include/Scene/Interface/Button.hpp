/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Button.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-09 17:01:11 by bperreon            |\________\          */
/*   Updated: 2016/06/02 17:55:39 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef BUTTON_HPP
# define BUTTON_HPP

# include "MouseButtonControllable.hpp"
# include "CursorMoveControllable.hpp"
# include "Vec2.hpp"
# include "OpenGL.h"
# include "OpenGL/ShaderProgram/ButtonShaderProgram.hpp"
# include "OpenGL/VAO/ButtonVAO.hpp"
# include "Renderable.hpp"
# include "Scene.hpp"
# include "SceneComponent.hpp"
# include "Area.hpp"

template <typename T>
class	Button;

template <typename T>
class	Button :
		public agl::Area,
		public agl::MouseButtonControllable< Button<T> >,
		public agl::CursorMoveControllable,
		public agl::Renderable
{
private:
	T						&_target;

	ButtonShaderProgram		&_shaderProgram;
	ButtonVAO				*_vao;

	void 					(T::*_press)(void);
	void					(T::*_release)(void);

	bool					_pressed;
	bool					_hover;

protected:
	bool			collide(const double x, const double y) const
	{
		double		button_x;
		double		button_y;

		button_x = (this->_position.x - this->_camera.getPosition().x) /
			this->_camera.getSize().x;
		button_y = (this->_position.y - this->_camera.getPosition().y) /
			this->_camera.getSize().y;
		return (x >= button_x && x <= button_x + this->_size.x /
				this->_camera.getSize().x &&
				y >= button_y && y <= button_y + this->_size.y /
				this->_camera.getSize().y);
	}
	virtual void	onModif(void)
	{
		this->_vao->updatePositions(this->_position, this->_size);
	}

public:
	Button(agl::Scene &scene, agl::Area *area,
			ButtonShaderProgram &shaderProgram, agl::Inputs &inputs,
			T &target, void (T::*press)(void), void (T::*release)(void),
			const agl::Vec2 &position, const agl::Vec2 &size,
			const PositionType pos_type, const PositionType size_type) :
		agl::Area(scene, area, position, size, pos_type, size_type),
		agl::MouseButtonControllable< Button<T> >(inputs, *this),
		agl::CursorMoveControllable(inputs),
		agl::Renderable(scene.getRender(), scene.getCamera(), 3),
		_target(target),
		_shaderProgram(shaderProgram),
		_press(press), _release(release),
		_pressed(false), _hover(false)
	{
		this->_vao = new ButtonVAO(shaderProgram.getID(),
				this->_position, this->_size);
		this->linkMouseAction(inputs.getMouseButtonActionName(GLFW_MOUSE_BUTTON_LEFT,
						GLFW_PRESS), &Button::press);
		this->linkMouseAction(inputs.getMouseButtonActionName(GLFW_MOUSE_BUTTON_LEFT,
						GLFW_RELEASE), &Button::release);
	}
	virtual ~Button(void)
	{
		delete this->_vao;
		this->unlinkMouseAction(this->_inputs.getMouseButtonActionName(GLFW_MOUSE_BUTTON_LEFT,
						GLFW_PRESS));
		this->unlinkMouseAction(this->_inputs.getMouseButtonActionName(GLFW_MOUSE_BUTTON_LEFT,
						GLFW_RELEASE));
	}

	virtual void	press(void)
	{
		this->_pressed = true;
//		this->_shaderProgram.pressed();
		if (this->_press)
			(this->_target.*(this->_press))();
	}

	virtual void	release(void)
	{
		if (this->_pressed)
		{
			this->_pressed = false;
			if (this->_release)
				(this->_target.*(this->_release))();
		}
	}

	virtual void	hover(void)
	{
		this->_hover = true;
	}

	virtual void	stopHover(void)
	{
		this->_hover = false;
		this->_pressed = false;
	}

	void	cursorMove(const double x, const double y)
	{
		if (this->collide(x, y))
		{
			if(!this->_hover)
				this->hover();
		}
		else
		{
			if (this->_hover)
				this->stopHover();
		}
	}

	virtual void	render(void) const
	{
		this->_shaderProgram.use();
		this->_shaderProgram.updateCamera(this->_camera);
		this->_shaderProgram.setStatut(this->_hover ? this->_pressed ? 1 : 2 : 0);
		this->_vao->render();
	}

	virtual void	objectPrint(std::ostream &o) const
	{
		o << C_OBJ << "Button" << C_RESET << ": {" << agl::logger::tab(1);
		agl::MouseButtonControllable< Button<T> >::objectPrint(o);
		o << ",\n";
		Renderable::objectPrint(o);
		o << ",\n";
		o << C_VAR << "position" << C_RESET << " = " << this->_position << ",\n";
		o << C_VAR << "size" << C_RESET << " = " << this->_size << ",\n";
		o << C_VAR << "vao" << C_RESET << " = " << *this->_vao << ",\n";
		o << C_VAR << "shader program" << C_RESET << " = " << this->_shaderProgram;
		o << agl::logger::tab(-1) << "}";
	}
};

#endif
