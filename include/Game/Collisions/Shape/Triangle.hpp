/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Triangle.hpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:55:06 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:55:06 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef TRIANGLE_HPP
# define TRIANGLE_HPP

//# include "Game/Collisions/Shape/Polygon.hpp"
# include "Game/Collisions/Collider/TriangleCollider.hpp"

class	Triangle
{
private:
	const TriangleCollider	_collider;
public:
	Triangle(void);
	virtual	~Triangle(void);
	virtual const Collider	&getCollider(void) const;
	virtual bool			collide(const Collider &c) const;
};

#endif
