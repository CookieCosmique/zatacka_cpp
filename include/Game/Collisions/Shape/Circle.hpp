/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Circle.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:11:34 by Bastien             |\________\          */
/*   Updated: 2016-03-22 21:11:34 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CIRCLE_HPP
# define CIRCLE_HPP

# include "Game/Collisions/Shape/Shape.hpp"
# include "Game/Collisions/Collider/CircleCollider.hpp"

class	Circle : public Shape
{
private:
	const CircleCollider	_collider;
public:
	Circle(void);
	virtual	~Circle(void);
	virtual const Collider	&getCollider(void) const;
	virtual void			collide(const Collider &c) const;
};

#endif
