/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ChunkDiv.hpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:57:10 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHUNKDIV_HPP
# define CHUNKDIV_HPP

#include "Game/Collisions/ColliderComponent/ColliderComponent.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Lines/LinesContainer.hpp"
#include "Game/Game.hpp"
#include "SmartPointer.hpp"
#include <map>

class Arrow;
class LinesContainer;
class Lines;
class FlatBox;
class Game;
//class ColliderComponent;

class ChunkDiv : public Div
{
protected:
	std::map< void*, agl::SmartPointer<Arrow> >	_arrows;
	std::map< void*, Lines* >				_arrowsLines;
	std::map< void*, ColliderComponent& >	_components;
	agl::SmartPointer<LinesContainer>		_linesCont;

	Game		&_game;
public:
	ChunkDiv(Game &game, const FlatBox &downLeft);
//	ChunkDiv(const FlatBox &zone);
	virtual ~ChunkDiv(void);

	void	addArrow(Arrow *arrow);
	void	removeArrow(Arrow *arrow);
	void	removeArrowLines(Arrow *arrow);
	void	removePotentialLines(Lines *lines);

	void	addColliderComponent(ColliderComponent &component);
	void	removeColliderComponent(ColliderComponent &component);

	Lines	*newLines(const agl::Vec2 &newPos, Arrow *arrow);
	Lines	*newLines(const agl::Vec2 &oldPos, const agl::Vec2 &newPos,
			Arrow *arrow);
	Lines	*newLines(const agl::Vec2 &olderPos, const agl::Vec2 &oldPos,
			const agl::Vec2 &newPos, Arrow *arrow);
	
	bool	moveLines(const agl::Vec2 &newPos, Arrow *arrow);
	bool	resetTimer(Arrow *arrow);
	void	collide(ColliderComponent &component);
	void	collideStrech(ColliderComponent &component, const agl::Vec2 &delta);

	Game 	&getGame(void) const;

	
	virtual void	objectPrint(std::ostream &o) const;

};
#endif
