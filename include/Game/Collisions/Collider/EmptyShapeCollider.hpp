/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   EmptyShapeCollider.hpp                          |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:55:17 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:55:17 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef EMPTYSHAPECOLLIDER_HPP
# define EMPTYSHAPECOLLIDER_HPP

# include "Game/Collisions/Collider/Collider.hpp"

class	EmptyShapeCollider : public Collider
{
private:
	const EmptyShape	&_t;
public:
	EmptyShapeCollider(const EmptyShape &t);
	virtual	~EmptyShapeCollider(void);
//	virtual bool	collide(const Polygon &p) const;
//	virtual bool	collide(const Circle &c) const;
	virtual bool	collide(const Triangle &t) const;
	virtual bool	collide(const Rectangle &t) const;
	virtual bool	collide(const EmptyShape &t) const;
};

#endif