/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Div.hpp                                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:53:13 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIV_HPP
# define DIV_HPP

# include "Object.hpp"
# include "Utility/FlatBox.hpp"

//template <typename T>
//class DivContainer;

//class DivFactory;

class Div : public agl::Object
{
protected:
	FlatBox						_zone;
public:
//	Div(AbstractDivContainer &container,const Vec2 &downLeft);
	Div(const FlatBox &zone);
	virtual ~Div(void) {};

	FlatBox				getZone(void);

//	virtual void		addArrow(Arrow *arrow) {(void) arrow;};
//	virtual void		removeArrow(Arrow *arrow) {(void) arrow;};

	virtual void		objectPrint(std::ostream &o) const;
};

#endif
