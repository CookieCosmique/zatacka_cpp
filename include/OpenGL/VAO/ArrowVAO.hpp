/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ArrowVAO.hpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-31 06:15:44 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:26:33 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARROW_VAO_HPP
# define ARROW_VAO_HPP

# include "Vec2.hpp"
# include "VAO.hpp"

class	ArrowVAO : public agl::VAO
{
private:
	GLuint		_vboPosition;
	GLuint		_vboDirection;

public:
	ArrowVAO(const GLuint program_id, const agl::Vec2 &position,
			const agl::Vec2 &direction);
	virtual	~ArrowVAO(void);

	void			updatePosition(const agl::Vec2 &position) const;
	void			updateDirection(const agl::Vec2 &direction) const;

	virtual void	render(void) const;

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
