/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ChunkDivFactory.hpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHUNKDIVFACTORY_HPP
# define CHUNKDIVFACTORY_HPP

#include "Game/Div/DivFactory.hpp"
#include "Game/Div/DivContainer/AbstractDivContainer.hpp"

class ChunkDiv;
class Game;

class ChunkDivFactory : public DivFactory<ChunkDiv>
{
private:
	Game	&_game;

public:
	ChunkDivFactory(Game &game);
	virtual	~ChunkDivFactory(void) {};

//	virtual Div	*make(DivContainer &container,const Vec2 &downLeft);
	virtual ChunkDiv	*make(const FlatBox &zone);
};

#endif