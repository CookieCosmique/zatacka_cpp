# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bperreon <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/22 17:51:18 by bperreon          #+#    #+#              #
#    Updated: 2016/06/04 16:33:06 by bperreon         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = zatacka

UNAME = $(shell uname -s)

CC = g++
CFLAGS = -Wall -Wextra -Werror -std=gnu++11

LIB_FOLDER = ./lib/
AGL = ./agl/lib/libagl.a
ifneq (,$(findstring CYGWIN,$(UNAME)))
	LIB = -L$(LIB_FOLDER) -lglfw3 -lgdi32 -lglew32 -lopengl32 -lagl
else ifneq (,$(findstring Darwin,$(UNAME)))
	LIB = -L$(LIB_FOLDER) -lglfw3 -lagl \
		  -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo
endif
HEADERS = -I./agl/external/glfw/include -I./agl/external/glew/include/GL/ \
		  -I./include -I./agl/include
SOURCE_FOLDER = ./src/
SOURCE_FILES =	main.cpp \
				Game/Game.cpp \
				Game/Player/Player.cpp \
				Game/Entity/Entity.cpp \
				Game/Entity/Arrow.cpp \
				Game/Collisions/Collisions.cpp \
				Game/Collisions/Shape/Triangle.cpp \
				Game/Collisions/Shape/Rectangle.cpp \
				Game/Collisions/Shape/EmptyShape.cpp \
				Game/Collisions/Shape/Shape.cpp \
				Game/Collisions/Collider/TriangleCollider.cpp \
				Game/Collisions/Collider/RectangleCollider.cpp \
				Game/Collisions/Collider/EmptyShapeCollider.cpp \
				Game/Collisions/ColliderComponent/ColliderComponent.cpp \
				Game/Camera/ManualCamera.cpp \
				Game/Div/DivContainer.cpp \
				Game/Div/DivContainerArray.cpp \
				Game/Div/DivIterator.cpp \
				Game/Div/DivArrayIterator.cpp \
				Game/Div/Div.cpp \
				Game/Div/ChunkDiv.cpp \
				Game/Div/DivGroup.cpp \
				Game/Div/DivSet.cpp \
				Game/Div/GroupDivIterator.cpp \
				Game/Div/DivContainer/AbstractDivContainer.cpp \
				Game/Div/DivToDo/DivToDo.cpp \
				Game/Div/DivToDo/DivToDoArrowCurr.cpp \
				Game/Div/DivToDo/DivToDoArrowNew.cpp \
				Game/Div/DivToDo/DivToDoArrowOld.cpp \
				Game/Div/DivToDo/DivToDoTest.cpp \
				Game/Div/DivToDo/DivToDoPopAPoint.cpp \
				Game/Div/DivToDo/DivToCollide.cpp \
				Game/Div/DivToDo/DivToCollideStrech.cpp \
				Game/Div/ChunkDivFactory.cpp \
				Game/HUD/TimerHUD.cpp \
				Game/Lines/LinesContainer.cpp \
				Game/Lines/Lines.cpp \
				Game/Lines/LinesMemory/LinesMemory.cpp \
				Game/Lines/LinesMemory/MemIterator.cpp \
				Game/Lines/LinesMemory/LinkIterator.cpp \
				Menu/Menu.cpp \
				Menu/MenuMainArea.cpp \
				OpenGL/ShaderProgram/ArrowShaderProgram.cpp \
				OpenGL/ShaderProgram/ButtonShaderProgram.cpp \
				OpenGL/ShaderProgram/CameraShaderProgram.cpp \
				OpenGL/ShaderProgram/LineShaderProgram.cpp \
				OpenGL/ShaderProgram/TextShaderProgram.cpp \
				OpenGL/ShaderProgram/TexturedComponentShaderProgram.cpp \
				OpenGL/VAO/ArrowVAO.cpp \
				OpenGL/VAO/ButtonVAO.cpp \
				OpenGL/VAO/LineVAO.cpp \
				OpenGL/VAO/TexturedComponentVAO.cpp \
				OpenGL/VAO/TextVAO.cpp \
				OpenGL/VAO/SquareVAO.cpp \
				Scene/Interface/TextComponent.cpp \
				Scene/Interface/TexturedComponent.cpp \
				Utility/Flags.cpp \
				Utility/FlatBox.cpp \
				Utility/SmartArray.cpp

#				Game/Collisions/Shape/Circle.cpp \
#				Game/Collisions/Shape/Polygon.cpp \
#				Game/Collisions/Shape/Rectangle.cpp \
#				Game/Collisions/Collider/CircleCollider.cpp \
#				Game/Collisions/Collider/PolygonCollider.cpp \
#				Game/Collisions/Collider/RectangleCollider.cpp \

SOURCE = $(addprefix $(SOURCE_FOLDER), $(SOURCE_FILES))

OBJ = $(SOURCE:.cpp=.o)

DOX_CONFIG = doxygen.config
# ALL

ifneq (,$(findstring CYGWIN,$(UNAME)))
all: OSNAME $(LIB_FOLDER) $(AGL) $(NAME)
else ifneq (,$(findstring Darwin,$(UNAME)))
all: OSNAME $(LIB_FOLDER) $(AGL) $(NAME)
else
all : OSNAME ERROR
endif

OSNAME:
	@echo "current os : " $(UNAME)

ERROR:
	@echo "no rules for make on this os"

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(LIB)

%.o: %.cpp
	@$(CC) $(CFLAGS) $(HEADERS) -c $< -o $@
	@echo "[CC] " $<

# LIB

$(LIB_FOLDER):
	mkdir -p $@


IFSEQ:
ifneq (,$(findstring CYGWIN,$(UNAME)))
	@ echo "its cygwin"
else
	@ echo "its NOT cygwin"
endif



#ifneq (,$(findstring CYGWIN,$(UNAME)))
#$(GLFW):
#	git submodule init glfw
#	git submodule update glfw
#	cd glfw && cmake . -DCMAKE_LEGACY_CYGWIN_WIN32=1 && make -j 8 -i && cd ..
#	cp $(GLFW) $(LIB_FOLDER)
#else
#$(GLFW):
#	git submodule init glfw
#	git submodule update glfw
#	cd glfw && cmake . && make -j 8 && cd ..
#	cp $(GLFW) $(LIB_FOLDER)
#endif

#ifneq (,$(findstring CYGWIN,$(UNAME)))
#$(GLEW):
#	git submodule init glew
#	git submodule update glew
#	make -C glew/auto
#	mv glew/src/glew.c glew/src/glew_old.c
#	echo "#define _WIN32" > glew/src/glew.c
#	cat glew/src/glew_old.c >> glew/src/glew.c
#	rm glew/src/glew_old.c
#	cd glew/build/cmake && cmake . -DCMAKE_LEGACY_CYGWIN_WIN32=1 && make -j 8 -i && cd ../../..
#	cp $(GLEW) $(LIB_FOLDER)
#else

#endif

$(AGL):
	git submodule init agl
	git submodule update agl
	cd agl && git submodule init && git submodule update && cd ..
	mkdir -p agl/build/ && cd agl/build/ && cmake .. && make && cd ../..
	cp $(AGL) $(LIB_FOLDER)
	cp agl/lib/libagl.a agl/lib/libglfw3.a $(LIB_FOLDER)

doc:
	doxygen $(DOX_CONFIG)


# CLEAN - FCLEAN - RE

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
