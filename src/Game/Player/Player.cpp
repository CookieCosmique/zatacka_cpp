/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Player.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-19 16:26:02 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:59:18 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Player/Player.hpp"

using namespace agl;

Player::Player(Game &game, Camera &camera, ArrowShaderProgram &shader_program,
		Inputs &inputs) :
	Arrow(game, camera, shader_program),
	KeyboardControllable(inputs, *this)
{
	linkInput(GLFW_KEY_RIGHT, GLFW_PRESS, std::string("StartRotationRight"), &Player::startRotationRight);
	linkInput(GLFW_KEY_RIGHT, GLFW_RELEASE, std::string("StopRotationRight"), &Player::stopRotationRight);
	linkInput(GLFW_KEY_LEFT, GLFW_PRESS, std::string("StartRotationLeft"), &Player::startRotationLeft);
	linkInput(GLFW_KEY_LEFT, GLFW_RELEASE, std::string("StopRotationLeft"), &Player::stopRotationLeft);
	linkInput(GLFW_KEY_UP, GLFW_PRESS, std::string("StartSpeed"), &Player::startSpeed);
	linkInput(GLFW_KEY_UP, GLFW_RELEASE, std::string("StopSpeed"), &Player::stopSpeed);
	logger::sslog(LOG_DEBUG) << C_OBJ << "Player" << C_RESET << " created." << logger::endlog();
}

Player::~Player(void)
{
	unlinkInput(GLFW_KEY_RIGHT, GLFW_PRESS, std::string("StartRotationRight"));
	unlinkInput(GLFW_KEY_RIGHT, GLFW_RELEASE, std::string("StopRotationRight"));
	unlinkInput(GLFW_KEY_LEFT, GLFW_PRESS, std::string("StartRotationLeft"));
	unlinkInput(GLFW_KEY_LEFT, GLFW_RELEASE, std::string("StopRotationLeft"));
	unlinkInput(GLFW_KEY_UP, GLFW_PRESS, std::string("StartSpeed"));
	unlinkInput(GLFW_KEY_UP, GLFW_RELEASE, std::string("StopSpeed"));
	logger::sslog(LOG_DEBUG) << C_OBJ << "Player" << C_RESET << " deleted." << logger::endlog();
}

// Print

void	Player::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Player" << C_RESET << ": { " << logger::tab(1);
	Arrow::objectPrint(o);
	o << ",\n";
	KeyboardControllable::objectPrint(o);
	o << logger::tab(-1) << "}";
}
