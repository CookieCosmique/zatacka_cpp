/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   main.cpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-15 03:46:21 by Bastien             |\________\          */
/*   Updated: 2016/06/04 16:37:11 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Environment.hpp"
#include "Game/Game.hpp"
#include "OpenGL/ShaderProgram/ArrowShaderProgram.hpp"
#include "Game/Player/Player.hpp"
#include "Menu/Menu.hpp"
#include "Utility/Flags.hpp"

using namespace agl;

void		init_flags(int ac, char **av)
{
	Flags flags(ac, av);

	if (flags.hasFlag(Flags::F_LOG_DEBUG))
		logger::logger.addFlag(F_LOG_DEBUG);
	if (flags.hasFlag(Flags::F_LOG_TIME))
		logger::logger.addFlag(F_LOG_TIME);
}

void		run(Environment &env)
{
	ArrowShaderProgram		shader_program;
	Menu					*menu;
	Game					*game;
	int						ret;

	while (1)
	{
		menu = new Menu(env);
		ret = menu->run();
		delete menu;
		if (ret == 0)
			break ;
		game = new Game(env);
		new Player(*game, game->getGameCamera(), shader_program,
				env.getInputs());
		game->run();
		delete game;
	}
}

int		main(int ac, char **av)
{
	try
	{
		init_flags(ac, av);
		Environment &env = Environment::getInstance();
		env.init();
		run(env);
	}
	catch (const std::exception &e)
	{
		logger::sslog(LOG_ERROR) << e.what() << logger::endlog();
		return (1);
	}
}
