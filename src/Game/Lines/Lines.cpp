/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Lines.cpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:15:33 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Lines/Lines.hpp"
#include "Game/Lines/LinesContainer.hpp"
#include "Game/Lines/LinesMemory/MemIterator.hpp"
#include "Game/Lines/LinesMemory/LinkIterator.hpp"
#include "Game/Div/ChunkDiv.hpp"

using namespace agl;

Lines::Lines(LinesContainer *container, const Vec2 &start, const Vec2 &end, int num, void *parent)
{
	_playerNum = num;
	_parentKey = parent;

	_container = container;
	_previous = NULL;
	_next = NULL;

	_outerStart	= _container->getMemory().request(start, _playerNum);
	_start		= _container->getMemory().request(start, _playerNum);
	_end		= _container->getMemory().request(end, _playerNum);
	_outerEnd	= _container->getMemory().request(end, _playerNum);

	_startLink	= _container->getMemory().link(_outerStart, _start, _end);
	_endLink	= _container->getMemory().link(_start, _end, _outerEnd);

	_container->getAllLines()->push_front(this);
	_posInAll = _container->getAllLines()->begin();
	
/*	logger::sslog(LOG_DEBUG) << "From scratch lines constructor !" << logger::endlog();
	logger::sslog(LOG_DEBUG) << "is first ? :" << isFirst() << logger::endlog();
	logger::sslog(LOG_DEBUG) << "is last ? :" << isLast() << logger::endlog();*/
//	logger::sslog(LOG_ERROR) << "NEW LINE !" << start << " / " << end << logger::endlog();
}

Lines::Lines(LinesContainer *container, const Vec2 &outerStart, const Vec2 &start, const Vec2 &end, int num, void *parent)
{
	_playerNum = num;
	_parentKey = parent;

	_container = container;
	_previous = NULL;
	_next = NULL;

	_outerStart	= _container->getMemory().request(outerStart, _playerNum);
	_start		= _container->getMemory().request(start, _playerNum);
	_end		= _container->getMemory().request(end, _playerNum);
	_outerEnd	= _container->getMemory().request(end, _playerNum);

	_startLink	= _container->getMemory().link(_outerStart, _start, _end);
	_endLink	= _container->getMemory().link(_start, _end, _outerEnd);

	_container->getAllLines()->push_front(this);
	_posInAll = _container->getAllLines()->begin();
	
/*	logger::sslog(LOG_DEBUG) << "From scratch lines constructor !" << logger::endlog();
	logger::sslog(LOG_DEBUG) << "is first ? :" << isFirst() << logger::endlog();
	logger::sslog(LOG_DEBUG) << "is last ? :" << isLast() << logger::endlog();*/
//	logger::sslog(LOG_ERROR) << "NEW LINE !" << start << " / " << end << logger::endlog();
}

Lines::Lines(LinesContainer *container, const Vec2 &newPos, const PreviousLines &previous)
{
//	logger::sslog(LOG_DEBUG) << "New line, from previous one : " << previous.getLines()->_start.getIndex() << " / " << previous.getLines()->_end.getIndex() << logger::endlog();
	_playerNum = previous.getLines()->getNum();
	_parentKey = previous.getLines()->getParentKey();

	_container = container;
	_previous = previous.getLines();
	_next = NULL;
	_previous->_next = this;

	_outerStart	= _previous->_start;
	_start		= _previous->_end;
	_end		= _previous->_outerEnd.move(newPos);
//	_end		= _previous->_outerEnd.set(newPos, _playerNum);
	_outerEnd	= _container->getMemory().request(newPos, _playerNum);

	_startLink	= _previous->_endLink;
	_endLink	= _container->getMemory().link(_start, _end, _outerEnd);

	_container->getAllLines()->push_front(this);
	_posInAll = _container->getAllLines()->begin();
//	logger::sslog(LOG_DEBUG) << "New line, new val : " << this->_start.getIndex() << " / " << this->_end.getIndex() << logger::endlog();
//	logger::sslog(LOG_ERROR) << "NEW LINE !" << getStart() << " / " << getEnd() << logger::endlog();

}

Lines::Lines(LinesContainer *container, const Vec2 &newPos, const NextLines &next)
{
	_playerNum = next.getLines()->getNum();
	_parentKey = next.getLines()->getParentKey();

	_container = container;
	_previous = NULL;
	_next = next.getLines();
	_next->_previous = this;

	_outerStart	= _container->getMemory().request(newPos, _playerNum);
	_start		= _next->_outerStart.move(newPos);
//	_start		= _next->_outerstart.set(newPos, _playerNum);
	_end		= _next->_start;
	_outerEnd	= _next->_end;

	_startLink	= _container->getMemory().link(_outerStart, _start, _end);
	_endLink	= _next->_startLink;

	_container->getAllLines()->push_front(this);
	_posInAll = _container->getAllLines()->begin();
//	logger::sslog(LOG_DEBUG) << "New line, new val : " << this->_start.getIndex() << " / " << this->_end.getIndex() << logger::endlog();
//	logger::sslog(LOG_ERROR) << "NEW LINE !" << getStart() << " / " << getEnd() << logger::endlog();
}

Lines::~Lines(void)
{
	if (_previous)
		_previous->noMoreNext();
	else
	{
		_container->getMemory().free(_startLink);
		_container->getMemory().free(_outerStart);
	}
	if (_next)
		_next->noMorePrevious();
	else
	{
		_container->getMemory().free(_endLink);
		_container->getMemory().free(_outerEnd);
	}

	if (!_previous && !_next)
	{
		_container->getMemory().free(_start);
		_container->getMemory().free(_end);
	}

	_container->getAllLines()->erase(_posInAll);
	if(_container->isAlive())
		_container->getDiv().removePotentialLines(this); 	//problem is : chunk div have already been destroyed, and lines container is destroyed after with renderable
														//so div doesnt fking exist when trying to remove potentialLines
}

void	Lines::noMoreNext(void)
{
	_next = NULL;
	_outerEnd.set(_end);
}

void	Lines::noMorePrevious(void)
{
	_previous = NULL;
	_outerStart.set(_start);
}

bool			Lines::isFirst(void) const
{
	return (_previous == NULL);
}

bool			Lines::isLast(void) const
{
	return (_next == NULL);
}

LinesContainer	*Lines::getContainer(void) const
{
	return (_container);
}

Vec2			Lines::getStart(void) const
{
	return (_start.getPos());
}

Vec2			Lines::getEnd(void) const
{
	return (_end.getPos());
}

int				Lines::getNum(void) const
{
	return (_playerNum);
}

void			*Lines::getParentKey(void) const
{
	return (_parentKey);
}

double			Lines::getTimer(void) const
{
	return (_timer);
}

void			Lines::moveStart(const Vec2 &newPos)
{
	if(isFirst())
		_outerStart.move(newPos);
	_start.move(newPos);
}

void			Lines::moveEnd(const Vec2 &newPos)
{
	if(isLast())
		_outerEnd.move(newPos);
	_end.move(newPos);
//	logger::sslog(LOG_DEBUG) << "MOVED !" << getStart() << " / " << getEnd() << logger::endlog();
}

void			Lines::resetTimer(void)
{
	_timer = _container->getDiv().getGame().getTime().getTime();
}
