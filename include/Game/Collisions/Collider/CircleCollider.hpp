/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   CircleCollider.hpp                              |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:13:09 by Bastien             |\________\          */
/*   Updated: 2016-03-22 21:13:09 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CIRCLE_COLLIDER_HPP
# define CIRCLE_COLLIDER_HPP

# include "Game/Collisions/Collider/Collider.hpp"

class	CircleCollider : public Collider
{
private:
	const Circle	&_c;
public:
	CircleCollider(const Circle &c);
	virtual	~CircleCollider(void);
	virtual void	collide(const Polygon &p) const;
	virtual void	collide(const Circle &c) const;
	virtual void	collide(const Triangle &t) const;
};

#endif
