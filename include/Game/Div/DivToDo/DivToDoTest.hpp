/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoTest.hpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVTODOTEST_HPP
# define DIVTODOTEST_HPP

#include "Game/Div/DivToDo/DivToDo.hpp"

class Div;
class ChunkDiv;

template <typename T>
class DivToDoTest : public DivToDo<T>
{
public:
	DivToDoTest(void);
	virtual	~DivToDoTest(void) {};

	virtual void 	doTo(T &div);
	void			doDiv(Div &div);
};

#endif

