/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Player.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-19 16:21:35 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:28:26 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER_HPP
# define PLAYER_HPP

# include "Game/Entity/Arrow.hpp"
# include "KeyboardControllable.hpp"

class	Player;

class	Player : public Arrow, public agl::KeyboardControllable<Player>
{
public:
	Player(Game &game, agl::Camera &camera, ArrowShaderProgram &shader_program,
			agl::Inputs &inputs);
	~Player(void);
	virtual void	objectPrint(std::ostream &o) const;
};

#endif
