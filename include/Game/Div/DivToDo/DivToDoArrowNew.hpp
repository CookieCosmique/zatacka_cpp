/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoArrowNew.hpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVTODOARROWNEW_HPP
# define DIVTODOARROWNEW_HPP

#include "Game/Div/DivToDo/DivToDo.hpp"
#include "Game/Entity/Arrow.hpp"

class ChunkDiv;

class DivToDoArrowNew : public DivToDo<ChunkDiv>
{
private:
	Arrow	&_arrow;
public:
	DivToDoArrowNew(Arrow &arrow);
	virtual	~DivToDoArrowNew(void) {};

	virtual void 	doTo(ChunkDiv &div);
};

#endif