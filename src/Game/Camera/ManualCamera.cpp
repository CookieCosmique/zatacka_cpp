/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ManualCamera.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-31 03:46:53 by Bastien             |\________\          */
/*   Updated: 2016/06/04 16:48:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cmath>
#include "Game/Camera/ManualCamera.hpp"
#include "Environment.hpp"

using namespace agl;

// Constructor - Destructor

ManualCamera::ManualCamera(const Vec2 &position, const Vec2 &size, float speed, Inputs &inputs, Update &update) :
		Camera(position, size),
		Updatable(update, 0),
		KeyboardControllable(inputs, *this),
		_defaultPosition(position),
		_defaultSize(size)
{
	this->_state = 0;
	this->_speed = speed;
	linkInput(GLFW_KEY_W, GLFW_PRESS, std::string("StartCameraMoveUp"), &ManualCamera::startMoveUp);
	linkInput(GLFW_KEY_W, GLFW_RELEASE, std::string("StopCameraMoveUp"), &ManualCamera::stopMoveUp);
	linkInput(GLFW_KEY_S, GLFW_PRESS, std::string("StartCameraMoveDown"), &ManualCamera::startMoveDown);
	linkInput(GLFW_KEY_S, GLFW_RELEASE, std::string("StopCameraMoveDown"), &ManualCamera::stopMoveDown);
	linkInput(GLFW_KEY_A, GLFW_PRESS, std::string("StartCameraMoveLeft"), &ManualCamera::startMoveLeft);
	linkInput(GLFW_KEY_A, GLFW_RELEASE, std::string("StopCameraMoveLeft"), &ManualCamera::stopMoveLeft);
	linkInput(GLFW_KEY_D, GLFW_PRESS, std::string("StartCameraMoveRight"), &ManualCamera::startMoveRight);
	linkInput(GLFW_KEY_D, GLFW_RELEASE, std::string("StopCameraMoveRight"), &ManualCamera::stopMoveRight);
	linkInput(GLFW_KEY_Z, GLFW_PRESS, std::string("StartCameraZoonIn"), &ManualCamera::startZoomIn);
	linkInput(GLFW_KEY_Z, GLFW_RELEASE, std::string("StopCameraZoonIn"), &ManualCamera::stopZoomIn);
	linkInput(GLFW_KEY_X, GLFW_PRESS, std::string("StartCameraZoomOut"), &ManualCamera::startZoomOut);
	linkInput(GLFW_KEY_X, GLFW_RELEASE, std::string("StopCameraZoomOut"), &ManualCamera::stopZoomOut);
	logger::sslog(LOG_DEBUG) << "ManualCamera created: " << *this << logger::endlog();
}

ManualCamera::~ManualCamera(void)
{
	unlinkInput(GLFW_KEY_W, GLFW_PRESS, std::string("StartCameraMoveUp"));
	unlinkInput(GLFW_KEY_W, GLFW_RELEASE, std::string("StopCameraMoveUp"));
	unlinkInput(GLFW_KEY_S, GLFW_PRESS, std::string("StartCameraMoveDown"));
	unlinkInput(GLFW_KEY_S, GLFW_RELEASE, std::string("StopCameraMoveDown"));
	unlinkInput(GLFW_KEY_A, GLFW_PRESS, std::string("StartCameraMoveLeft"));
	unlinkInput(GLFW_KEY_A, GLFW_RELEASE, std::string("StopCameraMoveLeft"));
	unlinkInput(GLFW_KEY_D, GLFW_PRESS, std::string("StartCameraMoveRight"));
	unlinkInput(GLFW_KEY_D, GLFW_RELEASE, std::string("StopCameraMoveRight"));
	unlinkInput(GLFW_KEY_Z, GLFW_PRESS, std::string("StartCameraZoonIn"));
	unlinkInput(GLFW_KEY_Z, GLFW_RELEASE, std::string("StopCameraZoonIn"));
	unlinkInput(GLFW_KEY_X, GLFW_PRESS, std::string("StartCameraZoomOut"));
	unlinkInput(GLFW_KEY_X, GLFW_RELEASE, std::string("StopCameraZoomOut"));
	logger::sslog(LOG_DEBUG) << "ManualCamera destroyed." << logger::endlog();
}

// Member functions

void			ManualCamera::update(const GameTime &game_time)
{
	float		time_exp;

	if (this->_state)
	{
		if (this->_state & ManualCamera::MOVE_UP)
			this->_position += Vec2(0, this->_speed * this->_size.y / this->_defaultSize.y * game_time.getDiff());
		if (this->_state & ManualCamera::MOVE_DOWN)
			this->_position += Vec2(0, -this->_speed * this->_size.y / this->_defaultSize.y * game_time.getDiff());
		if (this->_state & ManualCamera::MOVE_LEFT)
			this->_position += Vec2(-this->_speed * this->_size.x / this->_defaultSize.x * game_time.getDiff(), 0);
		if (this->_state & ManualCamera::MOVE_RIGHT)
			this->_position += Vec2(this->_speed * this->_size.x / this->_defaultSize.x * game_time.getDiff(), 0);
		if (this->_state & ManualCamera::ZOOM_IN)
		{
			time_exp = exp(-game_time.getDiff());
			this->_position -= this->_size * ((time_exp - 1) / 2);
			this->_size *= time_exp;
		}
		if (this->_state & ManualCamera::ZOOM_OUT)
		{
			time_exp = exp(game_time.getDiff());
			this->_position -= this->_size * ((time_exp - 1) / 2);
			this->_size *= time_exp;
		}
	}
}

void			ManualCamera::startMoveUp(void)
{
	this->_state |= ManualCamera::MOVE_UP;
}

void			ManualCamera::startMoveDown(void)
{
	this->_state |= ManualCamera::MOVE_DOWN;
}

void			ManualCamera::startMoveLeft(void)
{
	this->_state |= ManualCamera::MOVE_LEFT;
}

void			ManualCamera::startMoveRight(void)
{
	this->_state |= ManualCamera::MOVE_RIGHT;
}

void			ManualCamera::startZoomIn(void)
{
	this->_state |= ManualCamera::ZOOM_IN;
}

void			ManualCamera::startZoomOut(void)
{
	this->_state |= ManualCamera::ZOOM_OUT;
}

void			ManualCamera::stopMoveUp(void)
{
	this->_state &= ~ManualCamera::MOVE_UP;
}

void			ManualCamera::stopMoveDown(void)
{
	this->_state &= ~ManualCamera::MOVE_DOWN;
}

void			ManualCamera::stopMoveLeft(void)
{
	this->_state &= ~ManualCamera::MOVE_LEFT;
}

void			ManualCamera::stopMoveRight(void)
{
	this->_state &= ~ManualCamera::MOVE_RIGHT;
}

void			ManualCamera::stopZoomIn(void)
{
	this->_state &= ~ManualCamera::ZOOM_IN;
}

void			ManualCamera::stopZoomOut(void)
{
	this->_state &= ~ManualCamera::ZOOM_OUT;
}

// Print

void			ManualCamera::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "ManualCamera" << C_RESET << ": { " << logger::tab(1);
	Camera::objectPrint(o);
	o << ",\n";
	Updatable::objectPrint(o);
	o << ",\n";
	KeyboardControllable::objectPrint(o);
	o << logger::tab(-1) << "}";
}
