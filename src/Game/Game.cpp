/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Game.cpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/04 16:48:07 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Environment.hpp"
#include "Game/Game.hpp"
#include "Game/Div/DivContainerArray.hpp"
#include "Game/Div/ChunkDivFactory.hpp"
#include "Game/Div/DivIterator.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Div/DivGroup.hpp"
#include "Game/Div/DivToDo/DivToDoTest.hpp"
#include "Utility/FlatBox.hpp"
#include "Vec2.hpp"
#include "OpenGL/ShaderProgram/LineShaderProgram.hpp"

using namespace agl;

// Constructor - Destructor

Game::Game(Environment &env) : Scene(env.getWindow()),
	KeyboardControllable(env.getInputs(), *this)
{
	logger::sslog(LOG_FINE) << "Starting " << C_OBJ << "Game" << C_RESET << " creation ..." << logger::endlog(1);
	this->_gameCamera = new ManualCamera(Vec2(0, 0), Vec2(1280, 720), 325.0f, env.getInputs(), *(this->_update));
	this->_lineShaderProgram = new LineShaderProgram();
	this->_container = new DivContainerArray<ChunkDiv>(10, 10, Vec2(0.0, 0.0), Vec2(1000.0, 1000.0), new ChunkDivFactory(*this));
	linkInput(GLFW_KEY_ESCAPE, GLFW_PRESS, std::string("StopGame"), &Game::stop);

	this->_font = new FontTexture("assets/font.bmp", 8, 8, 0, 256);
//	this->_font = new FontTexture("assets/font2.bmp", 14, 16, '!' - 1, 224);
	this->_timer = new TimerHUD(*this, *this->_font, this->_textShaderProgram);
	logger::sslog(LOG_FINE, -1) << C_OBJ << "Game" << C_RESET << " created successfully" << logger::endlog();
}

Game::~Game(void)
{
	logger::sslog(LOG_FINE) << "Starting " << C_OBJ << "Game" << C_RESET << " deletion ..." << logger::endlog(1);

	unlinkInput(GLFW_KEY_ESCAPE, GLFW_PRESS, std::string("StopGame"));

	delete this->_container;
	delete this->_lineShaderProgram; //since scene exists, container is destroyed to soon, maybe should have the possibilite to destroy objects, and remove them from update
	this->_timer->kill();

	logger::sslog(LOG_FINE, -1) << C_OBJ << "Game" << C_RESET << " deleted." << logger::endlog();
}

// Member functions

int		Game::run(void)
{
	logger::sslog(LOG_FINE) << "Game start running" << logger::endlog();

	logger::sslog(LOG_FINE) << *_container->getAnchor(Vec2(50.5154,745.416)) << logger::endlog();
	DivGroup<ChunkDiv>	grp = _container->getGroup(FlatBox(Vec2(50,96.5), Vec2(154,687.4)));
	logger::sslog(LOG_FINE) << "UN GROUPDIV : " << grp << logger::endlog();
	DivToDoTest<ChunkDiv>	doThings;
	doThings.forEach(grp);

	Scene::run();

	logger::sslog(LOG_FINE) << "Game stop running" << logger::endlog();
	return (0);
}

// Accessors

DivContainer<ChunkDiv>	&Game::getContainer(void)
{
	return (*this->_container);
}

ManualCamera			&Game::getGameCamera(void)
{
	return (*this->_gameCamera);
}

LineShaderProgram		&Game::getLineShaderProgram(void)
{
	return (*(this->_lineShaderProgram));
}

// Print

void	Game::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Game" << C_RESET << ": {" << logger::tab(1);
	Scene::objectPrint(o);
	o << logger::tab(-1) << "}";
}
