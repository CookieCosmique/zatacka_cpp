/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Entity.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-29 21:23:47 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:59:37 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Entity/Entity.hpp"
#include "Game/Game.hpp"

using namespace agl;

// Constructors - Destructors

Entity::Entity(Game &game, Camera &camera, const int level_update, const int level_render) :
		Spawnable(), Updatable(game.getUpdate(), level_update),
		Renderable(game.getRender(), camera, level_render)
{

}

Entity::~Entity(void)
{

}

// Accessors

const Vec2	&Entity::getPosition(void) const
{
	return (this->_position);
}

// Print

void	Entity::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Entity" << C_RESET << ": {" << logger::tab(1);
	Updatable::objectPrint(o);
	o << ",\n";
	Renderable::objectPrint(o);
	o << logger::tab(-1) << "}";
}
