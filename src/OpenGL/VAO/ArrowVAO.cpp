/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ArrowVAO.cpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-31 06:36:59 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:24:23 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OpenGL/VAO/ArrowVAO.hpp"
#include "OpenGL.h"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

ArrowVAO::ArrowVAO(const GLuint program_id, const Vec2 &position,
		const Vec2 &direction)
{
	this->_vboPosition = loadVBO(program_id, sizeof(GLfloat) * 2, position.getXY(),
			GL_FLOAT, 1, "position", 2, GL_DYNAMIC_DRAW);
	this->_vboDirection = loadVBO(program_id, sizeof(GLfloat) * 2, direction.getXY(),
			GL_FLOAT, 2, "direction", 2, GL_DYNAMIC_DRAW);
}

ArrowVAO::~ArrowVAO(void)
{

}

// Member functions

void		ArrowVAO::updatePosition(const Vec2 &position) const
{
	updateBufferData(GL_ARRAY_BUFFER, this->_vboPosition, sizeof(GLfloat) * 2,
			position.getXY());
}

void		ArrowVAO::updateDirection(const Vec2 &direction) const
{
	updateBufferData(GL_ARRAY_BUFFER, this->_vboDirection, sizeof(GLfloat) * 2,
			direction.getXY());
}

void		ArrowVAO::render(void) const
{
	this->bind();
	glDrawArrays(GL_POINTS, 0, 1);
}

// Print

void		ArrowVAO::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "ArrowVAO" << C_RESET << ": {" << logger::tab(1);
	VAO::objectPrint(o);
	o << ", ";
	o << C_VAR << "vboPosition" << C_RESET << " = " << this->_vboPosition << ", ";
	o << C_VAR << "vboDirection" << C_RESET << " = " << this->_vboDirection;
	o << logger::tab(-1) << "}";
}
