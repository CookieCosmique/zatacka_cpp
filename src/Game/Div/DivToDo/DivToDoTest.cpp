/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoTest.cpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:13:02 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivToDo/DivToDoTest.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"

using namespace agl;

template <typename T>
DivToDoTest<T>::DivToDoTest(void) {}

template <typename T>
void	DivToDoTest<T>::doTo(T &div)
{
	doDiv(div);
}

template <typename T>
void	DivToDoTest<T>::doDiv(Div &div)
{
	logger::sslog(LOG_DEBUG) << "Stuff on : " << div << logger::endlog();
}


template class DivToDoTest<Div>;
template class DivToDoTest<ChunkDiv>;
