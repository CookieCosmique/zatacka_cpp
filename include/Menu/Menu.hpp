/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Menu.hpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 09:29:29 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:26:23 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MENU_HPP
# define MENU_HPP

# include "Environment.hpp"
# include "Scene.hpp"
# include "OpenGL/ShaderProgram/ButtonShaderProgram.hpp"
# include "KeyboardControllable.hpp"
# include "Menu/MenuMainArea.hpp"
# include "SmartPointer.hpp"
# include "TexturedComponent.hpp"

class	Menu : public agl::Scene, public agl::KeyboardControllable<Menu>
{
private:
	agl::SmartPointer<MenuMainArea>				_mainArea;
	agl::SmartPointer<agl::TexturedComponent>	_background;
	agl::SmartPointer<agl::TexturedComponent>	_logo;

	int								_returnValue;

public:
	Menu(agl::Environment &env);
	~Menu(void);

	virtual int		run(void);

	void			play(void);
	void			options(void);
	void			quit(void);

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
