/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoArrowCurr.hpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVTODOARROWCURR_HPP
# define DIVTODOARROWCURR_HPP

#include "Game/Div/DivToDo/DivToDo.hpp"

class Arrow;
class ChunkDiv;

class DivToDoArrowCurr : public DivToDo<ChunkDiv>
{
private:
	Arrow	&_arrow;
public:
	DivToDoArrowCurr(Arrow &arrow);
	virtual	~DivToDoArrowCurr(void) {};

	virtual void 	doTo(ChunkDiv &div);
};

#endif

