/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LineShaderProgram.hpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 10:50:08 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:25:51 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINE_SHADER_PROGRAM_HPP
# define LINE_SHADER_PROGRAM_HPP

# include "CameraShaderProgram.hpp"

# define LINE_VERTEX "shaders/line/vertex.glsl"
# define LINE_GEOMETRY "shaders/line/geometry.glsl"
# define LINE_FRAGMENT "shaders/line/fragment.glsl"

class	LineShaderProgram : public agl::CameraShaderProgram
{
public:
	LineShaderProgram(void) throw(InvalidShaderProgramException);
	~LineShaderProgram(void);

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
