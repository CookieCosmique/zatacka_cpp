/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LinkIterator.cpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:16:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Lines/LinesMemory/LinkIterator.hpp"
#include "Game/Lines/LinesContainer.hpp"

LinkIterator::LinkIterator(void)
{
	_container = NULL;
	_indexData = NULL;
	_valid = false;
	_index = -1;
}

LinkIterator::LinkIterator(int index, SmartArray<GLuint> &indexData, LinesContainer &container)
{
	_container = &container;
	_indexData = &indexData;
	_valid = true;
	_index = index;
}

LinkIterator::LinkIterator(const LinkIterator &ite) : LinkIterator(ite._index, *ite._indexData, *ite._container) 
{ 
	_container = ite._container;
	_valid = ite._valid;
}

int	LinkIterator::getIndex(void) const
{
	return (_index + _indexData->getOffset());
}

bool	LinkIterator::isValid(void) const
{
	return (_valid);
}

void	LinkIterator::free(void)
{
/*	(*_indexData)[_index][0] = 0;
	(*_indexData)[_index][1] = 0;
	(*_indexData)[_index][2] = 0;*/
	_indexData->freeVal(_index);
	
	_container->updateIndex();
	_valid = false;
}

LinkIterator	&LinkIterator::operator=(const LinkIterator &ite)
{
	_container = ite._container;
	_indexData = ite._indexData;
	_index = ite._index;
	_valid = ite._valid;

	return (*this);
}
