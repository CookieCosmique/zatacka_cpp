/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   MemIterator.cpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/14 17:40:19 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Lines/LinesMemory/MemIterator.hpp"
#include "Game/Lines/LinesContainer.hpp"
#include "Logger.hpp"

using namespace agl;

MemIterator::MemIterator(void)
{
	_container = NULL;
	_posData = NULL;
	_numData = NULL;
	_valid = false;
	_index = -1;
}

MemIterator::MemIterator(size_t index, SmartArray<float> &posData, SmartArray<int> &numData, LinesContainer &container)
{
	_container = &container;
	_posData = &posData;
	_numData = &numData;
	_valid = true;
	_index = index;
}

MemIterator::MemIterator(const MemIterator &ite) : MemIterator(ite._index, *ite._posData, *ite._numData, *ite._container) 
{
	_container = ite._container;
	_valid = ite._valid;
}

MemIterator	&MemIterator::move(const Vec2 &newPos)
{
	(*_posData)[_index][0] = newPos.x;
	(*_posData)[_index][1] = newPos.y;

	_container->updatePos();
	return (*this);
}

MemIterator	&MemIterator::set(const Vec2 &newPos, int newNum)
{
	(*_posData)[_index][0] = newPos.x;
	(*_posData)[_index][1] = newPos.y;
	(*_numData)[_index][0] = newNum;

	_container->updatePos();
	_container->updateNum();
	return (*this);
}

MemIterator	&MemIterator::set(const MemIterator &ite)
{
	_posData = ite._posData;
	_numData = ite._numData;
	_valid = ite._valid;
	_index = ite._index;

	_container->updatePos();
	_container->updateNum();
	return (*this);
}

Vec2	MemIterator::getPos(void) const
{
	if(_valid)
		return Vec2((*_posData)[_index]);
	else
	{
		logger::sslog(LOG_ERROR) << "Error : tried to access to a freed line position : "
		 << _index << logger::endlog();
		return Vec2();
	}
}

int		MemIterator::getNum(void) const
{
	if(_valid)
		return ((*_posData)[_index][0]);
	else
	{
		logger::sslog(LOG_ERROR) << "Error : tried to access to a freed line player num : "
		 << _index << logger::endlog();
		return -1;
	}
}

size_t	MemIterator::getIndex(void) const
{
	return (_index + _posData->getOffset());
}

bool	MemIterator::isValid(void) const
{
	return (_valid);
}

void	MemIterator::free(void)
{
	_posData->freeVal(_index);
	_numData->freeVal(_index);

	_valid = false;
}

MemIterator	&MemIterator::operator=(const MemIterator &ite)
{
	_container = ite._container;
	return (set(ite));
}
