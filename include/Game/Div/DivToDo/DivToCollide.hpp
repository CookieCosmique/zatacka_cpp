/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToCollide.hpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVTOCOLLIDE_HPP
# define DIVTOCOLLIDE_HPP

#include "Game/Div/DivToDo/DivToDo.hpp"

class ChunkDiv;
class ColliderComponent;

class DivToCollide : public DivToDo<ChunkDiv>
{
private:
	ColliderComponent	&_component;
public:
	DivToCollide(ColliderComponent &component);
	virtual	~DivToCollide(void) {};

	virtual void 	doTo(ChunkDiv &div);
};

#endif



