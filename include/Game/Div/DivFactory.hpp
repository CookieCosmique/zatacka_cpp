/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivFactory.hpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:50:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVFACTORY_HPP
# define DIVFACTORY_HPP

class FlatBox;

template <typename T>
class DivFactory
{
public:
	virtual ~DivFactory(void) {};

	virtual T	*make(const FlatBox &zone) = 0;
};
#endif
