/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Collider.hpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:03:11 by Bastien             |\________\          */
/*   Updated: 2016-03-22 21:03:11 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLLIDER_HPP
# define COLLIDER_HPP

# include "Game/Collisions/Shape/Shape.hpp"
# include "Game/Collisions/Collisions.hpp"

class	Collider
{
public:
//	virtual bool	collide(const Polygon &p) const = 0;
//	virtual bool	collide(const Circle &c) const = 0;
//	virtual bool	collide(const Rectangle &c) const = 0;
	virtual bool	collide(const Triangle &t) const = 0;
	virtual bool	collide(const Rectangle &t) const = 0;
	virtual bool	collide(const EmptyShape &t) const = 0;
};

#endif
