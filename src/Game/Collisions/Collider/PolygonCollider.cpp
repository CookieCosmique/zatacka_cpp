/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   PolygonCollider.cpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:08:22 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:08:22 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Collider/PolygonCollider.hpp"

PolygonCollider::PolygonCollider(const Polygon &p) : _p(p)
{
}

PolygonCollider::~PolygonCollider(void)
{
}

void		PolygonCollider::collide(const Polygon &p) const
{
	collision::collision(this->_p, p);
}

void		PolygonCollider::collide(const Circle &c) const
{
	collision::collision(this->_p, c);
}

void		PolygonCollider::collide(const Triangle &t) const
{
	collision::collision(this->_p, t);
}
