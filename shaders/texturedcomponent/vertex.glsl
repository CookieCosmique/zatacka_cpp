#version 400 core

layout(location = 1) in vec2 position;
layout(location = 2) in vec2 UVs;

out vec2	vUV;

void	main(void)
{
	gl_Position = vec4(position, 0.0f, 1.0f);
	vUV = UVs;
}
