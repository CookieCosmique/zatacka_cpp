/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TriangleCollider.hpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:55:17 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:55:17 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef TRIANGLE_COLLIDER_HPP
# define TRIANGLE_COLLIDER_HPP

# include "Game/Collisions/Collider/Collider.hpp"

class	TriangleCollider : public Collider
{
private:
	const Triangle	&_t;
public:
	TriangleCollider(const Triangle &t);
	virtual	~TriangleCollider(void);
//	virtual bool	collide(const Polygon &p) const;
//	virtual bool	collide(const Circle &c) const;
	virtual bool	collide(const Triangle &t) const;
	virtual bool	collide(const Rectangle &t) const;
	virtual bool	collide(const EmptyShape &t) const;
};

#endif