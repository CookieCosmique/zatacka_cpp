#version 400 core

in float	in_ratio;
in float	in_expos;
out vec4	out_color;

//uniform vec3	color;

//uniform int		skin_line;
//uniform int		skin_back;

vec3	calc_lighting(vec3 b_col, float rate)
{
	vec3	final_color;
	float	upper = 0.4f;
	float	upper_up = 2.0f;
	float	base = 0.35f;

	if(rate >= upper)
		final_color = (rate - upper) / (upper_up - upper) * vec3(1.0f, 1.0f, 1.0f) +
			(1 - (rate - upper) / (upper_up - upper)) * b_col;
	else
		final_color = ((rate / upper) * (1.0f - base) + base) * b_col;
	
	return (final_color);
}

vec3	mixed(float ratio, vec3 col_1, vec3 col_2)
{
	return (ratio * col_1 + (1 - ratio) * col_2);
}

vec3	other_lighting(float rate, float expos, vec3 color)
{
	vec3	final_color;
	vec3	prim_col;
	vec3	sec_col;

	float	u;
	float	d;
	float	coef;
	float	ld;

	prim_col = 0.35f + 0.59f * color;
//	if (skin_back == 0)
		sec_col = vec3(0.0f, 0.0f, 0.0f);
//	else if (skin_back == 1)
//		sec_col = vec3(0.07f, 0.07f, 0.1f);
//	else
//		sec_col = vec3(0.0f, 0.0f, 0.0f);

	u = 0.08f;
	d = 0.08f;
	coef = 0.3f;
	ld = 0.1f;

	if (expos > 0)	
		u += coef * expos * (1 - u);
	else
		d -= coef * expos * (1 - d);

	if (rate <= d || rate >= 1 - u)
		final_color = prim_col;
	else if (rate <= d + ld )
		final_color = mixed(((d + ld - rate) / ld), prim_col, sec_col);
	else if (rate >= 1 - u - ld )
		final_color = mixed(-((1 - u - ld - rate) / ld), prim_col, sec_col);
	else
		final_color = sec_col;

	return (final_color);
}

void	main(void)
{
	vec3 color = vec3(0.0f, 0.0f, 1.0f);
//	if (skin_line == 0)
		out_color = vec4(calc_lighting(color, in_expos * (1 - in_ratio) + (1 - in_expos) * in_ratio ), 1.0f);
//	else
//		out_color = vec4(other_lighting(in_ratio, 2 * in_expos - 1, color), 1.0f);
}
