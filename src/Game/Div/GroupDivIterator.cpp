/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   GroupDivIterator.cpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:11:36 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/GroupDivIterator.hpp"
#include "Game/Div/DivGroup.hpp"
#include "Game/Div/DivIterator.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"

using namespace agl;

template <typename T>
GroupDivIterator<T>::GroupDivIterator(DivGroup<T> &group, START_GRP_ITERATOR) : _group(group), _curr(_group.getBegin())
{
	_row = 0;
	_col = 0;
	_way = RIGHT;
}

template <typename T>
GroupDivIterator<T>::GroupDivIterator(DivGroup<T> &group, END_GRP_ITERATOR) : _group(group), _curr(_group.getEnd())
{
	_row = _group.getRows() - 1;
	if(even())
	{
		_col = 0;
		_way = LEFT;
	}
	else
	{
		_col = _group.getCols() - 1;
		_way = RIGHT;
	}
}

template <typename T>
GroupDivIterator<T>::GroupDivIterator(const GroupDivIterator<T> &other) : _group(other._group), _curr(other._curr)
{
	_row = other._row;
	_col = other._col;
	_way = other._way;
}

template <typename T>
bool	GroupDivIterator<T>::isStart(void) const
{
	return (_col == 0 && _row == 0);
}

template <typename T>
bool	GroupDivIterator<T>::isEnd(void) const
{
	if(even())
	{
		return (_col == (_group.getRows() - 1) && _row == (_group.getRows() - 1));
	}
	else
	{
		return (_col == 0 && _row == (_group.getRows() - 1));
	}
}

template <typename T>
int		GroupDivIterator<T>::getRow(void) const
{
	return (_row);
}

template <typename T>
int		GroupDivIterator<T>::getCol(void) const
{
	return (_col);
}

template <typename T>
bool	GroupDivIterator<T>::operator==(const GroupDivIterator<T> &other) const
{
	return(_row == other._row && _col == other._col && _group == other._group);
}

template <typename T>
bool	GroupDivIterator<T>::operator++(int)
{
	if(_way & RIGHT)
	{
		if(_col < (_group.getCols() - 1))
		{
			_col ++;
			_curr.right();
		}
		else
		{
			if(_row < (_group.getRows() - 1))
			{
				_row ++;
				_curr.up();
				_way = LEFT;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		if(_col > 0)
		{
			_col --;
			_curr.left();
		}
		else
		{
			if(_row < (_group.getRows() - 1))
			{
				_row ++;
				_curr.up();
				_way = RIGHT;
			}
			else
			{
				return false;
			}
		}
	}

	return true;
}

template <typename T>
bool	GroupDivIterator<T>::operator--(int)
{
	if(_way && RIGHT)
	{
		if(_col > 0)
		{
			_col --;
			_curr.left();
		}
		else
		{
			if(_row > 0)
			{
				_row --;
				_curr.down();
				_way = LEFT;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		if(_col < (_group.getCols() - 1))
		{
			_col ++;
			_curr.right();
		}
		else
		{
			if(_row > 0)
			{
				_row --;
				_curr.down();
				_way = RIGHT;
			}
			else
			{
				return false;
			}
		}
	}

	return true;
}

template <typename T>
T		&GroupDivIterator<T>::operator*(void)
{
	return (*_curr);
}

template <typename T>
bool	GroupDivIterator<T>::even() const
{
	return ( (_group.getRows() % 2) == 0);
}

template class GroupDivIterator<Div>;
template class GroupDivIterator<ChunkDiv>;
