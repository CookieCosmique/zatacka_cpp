/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ArrowShaderProgram.hpp                          |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 10:50:01 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:25:33 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARROW_SHADER_PROGRAM_HPP
# define ARROW_SHADER_PROGRAM_HPP

# include "Camera.hpp"
# include "CameraShaderProgram.hpp"

# define ARROW_VERTEX "shaders/arrow/vertex.glsl"
# define ARROW_GEOMETRY "shaders/arrow/geometry.glsl"
# define ARROW_FRAGMENT "shaders/arrow/fragment.glsl"

class	ArrowShaderProgram : public agl::CameraShaderProgram
{
private:
	GLuint		_id_color;
	GLuint		_id_speed;
	GLuint		_id_size;

	GLuint		_id_skin;

public:
	ArrowShaderProgram(void) throw(InvalidShaderProgramException);
	virtual ~ArrowShaderProgram(void);

	void		updateColor(const int color) const;
	void		updateSpeed(const float speed) const;
	void		updateSize(const float size) const;
	void		updateSkin(const int skin) const;

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
