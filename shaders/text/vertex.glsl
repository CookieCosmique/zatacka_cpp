#version 400 core

layout(location = 0) in vec2 position;
layout(location = 1) in float id;
layout(location = 2) in vec2 char_x;
layout(location = 3) in vec3 color;

out uint	g_id;
out vec2	g_char_x;
out vec3	g_color;

void	main(void)
{
	gl_Position = vec4(position, 0.0f, 1.0f);
	g_id = uint(id);
	g_char_x = char_x;
	g_color = color;
}
