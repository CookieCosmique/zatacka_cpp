/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Renderable.cpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-29 20:15:13 by Bastien             |\________\          */
/*   Updated: 2016-03-29 20:15:13 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Scene/Component/Renderable.hpp"
#include "Scene/Render.hpp"

// Constructor - Destructor

Renderable::Renderable(Render &render, Camera &camera, const int level) :
		 _camera(camera)
{
	render.addRenderable(this, level);
}

Renderable::~Renderable(void)
{

}

// Print

void		Renderable::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Renderable" << C_RESET << ": ";
	Spawnable::objectPrint(o);
}
