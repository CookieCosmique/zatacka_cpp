/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Triangle.cpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:57:43 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:57:43 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Shape/Triangle.hpp"

Triangle::Triangle(void) : _collider(*this)
{
}

Triangle::~Triangle(void)
{
}

const Collider		&Triangle::getCollider(void) const
{
	return (this->_collider);
}

bool				Triangle::collide(const Collider &c) const
{
	return (c.collide(*this));
}

