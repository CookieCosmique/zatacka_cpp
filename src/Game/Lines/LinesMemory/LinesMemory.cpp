/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LinesMemory.cpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/14 17:39:57 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Lines/LinesMemory/LinesMemory.hpp"
#include "Game/Lines/LinesMemory/MemIterator.hpp"
#include "Game/Lines/LinesMemory/LinkIterator.hpp"
#include "Game/Lines/LinesContainer.hpp"
#include "Utility/SmartArray.hpp"
#include "Vec2.hpp"
#include "Logger.hpp"

using namespace agl;

LinesMemory::LinesMemory(LinesContainer &container) : _posData(2, 1, NULL), _numData(1, 1, NULL), _indexData(3, zeroGLuint), _container(container)
{
/*	_posData.newVal(1.0,10.0);
	_posData.newVal(2.0,8.0);
	_posData.newVal(3.0,8.0);
	_numData.newVal(1);
	_numData.newVal(2);
	_numData.newVal(3);
	_posData.freeVal(1);
	_posData.freeVal(2);
	_numData.freeVal(1);
	_numData.freeVal(2);
	_posData.newVal(4.0,8.0);
	_posData.newVal(5.0,8.0);
	_numData.newVal(4);
	_numData.newVal(5);*/

	logger::sslog(LOG_DEBUG) << _posData << logger::endlog();
	logger::sslog(LOG_DEBUG) << _numData << logger::endlog();
	logger::sslog(LOG_DEBUG) << _indexData << logger::endlog();
}

LinesMemory::~LinesMemory(void) {}

MemIterator		LinesMemory::request(const Vec2 &pos, int num)
{
	size_t index;

	index = _posData.newVal(pos.getXY()); //take a *T parameter
	if(index != _numData.newVal(&num))
	{
		//error
	}
	_container.updatePos();
	_container.updateNum();
	return (MemIterator(index, _posData, _numData, _container));
}

LinkIterator	LinesMemory::link(MemIterator &start, MemIterator &middle, MemIterator &end)
{
	size_t	index;

	index = _indexData.newVal(start.getIndex(), middle.getIndex(), end.getIndex());
	_container.updateIndex();
	return (LinkIterator(index, _indexData, _container));
}

void			LinesMemory::free(MemIterator &ite)
{
	ite.free();
}

void			LinesMemory::free(LinkIterator &ite)
{
	ite.free();
}

float			*LinesMemory::getPosData(void) const
{
	return (_posData.getData());
}

int				*LinesMemory::getNumData(void) const
{
	return (_numData.getData());
}

GLuint			*LinesMemory::getIndexData(void) const
{
	return (_indexData.getData());
}

size_t			LinesMemory::getSizePosData(void) const
{
	return (_posData.getSizeData());
}

size_t			LinesMemory::getSizeNumData(void) const
{
	return (_numData.getSizeData());
}


size_t			LinesMemory::getSizeIndexData(void) const
{
	return (_indexData.getSizeData());
}


std::ostream	&operator<<(std::ostream &o, const LinesMemory &mem)
{
	o << C_OBJ << "LinesMemory" << C_RESET << "{" << logger::tab(1);
	o << C_VAR << "pos data" << C_RESET << " : " << mem._posData << ",\n";
	o << C_VAR << "num data" << C_RESET << " : " << mem._numData << ",\n";
	o << C_VAR << "index data " << C_RESET << " : " << mem._indexData << logger::tab(-1) << "}";

	return (o);
}
