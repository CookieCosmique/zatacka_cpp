/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MenuMainArea.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 16:48:31 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/02 17:30:40 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MENU_MAIN_AREA_HPP
# define MENU_MAIN_AREA_HPP

# include "Area.hpp"
# include "OpenGL/ShaderProgram/ButtonShaderProgram.hpp"
# include "Inputs.hpp"
# include "Scene/Interface/TextComponent.hpp"

class	Menu;

class	MenuMainArea : public agl::Area
{
private:
	ButtonShaderProgram		_buttonShaderProgram;
	TextShaderProgram		_textShaderProgram;
	agl::FontTexture		*_font;

protected:
	void				onModif(void);

public:
	MenuMainArea(Menu &menu, agl::Inputs &inputs);
	~MenuMainArea(void);

	virtual void		objectPrint(std::ostream &o) const;
};

#endif
