/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   EmptyShapeCollider.cpp                          |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:55:17 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:55:17 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Collider/EmptyShapeCollider.hpp"

EmptyShapeCollider::EmptyShapeCollider(const EmptyShape &t) : _t(t)
{

}

EmptyShapeCollider::~EmptyShapeCollider(void)
{

}

/*void		EmptyShapeCollider::collide(const Polygon &p) const
{
	collision::collision(this->_t, p);
}

void		EmptyShapeCollider::collide(const Circle &c) const
{
	collision::collision(this->_t, c);
}*/

bool		EmptyShapeCollider::collide(const Triangle &t) const
{
	return (collision::collision(this->_t, t));
}

bool		EmptyShapeCollider::collide(const Rectangle &t) const
{
	return (collision::collision(this->_t, t));
}

bool		EmptyShapeCollider::collide(const EmptyShape &t) const
{
	return (collision::collision(this->_t, t));
}



