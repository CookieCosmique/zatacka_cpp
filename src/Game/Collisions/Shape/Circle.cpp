/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Circle.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:08:10 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:08:10 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Shape/Circle.hpp"

Circle::Circle(void) : _collider(*this)
{
}

Circle::~Circle(void)
{
}

const Collider		&Circle::getCollider(void) const
{
	return (this->_collider);
}

void				Circle::collide(const Collider &c) const
{
	c.collide(*this);
}
