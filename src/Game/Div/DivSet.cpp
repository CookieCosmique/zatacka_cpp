/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivSet.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:11:10 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivSet.hpp"
#include "Game/Div/DivGroup.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Div/DivContainer.hpp"
#include "Game/Div/DivIterator.hpp"
#include "Game/Div/DivToDo/DivToDo.hpp"
#include "Game/Div/GroupDivIterator.hpp"

using namespace agl;

template <typename T>
DivSet<T>::DivSet(DivContainer<T> &container, DivToDo<T> *toDoNew, DivToDo<T> *toDoOld) : _container(container)
{
	this->_toDoNew = toDoNew;
	this->_toDoOld = toDoOld;
	this->_currGroup = NULL;
	this->_crossGroup = NULL;
}

template <typename T>
DivSet<T>::DivSet(DivContainer<T> &container, DivToDo<T> *toDoNew, DivToDo<T> *toDoOld, const FlatBox &newzone) : DivSet<T>(container, toDoNew, toDoOld)
{
	this->_currGroup = new DivGroup<T>(this->_container.getGroup(newzone));
	this->_crossGroup = new DivGroup<T>(this->_container.getGroup(newzone, this->_currGroup->getBegin()));
	this->_toDoNew->forEach(*this->_currGroup);
}

template <typename T>
DivSet<T>::~DivSet()
{
	logger::sslog(LOG_DEBUG) << "DELETING A DIV SET" << logger::endlog();
	if(this->_currGroup)
		forEachCurr(this->_toDoOld);

	if(this->_currGroup)
		delete this->_currGroup;
	if(this->_crossGroup)
		delete this->_crossGroup;
	if(this->_toDoNew)
		delete this->_toDoNew;
	if(this->_toDoOld)
		delete this->_toDoOld;
}

template <typename T>
void	DivSet<T>::setDoNew(DivToDo<T> *toDo)
{
	this->_toDoNew = toDo;
}

template <typename T>
void	DivSet<T>::setDoOld(DivToDo<T> *toDo)
{
	this->_toDoOld = toDo;
}

template <typename T>
void	DivSet<T>::update(const FlatBox &newzone)
{
	DivGroup<T>	*newGroup;

	if (this->_crossGroup)
		delete this->_crossGroup;

	if (this->_currGroup)
		newGroup = new DivGroup<T>(this->_container.getGroup(newzone, this->_currGroup->getBegin()));
	else
		newGroup = new DivGroup<T>(this->_container.getGroup(newzone));

	if (this->_currGroup)
		this->_crossGroup = new DivGroup<T>(this->_container.getGroup(newGroup->getZone() | this->_currGroup->getZone(), newGroup->getBegin()));
	else
		this->_crossGroup = new DivGroup<T>(this->_container.getGroup(newGroup->getZone(), newGroup->getBegin()));


	if (!this->_crossGroup->isEmpty() && (this->_toDoNew || this->_toDoOld)) //dont do it if doNew and doOld are NULL
	{
		GroupDivIterator<T>	ite = this->_crossGroup->start();
		bool				keep = true;

		while (keep == true)
		{	
			if (this->_currGroup)
			{
				if (!((*ite).getZone() && this->_currGroup->getZone()))
					doNew(*ite);
				if (!((*ite).getZone() && newGroup->getZone()))
					doOld(*ite);
			}
			else
				doNew(*ite);

			if(!ite++)
				keep = false;
		}
	}

	if (this->_currGroup)
		delete this->_currGroup;
	this->_currGroup = newGroup;
}

template <typename T>
void	DivSet<T>::forEachCurr(DivToDo<T> *toDo)
{
	if(toDo)
		toDo->forEach(*this->_currGroup);

	delete toDo;
}

template <typename T>
void	DivSet<T>::forEachCross(DivToDo<T> *toDo)
{
	if(toDo)
		toDo->forEach(*this->_crossGroup);

	delete toDo;
}

template <typename T>
void	DivSet<T>::doNew(T &div)
{
	if(this->_toDoNew)
		this->_toDoNew->doTo(div);
}

template <typename T>
void	DivSet<T>::doOld(T &div)
{
	if(this->_toDoOld)
		this->_toDoOld->doTo(div);
}

template class DivSet<Div>;
template class DivSet<ChunkDiv>;
