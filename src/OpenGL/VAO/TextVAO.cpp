/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TextVAO.cpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 18:08:09 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:26:06 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OpenGL/VAO/TextVAO.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

TextVAO::TextVAO(const GLuint program_id, const FontTexture &font,
		const std::string &str, const int color, const int emphasis) :
	_font(font), _str(str)
{
	this->create_arrays(this->_str.length());
	this->bind();
	this->_ebo = this->createEBO();
	this->_vbo_coordinates = this->createVBO(program_id, GL_FLOAT, 0, "position", 2);
	this->_vbo_ids = this->createVBO(program_id, GL_FLOAT, 1, "id", 1);
	this->_vbo_char_x = this->createVBO(program_id, GL_FLOAT, 2, "char_x" , 2);
	this->_vbo_colors = this->createVBO(program_id, GL_FLOAT, 3, "color", 3);
	this->setString(str, color, emphasis);
	this->setColor(color);
}

TextVAO::~TextVAO(void)
{
	this->delete_arrays();
}

// Member Functions

void				TextVAO::render(void) const
{
	this->bind();
	this->_font.bind();
	glDrawElements(GL_POINTS, this->_str.length(), GL_UNSIGNED_INT, 0);
}

void				TextVAO::delete_arrays(void)
{
	delete [] this->_ids;
	delete [] this->_coordinates;
	delete [] this->_indices;
	delete [] this->_char_x;
	delete [] this->_colors;
}

void				TextVAO::create_arrays(const unsigned int size)
{
	this->_ids = new GLfloat[size];
	this->_coordinates = new GLfloat[size * 2];
	this->_indices = new GLuint[size];
	this->_char_x = new GLfloat[size * 2];
	this->_colors = new GLfloat[size * 3];
}

void				TextVAO::resize_arrays(const unsigned int size)
{
	this->delete_arrays();
	this->create_arrays(size);
}

void				TextVAO::update_buffers(void) const
{
	this->bind();
	this->updateBufferData(GL_ELEMENT_ARRAY_BUFFER, this->_ebo,
			sizeof(GLuint) * this->_str.length(), this->_indices);
	this->updateBufferData(GL_ARRAY_BUFFER, this->_vbo_coordinates,
			sizeof(GLfloat) * this->_str.length() * 2, this->_coordinates);
	this->updateBufferData(GL_ARRAY_BUFFER, this->_vbo_ids,
			sizeof(GLfloat) * this->_str.length(), this->_ids);
	this->updateBufferData(GL_ARRAY_BUFFER, this->_vbo_char_x,
			sizeof(GLfloat) * this->_str.length() * 2, this->_char_x);
	this->updateBufferData(GL_ARRAY_BUFFER, this->_vbo_colors,
			sizeof(GLfloat) * this->_str.length() * 3, this->_colors);
}

void				TextVAO::setString(const std::string &str,
		const int color, const int emphasis)
{
	unsigned int	i;
	float			x;

	if (this->_str.length() != str.length())
		this->resize_arrays(str.length());
	this->_str = str;
	i = 0;
	x = 0;
	while (i < this->_str.length())
	{
		this->_ids[i] = this->_font.getCharId(this->_str.c_str()[i]);
		this->_coordinates[i * 2] = x;
		this->_coordinates[i * 2 + 1] = 0;
		this->_indices[i] = i;
		this->_char_x[i * 2] = this->_font.getCharX(this->_str.c_str()[i]);
		this->_char_x[i * 2 + 1] = this->_font.getCharLength(this->_str.c_str()[i]);
		x += (this->_font.getCharLength(this->_str.c_str()[i]) +
				((emphasis & 1) ? 1 : 0)) /
				(float)this->_font.getCharWidth();
		++i;
	}
	this->_size.x = this->_coordinates[(this->_str.length() - 1) * 2] +
			this->_char_x[(this->_str.length() - 1) * 2 + 1] /
			this->_font.getCharWidth() - this->_coordinates[0];
	this->_size.y = 1;
	this->setColor(color);
	this->update_buffers();
}

void				TextVAO::setColor(const float r, const float g, const float b)
{
	unsigned int	i;

	i = 0;
	while (i < this->_str.length())
	{
		this->_colors[i * 3] = r / 255.0f;
		this->_colors[i * 3 + 1] = g / 255.0f;
		this->_colors[i * 3 + 2] = b / 255.0f;
		++i;
	}
	this->update_buffers();
}

void				TextVAO::setColor(const int color)
{
	this->setColor((color & 0xFF0000) >> 16,
				   (color & 0x00FF00) >> 8,
				   (color & 0x0000FF));
}

const Vec2			&TextVAO::getSize(void) const
{
	return (this->_size);
}

const std::string	&TextVAO::getString(void) const
{
	return (this->_str);
}

// Print

void				TextVAO::objectPrint(std::ostream &o) const
{
	unsigned int		i;
	o << C_OBJ << "TextVAO" << C_RESET << ": {" << logger::tab(1);
	VAO::objectPrint(o);
	o << ",\n";
	o << C_VAR << "str" << C_RESET << " = " << C_STR << "\"" << this->_str << "\"" << C_RESET << ",\n";
	o << C_VAR << "ids" << C_RESET << " = " << logger::tab(1);
	i = 0;
	while (i < this->_str.length())
	{
		o << this->_ids[i];
		if (++i < this->_str.length())
			o << ",\n";
	}
	o << logger::tab(-1) << "},\n";
	o << C_VAR << "colors" << C_RESET << " = " << logger::tab(1);
	i = 0;
	while (i < this->_str.length())
	{
		o << this->_colors[i * 3] << ", " << this->_colors[i * 3 + 1] << ", " <<
			this->_colors [i * 3 + 2];
		++i;
		if (i < this->_str.length())
			o << ",\n";
	}
	o << logger::tab(-1) << "},\n";
	o << C_VAR << "coordinates" << C_RESET << " = " << logger::tab(1);
	i = 0;
	while (i < this->_str.length() * 2)
	{
		o << this->_coordinates[i] << ", " << this->_coordinates[i + 1];
		i += 2;
		if (i < this->_str.length() * 2)
			o << ",\n";
	}
	o << logger::tab(-1) << "},\n";
	o << C_VAR << "char_x" << C_RESET << " = " << logger::tab(1);
	i = 0;
	while (i < this->_str.length() * 2)
	{
		o << this->_char_x[i] << ", " << this->_char_x[i + 1];
		i += 2;
		if (i < this->_str.length() * 2)
			o << ",\n";
	}
	o << logger::tab(-1) << "},\n";
	o << C_VAR << "vbo ids" << C_RESET << " = " << this->_vbo_ids << ",\n";
	o << C_VAR << "vbo coordinates" << C_RESET << " = " << this->_vbo_coordinates << ",\n";
	o << logger::tab(-1) << "}";
}
