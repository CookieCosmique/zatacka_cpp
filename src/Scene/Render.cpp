/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Render.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-29 23:30:16 by Bastien             |\________\          */
/*   Updated: 2016/04/28 17:42:37 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Scene/Render.hpp"

// Constructor - Destructor

Render::Render(void)
{
	logger::sslog(LOG_DEBUG) << C_OBJ << "Render" << C_RESET << " created successfully." << logger::endlog();
}

Render::~Render(void)
{
	int		i;
	std::forward_list< SmartPointer<Renderable> >::iterator	it;

	logger::sslog(LOG_DEBUG) << "Starting " << C_OBJ << "Render" << C_RESET << " deletion ..." << logger::endlog(1);
//	logger::sslog(LOG_DEBUG) << *this << logger::endlog(0);
	i = 0;
	while (i < MAX_RENDER_LEVEL)
	{
		while (!(_renderableObjects[i].empty()))
		{
			it = _renderableObjects[i].begin();
			(*it)->kill();
			_renderableObjects[i].pop_front();
		}
		++i;
	}
	logger::sslog(LOG_DEBUG,-1) << C_OBJ << "Render" << C_RESET << " Deleted." << logger::endlog();
}

// Member functions

void		Render::render(GLFWwindow *window)
{
	Renderable		*renderable;
	int 			i;
	std::forward_list< SmartPointer<Renderable> >::iterator	it;
	std::forward_list< SmartPointer<Renderable> >::iterator	buffer;

	glClearColor(0.5f, 0.5f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	i = 0;
	while (i < MAX_RENDER_LEVEL)
	{
		if(!this->_renderableObjects[i].empty())
		{
			it = this->_renderableObjects[i].begin();
			buffer = it;
			while (it != this->_renderableObjects[i].end())
			{
				renderable = *it;
				if (renderable->isAlive())
					renderable->render();
				if (!renderable->isAlive())
				{
					if (it == buffer)
					{
						buffer++;
						this->_renderableObjects[i].pop_front();
						it = buffer;
					}
					else
					{
						this->_renderableObjects[i].erase_after(buffer);
						it = buffer;
						++it;
					}
				}
				else
				{
					buffer = it;
					++it;
				}
			}
		}
		++i;
	}
	glfwSwapBuffers(window);
}

void		Render::addRenderable(Renderable *renderable, const int level)
{
	if (level < MAX_RENDER_LEVEL)
	{
		this->_renderableObjects[level].push_front(renderable);
	}
}

// Print

void		Render::objectPrint(std::ostream &o) const
{
	int					i;
	int					size;
	std::forward_list< SmartPointer<Renderable> >::const_iterator	it;

	o << C_OBJ << "Render" << C_RESET << ": {" << logger::tab(1);
	i = 0;
	while (i < MAX_RENDER_LEVEL)
	{
		size = distance(this->_renderableObjects[i].begin(), this->_renderableObjects[i].end());
		o << C_VAR << "renderable" << C_RESET << "[" << i << "] (" << size << ")";
		if (size)
		{
			o << ": {" << logger::tab(1);
			it = this->_renderableObjects[i].begin();
			while (it != this->_renderableObjects[i].end())
			{
				o << **it;
				if (++it != this->_renderableObjects[i].end())
					o << ",\n";
			}
			o << logger::tab(-1) << "}";
		}
		if (++i < MAX_RENDER_LEVEL)
			o << ",\n";
	}
	o << logger::tab(-1) << "}";
}
