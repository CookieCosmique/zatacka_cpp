/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   GroupDivIterator.hpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef GROUPDIVITERATOR_HPP
# define GROUPDIVITERATOR_HPP

# include "Game/Div/DivIterator.hpp"

template <typename T>
class DivIterator;
template <typename T>
class DivGroup;

class START_GRP_ITERATOR {};
class END_GRP_ITERATOR {};

enum Side { RIGHT = 0x01, LEFT = 0x02 };

template <typename T>
class GroupDivIterator
{
private:
	DivGroup<T>		&_group;

	DivIterator<T>	_curr;
	int				_row;
	int				_col;
	Side			_way;

	bool		even() const;
public:
	GroupDivIterator(DivGroup<T> &group, START_GRP_ITERATOR);
	GroupDivIterator(DivGroup<T> &group, END_GRP_ITERATOR);
	GroupDivIterator(const GroupDivIterator &other);

	bool	isStart(void) const;
	bool	isEnd(void) const;
	int		getRow(void) const;
	int		getCol(void) const;

	bool	operator==(const GroupDivIterator<T> &other) const;

	bool	operator++(int);
	bool	operator--(int);

	T		&operator*(void);

};

#endif