/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SmartArray.hpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/04 16:49:12 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SMARTARRAY_HPP
# define SMARTARRAY_HPP

# define MAX_SIZE 4

# include <stack>
# include <stdarg.h>
# include <cstdio>
# include "Object.hpp"
# include "OpenGL.h"

template <typename T>
class SmartArray : public agl::Object
{
public:
	class	OverSizedException : public std::exception
	{
	private:
		size_t	_size;
		size_t	_maxSize;
	public:
		OverSizedException(size_t size, size_t maxsize);
		virtual const char	*what(void) const throw();
	};

	class	WrongParametersNumberException : public std::exception
	{
	private:
		size_t _size;
		size_t _tried;
	public:
		WrongParametersNumberException(size_t size, size_t tried);
		virtual const char	*what(void) const throw();
	};

private:
	size_t	_size;	//< size of a chunk of element (n-uplet)
	size_t	_maxSize; //< _data is of size (_maxSize * size)
	size_t	_offset;

	T	(*_initializer) (void);
	T	*_data;	//< array of data stocked

	std::stack<size_t>	_indexStack; //< the stack of avalaible index in the array

public:
	SmartArray(size_t size, T (*initializer)(void)) throw(OverSizedException);
	SmartArray(size_t size, size_t offset, T (*initializer)(void)) throw(OverSizedException);
	~SmartArray(void);

	size_t	newVal(const T *newData);
	size_t	newValTmp(const T **newData);
	size_t	newVal(const T &one) throw(WrongParametersNumberException);
	size_t	newVal(const T &one, const T &two) throw(WrongParametersNumberException);
	size_t	newVal(const T &one, const T &two, const T &three) throw(WrongParametersNumberException);
	size_t	newVal(const T &one, const T &two, const T &three, const T &four) throw(WrongParametersNumberException);

	void	freeVal(size_t index);

	size_t	getSize(void) const;
	size_t	getOffset(void) const;
	size_t	getMaxSize(void) const;
	size_t	getSizeData(void) const;

	T	*getData(void) const;
	T	*operator[](size_t i) const;

	void objectPrint(std::ostream &o) const;
};

int		zeroInt(void);
GLuint	zeroGLuint(void);
float	zeroFloat(void);

#endif
