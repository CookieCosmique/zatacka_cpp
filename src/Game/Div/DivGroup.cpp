/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivGroup.cpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:10:44 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivGroup.hpp"
#include "Game/Div/DivContainer.hpp"
#include "Game/Div/DivIterator.hpp"
#include "Game/Div/GroupDivIterator.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Utility/FlatBox.hpp"

using namespace agl;

template <typename T>
DivGroup<T>::DivGroup(DivContainer<T> &container, const FlatBox &zone, const DivIterator<T> &begin, const DivIterator<T> &end) : 
	_container(container), _begin(begin), _end(end), _zone(zone)
{
	_cols = _zone.getDimensions().x / _container.getDimensions().x;
	_rows = _zone.getDimensions().y / _container.getDimensions().y;
}

template <typename T>
GroupDivIterator<T>	DivGroup<T>::start()
{
	return GroupDivIterator<T>(*this, START_GRP_ITERATOR());
}

template <typename T>
GroupDivIterator<T>	DivGroup<T>::end()
{
	return GroupDivIterator<T>(*this, END_GRP_ITERATOR());
}

template <typename T>
DivIterator<T>		DivGroup<T>::getBegin()
{
	return (_begin);
}

template <typename T>
DivIterator<T>		DivGroup<T>::getEnd()
{
	return (_end);
}

template <typename T>
bool				DivGroup<T>::isEmpty() const
{
	return (_zone.isEmpty());
}

template <typename T>
int					DivGroup<T>::getRows() const
{
	return (_rows);
}

template <typename T>
int					DivGroup<T>::getCols() const
{
	return (_cols);
}

template <typename T>
const FlatBox		&DivGroup<T>::getZone() const
{
	return (_zone);
}

template <typename T>
bool				DivGroup<T>::operator==(const DivGroup<T> &other)
{
	return (this == &other);
}

template <typename T>
void				DivGroup<T>::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "DivGroup" << C_RESET << " : " << " {" << logger::tab(1);
	o << C_VAR << "zone" << C_RESET << " : " << _zone << ",\n";
	o << C_VAR << "col / rows" << C_RESET << " : " << _cols << " / " << _rows;
	o << logger::tab(-1) << "}";
}

template class DivGroup<Div>;
template class DivGroup<ChunkDiv>;
