/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Flags.cpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-15 21:36:58 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:29:07 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Utility/Flags.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

Flags::Flags(int ac, char **av)
{
	int			i;

	this->_flagsList['d'] = Flags::F_LOG_DEBUG;
	this->_flagsList['t'] = Flags::F_LOG_TIME;
	this->_flagsList['a'] = Flags::F_ANTI_ALIASING;
	this->_flagsList['f'] = Flags::F_FIXED_FPS;
	this->_flags = 0;
	i = 0;
	while (++i < ac)
		addFlags(av[i], av[0]);
}

Flags::~Flags(void) {}

// Private Member Functions

void		Flags::printUnknownFlag(const char c, const char *path) const
{
	std::stringstream						ss;
	std::map<char, int>::const_iterator		it;

	it = this->_flagsList.begin();
	while (it != this->_flagsList.end())
	{
		ss << it->first;
		++it;
	}
	logger::sslog(LOG_WARNING) << "Unknown flag -\"" << c << "\"" << logger::endlog();
	logger::sslog(LOG_WARNING) <<  "Usage: " << path << " [-" << ss.str() << "] [NB]" << logger::endlog();
}

void		Flags::addFlags(const char *s, const char *path)
{
	size_t		i;
	char		c;
	int			flag;

	if (s[0] != '-')
		return ;
	i = 0;
	while ((c = s[++i]))
	{
		try
		{
			flag = this->_flagsList.at(c);
			this->_flags |= flag;
		}
		catch (std::out_of_range &oor)
		{
			printUnknownFlag(c, path);
		}
	}
}

// Public Member Functions

bool		Flags::hasFlag(const int flag) const
{
	return (flag & this->_flags);
}

// Print

void		Flags::objectPrint(std::ostream &o) const
{
	std::map<char, int>::const_iterator		it;
	bool									first;

	o << C_OBJ << "Flag" << C_RESET << ": { ";
	first = true;
	it = this->_flagsList.begin();
	while (it != this->_flagsList.end())
	{
		if (hasFlag(it->second))
		{
			if (!first)
				o << ", ";
			else
				first = false;
			o << it->first;
		}
		++it;
	}
	o << " }";
}
