/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToCollide.cpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivToDo/DivToCollide.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Collisions/ColliderComponent/ColliderComponent.hpp"

DivToCollide::DivToCollide(ColliderComponent &component) : _component(component) {}

void	DivToCollide::doTo(ChunkDiv &div)
{
	div.collide(_component); // div will call doOnCollision() de component;
}

