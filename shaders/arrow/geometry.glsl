#version 400 core

layout(points) in;
layout(triangle_strip, max_vertices = 12) out;

in vec2		out_dir[];

out vec3	in_color1;
out vec3	in_color2;
out vec2	in_origin;
out vec2	in_pos;
out float	in_size;

uniform float	speed;
uniform float	size;
uniform vec3	color;

uniform int		skin;

uniform vec2	cam_size;
uniform vec2	cam_pos;

void	emit_point(vec2 pos)
{
	in_pos = vec2(pos.x, pos.y);
	gl_Position = vec4((pos.x - cam_pos.x) / cam_size.x * 2 - 1.0f,
						(pos.y - cam_pos.y) / cam_size.y * 2 - 1.0f, 0.0f, 1.0f);
	EmitVertex();
}

void 	draw_arrow_neon(vec2 origin, vec2 axis_x, vec2 axis_y)
{
	vec2	points[6];
	points[0] = vec2(-0.30f, -0.50f);
	points[1] = vec2(-0.15f, -0.15f);
	points[2] = vec2( 1.00f,  0.00f);
	points[3] = vec2( 0.00f,  0.00f);
	points[4] = vec2(-0.30f,  0.50f);
	points[5] = vec2(-0.15f,  0.15f);
	float	move;
	int		i;

	// FIRST ARROW
	vec3 col = 0.35f + 0.59f * color;
	in_color1 = col;
	in_color2 = col;

	i = -1;
	while (++i < points.length())
		emit_point(origin + points[i].x * axis_x + points[i].y * axis_y);
	EndPrimitive();


	// SECOND ARROW
	in_color1 = vec3(0.07f, 0.07f, 0.1f);

	move = 0.1f;
	points[0] += move;
	points[1].x += 0.8 * move;
	points[2].x -= 1.4 * move;
	points[3].x += move;
	points[4].x += move;
	points[4].y -= move;
	points[5].x += 0.8 * move;

	i = -1;
	while (++i < points.length())
		emit_point(origin + points[i].x * axis_x + points[i].y * axis_y);
	EndPrimitive();
}

void	draw_arrow_normal(vec2 origin, vec2 axis_x, vec2 axis_y)
{
	vec2	points[4];
	points[0] = vec2(-0.3f, -0.5f);
	points[1] = vec2(0.0f, 0.0f);
	points[2] = vec2(1.0f, 0.0f);
	points[3] = vec2(-0.3f, 0.5f);

	vec3	colors[6];
	colors[0] = vec3(1.0f, 1.0f, 1.0f);
	colors[1] = vec3(0.3f, 0.3f, 0.3f);
	colors[2] = vec3(0.3f, 0.3f, 0.3f);
	colors[3] = vec3(0.15f, 0.15f, 0.15f);
	colors[4] = vec3(0.15f, 0.15f, 0.15f);
	colors[5] = vec3(0.5f, 0.5f, 0.5f);
	int		i;

	i = 0;
	while (i < 3)
	{
		in_color1 = colors[i];
		emit_point(origin + points[i].x * axis_x + points[i].y * axis_y);
		i++;
	}
	i = 1;
	while (i < 4)
	{
		in_color1 = colors[i + 2];
		emit_point(origin + points[i].x * axis_x + points[i].y * axis_y);
		i++;
	}
	EndPrimitive();
}

void	main(void)
{
	vec2	axis_x;
	vec2 	axis_y;
	vec2	origin;

	float	size_x;
	float	size_y;

	// Arrow Size
	in_size = size;
	size_x = size * pow(speed, 0.5);
	size_y = size / pow(speed, 0.3);

	// Axis
	axis_x = vec2(out_dir[0].x * size_x, out_dir[0].y * size_x);
	axis_y = vec2(-out_dir[0].y * size_y, out_dir[0].x * size_y);

	// Origin
	origin = gl_in[0].gl_Position.xy + (size - size_x) * axis_x / size_x;
	in_origin = origin + 0.3 * axis_x;
	in_origin = vec2(in_origin.x, in_origin.y);

	if (skin == 0)
		draw_arrow_normal(origin, axis_x, axis_y);
	else if (skin == 1)
		draw_arrow_neon(origin, axis_x, axis_y);
}