/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivContainerArray.cpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:04:05 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivContainer.hpp"
#include "Game/Div/DivContainerArray.hpp"
#include "Game/Div/Div.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Div/DivFactory.hpp"
#include "Game/Div/DivArrayIterator.hpp"
#include "Game/Div/DivIterator.hpp"

using namespace agl;

template <typename T>
DivContainerArray<T>::DivContainerArray(int x, int y,const Vec2 &downLeft, const Vec2 &dimensions, DivFactory<T> *factory) : DivContainer<T>(dimensions, factory)
//DivContainerArray<T>::DivContainerArray(int x, int y,const Vec2 &downLeft, const Vec2 &dimensions) : DivContainer<T>(dimensions)
{
	int	i;
	int	j;

	_sizeX = x;
	_sizeY = y;
	this->_format += downLeft;
	this->_zone.setDownLeft(downLeft);
	this->_zone.setUpRight(downLeft + (dimensions.vx() * (x+1)) + (dimensions.vy() * (y+1)));

	logger::sslog(LOG_FINE) << "Starting " << C_OBJ << "Array Container" << C_RESET << " creation ..." << logger::endlog(1);

	_divArray = new T**[_sizeX];
	i = 0;
	while (i < _sizeX)
	{
		_divArray[i] = new T*[_sizeY];
		j = 0;
		while (j < _sizeY)
		{
			if(this->_factory)
				_divArray[i][j] = this->_factory->make(this->_format + ((dimensions.vx() * i) + (dimensions.vy() * j)));
//				_divArray[i][j] = new T(this->_format + ((dimensions.vx() * i) + (dimensions.vy() * j)));

			j ++;
		}
		i++;
	}

	logger::sslog(LOG_FINE, -1) << C_OBJ << "Array Container" << C_RESET << " created successfully" << logger::endlog();
}
template <typename T>
DivContainerArray<T>::~DivContainerArray()
{
	int	i;
	int	j;

	i = 0;
	while (i < _sizeX)
	{
		j = 0;
		while (j < _sizeY)
		{
			delete _divArray[i][j];
			j ++;
		}
		delete[] _divArray[i];
		i++;
	}
	delete[] _divArray;
}
template <typename T>
DivIterator<T>	DivContainerArray<T>::getAnchor(void)
{
	return (DivIterator<T>(new DivArrayIterator<T>(*this, 0, 0)));
}
template <typename T>
DivIterator<T>	DivContainerArray<T>::getAnchor(const Vec2 &pos)
{
	return (DivIterator<T>(new DivArrayIterator<T>(*this, (pos.y - this->_format.getDownLeft().y) / this->_dimensions.y,
													(pos.x - this->_format.getDownLeft().x) / this->_dimensions.x)));
}
template <typename T>
DivIterator<T>	DivContainerArray<T>::getAnchor(const Vec2 &pos,const DivIterator<T> &anchor)
{
	(void) anchor;
	return (getAnchor(pos));
}
template <typename T>
T			&DivContainerArray<T>::getDiv(const int row,const int col)
{
	int	tmp_col = col;
	int	tmp_row = row;
	tmp_col = (tmp_col > 0) ? tmp_col : 0;
	tmp_col = (tmp_col < _sizeX) ? tmp_col : (_sizeX - 1);
	tmp_row = (tmp_row > 0) ? tmp_row : 0;
	tmp_row = (tmp_row < _sizeY) ? tmp_row : (_sizeY - 1);

	return (*_divArray[tmp_col][tmp_row]);
}

template class DivContainerArray<Div>;
template class DivContainerArray<ChunkDiv>;
