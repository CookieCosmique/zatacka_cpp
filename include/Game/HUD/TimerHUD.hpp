/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TimerHUD.hpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-21 22:11:51 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:27:38 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TIMER_HUD_HPP
# define TIMER_HUD_HPP

# include "Updatable.hpp"
# include "Scene/Interface/TextComponent.hpp"

class	TimerHUD : public TextComponent, public agl::Updatable
{
private:
	double		_last_time;

public:
	TimerHUD(agl::Scene &scene, const agl::FontTexture &font,
			TextShaderProgram &shaderProgram);
	~TimerHUD(void);

	void	update(const agl::GameTime &game_time);

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
