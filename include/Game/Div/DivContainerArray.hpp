/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivContainerArray.hpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:52:34 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVCONTAINERARRAY_HPP
# define DIVCONTAINERARRAY_HPP

# include "Game/Div/DivContainer.hpp"

template <typename T>
class DivArrayIterator;
template <typename T>
class DivIterator;

template <typename T>
class DivContainerArray : public DivContainer<T>
{
private:
	T			***_divArray;
	int			_sizeX;
	int			_sizeY;

public:
	DivContainerArray(int x, int y,const agl::Vec2 &downLeft,
			const agl::Vec2 &dimensions, DivFactory<T> *factory);
//	DivContainerArray(int x, int y,const Vec2 &downLeft,const Vec2 &dimensions);
	virtual ~DivContainerArray(void);
	
	virtual DivIterator<T>	getAnchor(void);
	virtual DivIterator<T>	getAnchor(const agl::Vec2 &pos);
	virtual DivIterator<T>	getAnchor(const agl::Vec2 &pos,
			const DivIterator<T> &anchor);
	
	T	&getDiv(const int row,const int col);
};

#endif

