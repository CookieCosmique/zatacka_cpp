/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivContainer.hpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:50:24 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVCONTAINER_HPP
# define DIVCONTAINER_HPP

# include "Utility/FlatBox.hpp"
# include "Game/Div/DivFactory.hpp"
# include "Game/Div/DivContainer/AbstractDivContainer.hpp"

template <typename T>
class DivGroup;
template <typename T>
class DivSet;
template <typename T>
class DivIterator;
template <typename T>
class DivFactory;
template <typename T>
class DivToDo;

class FlatBox;

template <typename T>
class DivContainer : public AbstractDivContainer
{
protected:
	DivFactory<T>	*_factory;
/*	Vec2			_dimensions;
	FlatBox			_format;	//< the down left is the down left corner of the container, and the up right is downleft + dimension
	FlatBox			_zone;		//< the zone covered by the container*/
public:
//	DivContainer(int x, int y, Vec2 dimensions,const DivFactory &factory);
	DivContainer(const agl::Vec2 &dimensions, DivFactory<T> *factory);
//	DivContainer(const Vec2 &dimensions);
	virtual ~DivContainer(void);

/*	Vec2	getDimensions(void) const;
	FlatBox	getFormat(void) const;
	FlatBox	getZone(void) const;*/

	virtual DivIterator<T>	getAnchor(void) = 0;
	virtual DivIterator<T>	getAnchor(const agl::Vec2 &pos) = 0;
	virtual DivIterator<T>	getAnchor(const agl::Vec2 &pos,
			const DivIterator<T> &anchor) = 0;

	DivGroup<T>	getGroup(const FlatBox &box);
	DivGroup<T>	getGroup(const FlatBox &box, const DivIterator<T> &anchor);
	
	DivSet<T>	*getEmptySet(DivToDo<T> *toDoNew, DivToDo<T> *toDoOld);
	DivSet<T>	*getNewSet(DivToDo<T> *toDoNew, DivToDo<T> *toDoOld, const FlatBox &zone);


};

#endif
