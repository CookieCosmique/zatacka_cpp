/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoArrowNew.cpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivToDo/DivToDoArrowNew.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Entity/Arrow.hpp"
#include "Game/Lines/Lines.hpp"

DivToDoArrowNew::DivToDoArrowNew(Arrow &arrow) : _arrow(arrow) {}

void	DivToDoArrowNew::doTo(ChunkDiv &div)
{
	div.addArrow(&_arrow);

	if(_arrow.isOn())
	{
		_arrow.setLastLinesPosition(div.newLines(_arrow.getLastPosition(), _arrow.getLastPosition(), _arrow.getLastPosition(), &_arrow)->getStart());
		div.resetTimer(&_arrow);
//		_arrow.willPop();
	}
	else
	{
		_arrow.setLastLinesPosition(div.newLines(_arrow.getLastPosition(), _arrow.getLastPosition(), &_arrow)->getStart());
		div.resetTimer(&_arrow);
	}
/*	if(_arrow.getLines())
	{
		logger::sslog(LOG_DEBUG) << "in the if" << logger::endlog();
//		_arrow.setLines(div.newLines(_arrow.getLines()->getEnd(), _arrow.getPosition(), &_arrow));
		div.newLines(_arrow.getLastPosition(), _arrow.getPosition(), &_arrow);
	}
	else
	{
		logger::sslog(LOG_DEBUG) << "in the else" << logger::endlog();
//		_arrow.setLines(div.newLines(_arrow.getPosition(), _arrow.getPosition(), &_arrow));
		div.newLines(_arrow.getPosition(), _arrow.getPosition(), &_arrow);
	}*/
//	logger::sslog(LOG_DEBUG) << "Gud done" << logger::endlog();
//	logger::sslog(LOG_DEBUG) << "New Arrow div : " << div << logger::endlog();
}