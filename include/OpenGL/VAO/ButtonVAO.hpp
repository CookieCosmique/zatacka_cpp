/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ButtonVAO.hpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-12 21:48:22 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:27:00 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUTTON_VAO_HPP
# define BUTTON_VAO_HPP

# include "SquareVAO.hpp"

class	ButtonVAO : public agl::SquareVAO
{
public:
	ButtonVAO(const GLuint program_id, const agl::Vec2 &position,
			const agl::Vec2 &size);
	virtual ~ButtonVAO(void);

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
