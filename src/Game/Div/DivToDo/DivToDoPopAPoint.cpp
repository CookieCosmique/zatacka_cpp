/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoPopAPoint.cpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:13:22 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivToDo/DivToDoPopAPoint.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Entity/Arrow.hpp"
#include "Game/Lines/Lines.hpp"

DivToDoPopAPoint::DivToDoPopAPoint(const agl::Vec2 &newPos, Arrow &arrow) : _arrow(arrow), _newPos(newPos) {}

void	DivToDoPopAPoint::doTo(ChunkDiv &div)
{
//	logger::sslog(LOG_DEBUG) << "Pop a point ! " << div << logger::endlog();
	if(_arrow.isOn())
	{
		_arrow.setLastLinesPosition(div.newLines(_newPos, &_arrow)->getStart());
		div.resetTimer(&_arrow);
	}
}

