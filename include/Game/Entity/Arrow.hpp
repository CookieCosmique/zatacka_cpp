/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Arrow.hpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-31 06:01:12 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:40:06 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARROW_HPP
# define ARROW_HPP

# include "OpenGL/ShaderProgram/ArrowShaderProgram.hpp"
# include "OpenGL/VAO/ArrowVAO.hpp"
# include "Game/Entity/Entity.hpp"
# include "Utility/FlatBox.hpp"
# include "Game/Collisions/Shape/Triangle.hpp"
# include "Game/Collisions/ColliderComponent/ColliderComponent.hpp"

template <typename T>
class DivSet;

class ChunkDiv;
class FlatBox;
class Lines;

class	Arrow : public Entity
{
private:
	ArrowShaderProgram		&_shaderProgram;
	ArrowVAO				*_vao;

	agl::Vec2				_direction;

	float					_speed;
	float					_speed_min;
	float					_speed_max;
	int						_acceleration;

	float					_rotation;
	int						_rotationState;

	unsigned int			_color;
	unsigned int			_size;

	int						_skin;
	int						_num;
	
	DivSet<ChunkDiv>		*_divSet;
	FlatBox					_grossoModdo;
	agl::Vec2				_lastPosition;
	agl::Vec2				_lastLinesPosition;
//	Lines					*_lastLines;
	float					_sinceLast;
	float					_traveled;

	bool					_willPop;
	bool					_willPopBefore;
	bool					_wontNext;

	bool					_hole;
	float					_holeSize;
	float					_holeDist;
	float					_lastHole;

	Triangle				_triangle;
	ColliderComponent		_colliderComponent;

	void					popPoint(void);
public:
	Arrow(Game &game, agl::Camera &camera, ArrowShaderProgram &shader_program);
	virtual ~Arrow(void);

	virtual void	render(void) const;
	virtual void	update(const agl::GameTime &game_time);

	void			startRotationRight(void);
	void			startRotationLeft(void);
	void			stopRotationRight(void);
	void			stopRotationLeft(void);
	void			startSpeed(void);
	void			stopSpeed(void);

	void			startHole(void);
	void			stopHole(void);

	void			setLastLinesPosition(const agl::Vec2 &newPos);

	void			willPop(void);

	const agl::Vec2			&getDirection(void) const;
	const agl::Vec2			&getLastPosition(void) const;
	const agl::Vec2			&getLastLinesPosition(void) const;
	ColliderComponent	&getColliderComponent(void);

	bool			isOn(void) const;
//	Lines			*getLines(void) const;
	int				getNum(void) const;

//	void			setLines(Lines *lines);

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
