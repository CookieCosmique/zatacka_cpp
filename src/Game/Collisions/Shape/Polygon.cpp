/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Polygon.cpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:07:59 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:07:59 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Shape/Polygon.hpp"

Polygon::Polygon(void) : _collider(*this)
{
}

Polygon::~Polygon(void)
{
}

const Collider		&Polygon::getCollider(void) const
{
	return (this->_collider);
}

void				Polygon::collide(const Collider &c) const
{
	c.collide(*this);
}
