/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LinesMemory.hpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:55:03 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINESMEMORY_HPP
# define LINESMEMORY_HPP

# include "Vec2.hpp"
# include "Utility/SmartArray.hpp"

class MemIterator;
class LinkIterator;
class LinesContainer;

class LinesMemory
{
private:
	SmartArray<float>			_posData;
	SmartArray<int>				_numData;
	SmartArray<GLuint>			_indexData;

	LinesContainer				&_container;
public:
	LinesMemory(LinesContainer &container);
	~LinesMemory(void);

	MemIterator		request(const agl::Vec2 &pos, int num);
	LinkIterator	link(MemIterator &start, MemIterator &middle, MemIterator &end);

	void			free(MemIterator &ite);
	void			free(LinkIterator &ite);

	float			*getPosData(void) const;
	int				*getNumData(void) const;
	GLuint			*getIndexData(void) const;

	size_t			getSizePosData(void) const;
	size_t			getSizeNumData(void) const;
	size_t			getSizeIndexData(void) const;

	friend std::ostream	&operator<<(std::ostream &o, const LinesMemory &mem);
};

#endif
