/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MenuMainArea.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 16:51:43 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/01 18:52:29 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Menu/MenuMainArea.hpp"
#include "Scene/Interface/Button.hpp"
#include "Logger.hpp"
#include "Menu/Menu.hpp"

using namespace agl;

// Constructor - Destructor

MenuMainArea::MenuMainArea(Menu &menu, Inputs &inputs) :
	Area(menu, NULL,
			Vec2(menu.getCamera().getPosition().x +
				menu.getCamera().getSize().x * 0.3f,
				menu.getCamera().getPosition().y +
				menu.getCamera().getSize().y * 0.15f),
			Vec2(menu.getCamera().getSize().x * 0.4f,
				menu.getCamera().getSize().y * 0.4f))
{
	Button<Menu>	*button1 = new Button<Menu>(menu, this,
			this->_buttonShaderProgram, inputs, menu, NULL, &Menu::play,
			Vec2(0.5f, 1.0f), Vec2(1.0f, 0.25f),
			SceneComponent::PositionType::POS_RATIO,
			SceneComponent::PositionType::POS_RATIO);
	Button<Menu>	*button2 = new Button<Menu>(menu, this,
			this->_buttonShaderProgram, inputs, menu, NULL, &Menu::options,
			Vec2(0.5f, 0.5f), Vec2(1.0f, 0.25f),
			SceneComponent::PositionType::POS_RATIO,
			SceneComponent::PositionType::POS_RATIO);
	Button<Menu>	*button3 = new Button<Menu>(menu, this,
			this->_buttonShaderProgram, inputs, menu, NULL, &Menu::quit,
			Vec2(0.5f, 0.0f), Vec2(1.0f, 0.25f),
			SceneComponent::PositionType::POS_RATIO,
			SceneComponent::PositionType::POS_RATIO);

	this->_font = new FontTexture("assets/font.bmp", 8, 8, 0, 255);
//	this->_font = new FontTexture("assets/font2.bmp", 14, 16, '!' - 1, 224);
//	this->_font = new FontTexture("assets/font3.bmp", 8, 8, 0, 255);
//	this->_font = new FontTexture("assets/font4.bmp", 8, 8, 0, 255);
	logger::sslog(LOG_DEBUG) << *this->_font << logger::endlog();
	button1->addComponent(new TextComponent(menu, button1, *this->_font,
				this->_textShaderProgram,
				"Play Game", 0xFFFFFF, 4 | 8, Vec2(0.5f, 0.5f), Vec2(0, 2.0f / 3.0f),
				SceneComponent::PositionType::POS_RATIO,
				SceneComponent::PositionType::POS_RATIO));
	button2->addComponent(new TextComponent(menu, button2, *this->_font,
				this->_textShaderProgram,
				"Options", 0xEEEEEE, 0, Vec2(0.5f, 0.5f), Vec2(0, 2.0f / 3.0f),
				SceneComponent::PositionType::POS_RATIO,
				SceneComponent::PositionType::POS_RATIO));
	button3->addComponent(new TextComponent(menu, button3, *this->_font,
				this->_textShaderProgram,
				"Quit", 0xFFFFFF, 4, Vec2(0.5f, 0.5f), Vec2(0, 2.0f / 3.0f),
				SceneComponent::PositionType::POS_RATIO,
				SceneComponent::PositionType::POS_RATIO));

	this->addComponent(button1);
	this->addComponent(button2);
	this->addComponent(button3);
}

MenuMainArea::~MenuMainArea(void)
{
	delete this->_font;
}

// Member Functions

void		MenuMainArea::onModif(void)
{
//	logger::sslog(LOG_INFO) << *this << logger::endlog();
}

// Print

void		MenuMainArea::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "MenuMainArea" << C_RESET << " : {" << logger::tab(1);
	Area::objectPrint(o);
	o << logger::tab(-1) << "}";
}
