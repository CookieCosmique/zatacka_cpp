/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDo.hpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:57:55 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVTODO_HPP
# define DIVTODO_HPP

template <typename T>
class DivGroup;

template <typename T>
class DivToDo
{
public:
	virtual ~DivToDo(void) {};

	virtual void doTo(T &div) = 0;

	void		forEach(DivGroup<T> grp);
};

#endif
