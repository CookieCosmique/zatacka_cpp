/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Shape.hpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:11:08 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:37:39 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHAPE_HPP
# define SHAPE_HPP

#include "Utility/FlatBox.hpp"

class Polygon;
class Circle;
class Triangle;
class Rectangle;
class EmptyShape;

class Collider;

class Shape
{
protected:
	Shape	*_stretched;
public:
	Shape(void);
	virtual	~Shape(void);
	virtual const Collider	&getCollider(void) const = 0;
	virtual bool			collide(const Collider &c) const = 0;

	virtual FlatBox			getZone(void);
	virtual const Shape		&stretch(const agl::Vec2 &delta);
};

#endif
