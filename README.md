# README #

### What is this repository for? ###

* Tests for game Zatacka in C using OpenGL
* Gamed based on original Zatacka and Curve fever games. Each player move and arrow that draw a line behind it. When the arrow collides with any section of line, the player dies. In this program the player will bounce and not die.
* More options, Weapons, Bonuses, Maps, Game modes and online mode will be added
* TODO: Clean, Collisions, Background, Menu, Interface

### How do I get set up? ###

* Works on environments Cygwin and Darwin
* Makefile
* 'make doc' : create doc using doxygen (incomplete)
* If libglfw3.a or libglew32.a does not work, download it on the official website

### How do i execute the game ? ###

* /Zatacka [-adft]
* **arguments** :
*   a : anti-aliasing
*   d : more debug messages
*   f : fix fps to 60
*   t : show time before messages
* **controls** :
*   Player : arrows
*   Manual camera: W,A,S,D,Z,X (qwerty)
*   Quit: Escape

### Videos ###

* https://youtu.be/VGBl3iBsnWk
* https://youtu.be/ix4CRHfhxVc

### Screenshots ###

![share.png](https://bitbucket.org/repo/Lg9zyj/images/3708824248-share.png)
![Share2.png](https://bitbucket.org/repo/Lg9zyj/images/1390157473-Share2.png)
![Share3.png](https://bitbucket.org/repo/Lg9zyj/images/529936617-Share3.png)
![share4.png](https://bitbucket.org/repo/Lg9zyj/images/3809225956-share4.png)