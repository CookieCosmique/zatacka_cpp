/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Lines.hpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:08:34 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINES_HPP
# define LINES_HPP

# include "Game/Lines/LinesMemory/LinkIterator.hpp"
# include "Game/Lines/LinesMemory/MemIterator.hpp"
# include "GameTime.hpp"
# include <list>

class LinesContainer;
class Lines;

class AbstractLines
{
protected:
	Lines *_lines;
public:
	AbstractLines(Lines *lines) {_lines = lines;}
	Lines	*getLines(void) const {return _lines;}
};

class PreviousLines : public AbstractLines
{
public:
	PreviousLines(Lines *lines) : AbstractLines(lines) {}
};

class NextLines : public AbstractLines
{
public:
	NextLines(Lines *lines) : AbstractLines(lines) {}
};

class Lines
{
private:
	LinesContainer				*_container;
	std::list<Lines*>::iterator	_posInAll;
	Lines	*_previous;
	Lines	*_next;

	MemIterator	_outerStart;	
	MemIterator	_start;	
	MemIterator	_end;	
	MemIterator	_outerEnd;	

	LinkIterator	_startLink;
	LinkIterator	_endLink;

	int				_playerNum;
	void			*_parentKey;

	double			_timer;

public:
	Lines(LinesContainer *container, const agl::Vec2 &start, const agl::Vec2 &end, int num, void *parent);
	Lines(LinesContainer *container, const agl::Vec2 &outerStart, const agl::Vec2 &start, const agl::Vec2 &end, int num, void *parent);
	Lines(LinesContainer *container, const agl::Vec2 &newPos, const PreviousLines &previous);
	Lines(LinesContainer *container, const agl::Vec2 &newPos, const NextLines &next);
//	Lines(LinesContainer *container, const agl::Vec2 &start, const agl::Vec2 &end, int num, const PreviousLines &previous,const NextLines &next);
	~Lines(void);

	bool			isFirst(void) const;
	bool			isLast(void) const;
	LinesContainer	*getContainer(void) const;
	agl::Vec2		getStart(void) const;
	agl::Vec2		getEnd(void) const;
	int				getNum(void) const;
	void			*getParentKey(void) const;
	double			getTimer(void) const;
	
	void	moveStart(const agl::Vec2 &newPos);
	void	moveEnd(const agl::Vec2 &newPos);

	void	noMoreNext(void);
	void	noMorePrevious(void);
	void	resetTimer(void);

};

#endif
