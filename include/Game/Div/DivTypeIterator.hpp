/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivTypeIterator.hpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVTYPEITERATOR_HPP
# define DIVTYPEITERATOR_HPP

template <typename T>
class DivTypeIterator
{
public:
	virtual ~DivTypeIterator(void) {};
	virtual	void	right(void) = 0;
	virtual	void	left(void) = 0;
	virtual	void	up(void) = 0;
	virtual	void	down(void) = 0;

	virtual	void	rightN(const int n) = 0;
	virtual	void	leftN(const int n) = 0;
	virtual	void	upN(const int n) = 0;
	virtual	void	downN(const int n) = 0;

	virtual T		&getDiv(void) = 0;

	virtual DivTypeIterator<T>	*copy(void)  = 0;
};

#endif