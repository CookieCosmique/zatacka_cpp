#version 400 core

in vec3		in_color1;
in vec3		in_color2;
in vec2		in_origin;
in vec2		in_pos;
in float	in_size;

out vec4	out_color;

uniform	int	skin;

vec3	get_color_neon(vec2 pos, vec3 color1, vec3 color2)
{
	/*	
	float	ld;
	float	up;
	float	dist;

	ld = 0.07f;
	up = 0.10f;
	dist = -pos.x;

	if (dist >= up)
		return (color2);
	else if (dist >= up + ld)
		return (mix(color2, color1, (up + ld - dist) / ld));
		else
	return (color1);
	*/
	return (-pos.x >= 0.1f ? color2 : color1);
}

vec3	get_color_normal(void)
{
	return (in_color1);
}

void	main(void)
{
	if (skin == 0)
		out_color = vec4(get_color_normal(), 1.0f);
	else if (skin == 1)
		out_color = vec4(get_color_neon((in_origin - in_pos) / in_size, in_color1, in_color2), 1.0f);
}