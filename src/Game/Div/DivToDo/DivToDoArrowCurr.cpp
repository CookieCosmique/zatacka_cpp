/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivToDoArrowCurr.cpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/DivToDo/DivToDoArrowCurr.hpp"
#include "Game/Div/ChunkDiv.hpp"
#include "Game/Lines/Lines.hpp"
#include "Game/Entity/Arrow.hpp"

DivToDoArrowCurr::DivToDoArrowCurr(Arrow &arrow) : _arrow(arrow) {}

void	DivToDoArrowCurr::doTo(ChunkDiv &div)
{
	if(_arrow.isOn())
	{
		if(!div.moveLines(_arrow.getPosition(), &_arrow))
		{
			_arrow.setLastLinesPosition(div.newLines(_arrow.getLastPosition(), _arrow.getPosition(), &_arrow)->getStart());
			div.resetTimer(&_arrow);
			//the last lines doesnt exist : create a new one !
		}
		else
		{
			div.resetTimer(&_arrow);
		}
	}

	//move every last lines
//	logger::sslog(LOG_DEBUG) << "Doing stuff arrow : " << div << logger::endlog();
}