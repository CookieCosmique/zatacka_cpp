/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TriangleCollider.cpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:57:51 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:57:51 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Collider/TriangleCollider.hpp"

TriangleCollider::TriangleCollider(const Triangle &t) : _t(t)
{
}

TriangleCollider::~TriangleCollider(void)
{
}

/*bool		TriangleCollider::collide(const Polygon &p) const
{
	collision::collision(this->_t, p);
}

bool		TriangleCollider::collide(const Circle &c) const
{
	collision::collision(this->_t, c);
}*/

bool		TriangleCollider::collide(const Triangle &t) const
{
	return (collision::collision(this->_t, t));
}

bool		TriangleCollider::collide(const Rectangle &t) const
{
	return (collision::collision(this->_t, t));
}

bool		TriangleCollider::collide(const EmptyShape &t) const
{
	return (collision::collision(this->_t, t));
}
