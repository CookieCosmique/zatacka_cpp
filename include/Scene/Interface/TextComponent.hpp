/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TextComponent.hpp                               |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-18 17:06:11 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:27:16 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXT_COMPONENT_HPP
# define TEXT_COMPONENT_HPP

# include "SceneComponent.hpp"
# include "Renderable.hpp"
# include "FontTexture.hpp"
# include "OpenGL/ShaderProgram/TextShaderProgram.hpp"
# include "OpenGL/VAO/TextVAO.hpp"

class	TextComponent : public agl::SceneComponent, public agl::Renderable
{
private:
	const agl::FontTexture	&_font;
	TextShaderProgram	&_shaderProgram;
	TextVAO				*_vao;
	int					_emphasis;
	int					_color;

protected:
	void				onModif(void);

public:
	TextComponent(agl::Scene &scene, agl::Area *area,
			const agl::FontTexture &font,
			TextShaderProgram &shaderProgram,
			const std::string &str, const int color, const int emphasis,
			const agl::Vec2 &position, const agl::Vec2 &size,
			const PositionType pos_type, const PositionType size_type,
			const int level = 4);
	virtual ~TextComponent(void);

	void				render(void) const;

	void				setString(const std::string &str);

	virtual void		objectPrint(std::ostream &o) const;
};

#endif
