/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   PolygonCollider.hpp                             |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 21:11:51 by Bastien             |\________\          */
/*   Updated: 2016-03-22 21:11:51 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef POLYGON_COLLIDER_HPP
# define POLYGON_COLLIDER_HPP

# include "Game/Collisions/Collider/Collider.hpp"

class	PolygonCollider : public Collider
{
private:
	const Polygon	&_p;
public:
	PolygonCollider(const Polygon &p);
	virtual	~PolygonCollider(void);
	virtual void	collide(const Polygon &p) const;
	virtual void	collide(const Circle &c) const;
	virtual void	collide(const Triangle &t) const;
};

#endif
