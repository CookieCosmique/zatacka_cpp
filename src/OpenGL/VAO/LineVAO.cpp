/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LineVAO.cpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-24 19:13:52 by Bastien             |\________\          */
/*   Updated: 2016/06/04 16:50:38 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.hpp"
#include "OpenGL/VAO/LineVAO.hpp"
#include "Game/Lines/LinesMemory/LinesMemory.hpp"

using namespace agl;

// Constructor - Destructor

LineVAO::LineVAO(const GLuint program_id, const LinesMemory &memory)
{
	this->_vbo_pos = loadVBO(program_id, sizeof(GLfloat) * memory.getSizePosData(),
			memory.getPosData(), GL_FLOAT, 1, "position", 2, GL_DYNAMIC_DRAW);
	this->_vbo_id = loadVBO(program_id, sizeof(GLint) * memory.getSizeNumData(),
			memory.getNumData(), GL_INT, 2, "id", 1, GL_DYNAMIC_DRAW);
	this->_ebo = loadEBO(sizeof(GLuint) * memory.getSizeIndexData(), memory.getIndexData(),
			 GL_DYNAMIC_DRAW);
	this->_size = memory.getSizeIndexData();
}

LineVAO::~LineVAO(void)
{

}

// Member functions

void	LineVAO::updatePos(const LinesMemory &memory)
{
	this->updatePos(memory.getPosData(), memory.getSizePosData());
}

void	LineVAO::updatePos(const float *data, const size_t size)
{
	updateBufferData(GL_ARRAY_BUFFER, this->_vbo_pos, sizeof(GLfloat) * size, data);
}

void	LineVAO::updateNum(const LinesMemory &memory)
{
	this->updateNum(memory.getNumData(), memory.getSizeNumData());
}

void	LineVAO::updateNum(const int *data, const size_t size)
{
	updateBufferData(GL_ARRAY_BUFFER, this->_vbo_id, sizeof(GLint) * size, data);
}

void	LineVAO::updateIndex(const LinesMemory &memory)
{
	this->updateIndex(memory.getIndexData(), memory.getSizeIndexData());
}

void	LineVAO::updateIndex(const GLuint *data, const size_t size)
{
	updateBufferData(GL_ELEMENT_ARRAY_BUFFER, this->_ebo, sizeof(GLuint) * size, data);
	this->_size = size;
}

void	LineVAO::updateAll(const LinesMemory &memory)
{
	updateBufferData(GL_ARRAY_BUFFER, this->_vbo_pos, sizeof(GLfloat) * memory.getSizePosData(),
			memory.getPosData());
	updateBufferData(GL_ARRAY_BUFFER, this->_vbo_id, sizeof(GLint) * memory.getSizeNumData(),
			memory.getNumData());
	updateBufferData(GL_ELEMENT_ARRAY_BUFFER, this->_ebo, sizeof(GLuint) * memory.getSizeIndexData(),
			memory.getIndexData());
	this->_size = memory.getSizeIndexData();
}

void	LineVAO::render(void) const
{
	this->bind();
	glDrawElements(GL_TRIANGLES, this->_size, GL_UNSIGNED_INT, 0);
}

// Print

void	LineVAO::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "LineVAO" << C_RESET << ": { " << logger::tab(1);
	o << C_VAR << "vbo_pos" << C_RESET << " = " << this->_vbo_pos << ", ";
	o << C_VAR << "vbo_id" << C_RESET << " = " << this->_vbo_id << ", ";
	o << C_VAR << "ebo" << C_RESET << " = " << this->_ebo << ", ";
	o << C_VAR << "size" << C_RESET << " = " << this->_size << ",\n";
	VAO::objectPrint(o);
	o << logger::tab(-1) << " }";
}
