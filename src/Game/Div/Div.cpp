/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Div.cpp                                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 17:05:45 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Div/Div.hpp"
#include "Game/Div/DivContainer.hpp"
#include "Game/Div/DivContainer/AbstractDivContainer.hpp"
#include "Logger.hpp"

/*Div::Div(AbstractDivContainer &container, const Vec2 &downLeft) : _container(container)
{
	_zone = _container->getFormat() + ...;
}*/

Div::Div(const FlatBox &zone)
{
	_zone = zone;
}

FlatBox			Div::getZone(void)
{
	return (_zone);
}

void	Div::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Div" << C_RESET << " : " << " { " << C_VAR << "zone" << C_RESET << " : " << _zone << "}";
}
