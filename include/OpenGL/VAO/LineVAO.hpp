/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   LineVAO.hpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-24 19:09:00 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:54:02 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINE_VAO_HPP
# define LINE_VAO_HPP

# include "VAO.hpp"

class	LinesMemory;

class	LineVAO : public agl::VAO
{
private:
	GLuint		_vbo_pos;
	GLuint		_vbo_id;
	GLuint		_ebo;
	size_t		_size;

public:
	LineVAO(const GLuint program_id, const LinesMemory &memory);
	~LineVAO(void);

	void			updateAll(const LinesMemory &memory);
	void			updatePos(const LinesMemory &memory);
	void			updatePos(const float *data, const size_t size);
	void			updateNum(const LinesMemory &memory);
	void			updateNum(const int *data, const size_t size);
	void			updateIndex(const LinesMemory &memory);
	void			updateIndex(const GLuint *data, const size_t size);
	virtual void	render(void) const;

	virtual void	objectPrint(std::ostream &o) const;
};

#endif
