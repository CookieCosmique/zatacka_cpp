/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TimerHUD.cpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-21 22:16:03 by Bastien             |\________\          */
/*   Updated: 2016/06/14 17:39:21 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Game/HUD/TimerHUD.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

TimerHUD::TimerHUD(Scene &scene, const FontTexture &font,
		TextShaderProgram &shaderProgram) :
	TextComponent(scene, NULL, font, shaderProgram, "00:00",
			0xFFAAAA, 1 | 4, Vec2(0.5f, 0.95f), Vec2(0, 0.05f),
			SceneComponent::PositionType::POS_RATIO,
			SceneComponent::PositionType::POS_RATIO),
	Updatable(scene.getUpdate(), 1)
{
	this->_last_time = 0;
}

TimerHUD::~TimerHUD(void)
{

}

// Member Functions

void		TimerHUD::update(const GameTime &game_time)
{
	if (game_time.getTime() - this->_last_time >= 1)
	{
		this->setString(game_time.getTimeStr());
		this->_last_time = game_time.getTime();
	}
}

// Print

void		TimerHUD::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "TimerHUD" << C_RESET << ": {" << logger::tab(1);
	TextComponent::objectPrint(o);
	o << "\n";
	Updatable::objectPrint(o);
	o << logger::tab(-1) << "}";
}
