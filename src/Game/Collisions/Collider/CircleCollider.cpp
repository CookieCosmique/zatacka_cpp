/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   CircleCollider.cpp                              |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:08:30 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:08:30 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Collisions/Collider/CircleCollider.hpp"

CircleCollider::CircleCollider(const Circle &c) : _c(c)
{
}

CircleCollider::~CircleCollider(void)
{
}

void		CircleCollider::collide(const Polygon &p) const
{
	collision::collision(this->_c, p);
}

void		CircleCollider::collide(const Circle &c) const
{
	collision::collision(this->_c, c);
}

void		CircleCollider::collide(const Triangle &t) const
{
	collision::collision(this->_c, t);
}
