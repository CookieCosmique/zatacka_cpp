/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TextShaderProgram.cpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 18:07:48 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:23:37 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OpenGL/ShaderProgram/TextShaderProgram.hpp"
#include "Logger.hpp"

using namespace agl;

// Constructor - Destructor

TextShaderProgram::TextShaderProgram(void) throw(InvalidShaderProgramException) :
	CameraShaderProgram(TEXT_VERTEX, TEXT_GEOMETRY, TEXT_FRAGMENT)
{
	this->_id_char_size = glGetUniformLocation(this->_id, "char_size");
	this->_id_font_size = glGetUniformLocation(this->_id, "font_size");
	this->_id_pos = glGetUniformLocation(this->_id, "pos");
	this->_id_size = glGetUniformLocation(this->_id, "size");
	this->_id_emphasis = glGetUniformLocation(this->_id, "emphasis");
}

TextShaderProgram::~TextShaderProgram(void)
{

}

// Member Functions

void		TextShaderProgram::updateFont(const FontTexture &font) const
{
	this->updateCharSize(font.getCharWidth(), font.getCharHeight());
	this->updateFontSize(font.getWidth(), font.getHeight());
}

void		TextShaderProgram::updateCharSize(const float width, const float height) const
{
	glUniform2f(this->_id_char_size, width, height);
}

void		TextShaderProgram::updateFontSize(const float width, const float height) const
{
	glUniform2f(this->_id_font_size, width, height);
}

void		TextShaderProgram::updatePosition(const Vec2 &position) const
{
	glUniform2fv(this->_id_pos, 1, position.getXY());
}

void		TextShaderProgram::updateSize(const float size) const
{
	glUniform1f(this->_id_size, size);
}

void		TextShaderProgram::updateEmphasis(const int emphasis) const
{
	glUniform1i(this->_id_emphasis, emphasis);
}

// Print

void		TextShaderProgram::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "TextShaderProgram" << C_RESET << ": {" << logger::tab(1);
	CameraShaderProgram::objectPrint(o);
	o << ",\n";
	o << C_VAR << "id_char_size" << C_RESET << " = " << this->_id_char_size << ",\n";
	o << C_VAR << "id_font_size" << C_RESET << " = " << this->_id_font_size;
	o << logger::tab(-1) << "}";
}
