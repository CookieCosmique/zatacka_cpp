/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TestEntity.cpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-30 22:28:36 by Bastien             |\________\          */
/*   Updated: 2016-03-30 22:28:36 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#include "Game/Entity/TestEntity.hpp"

TestEntity::TestEntity(Game &game, Camera &camera, const float life) :
		Entity(game, camera, 1, 1), _life(life)
{
	this->_compt = 0;
}

TestEntity::~TestEntity(void)
{
	logger::sslog(LOG_DEBUG) << "Deleting: " << *this << logger::endlog();
}

void	TestEntity::update(const GameTime &game_time)
{
	this->_compt += game_time.getDiff() * game_time.getSpeed();
	if (this->_compt > this->_life)
		kill();
}

void	TestEntity::render(void) const
{

}

void	TestEntity::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "TestEntity" << C_RESET << ": { " << logger::tab(1) <<
			C_VAR << "life" << C_RESET << " = " << this->_life << ", \n" <<
			C_VAR << "compt" << C_RESET << " = " << this->_compt << ", \n";
	Entity::objectPrint(o);
	o << logger::tab(-1) << "}";
}