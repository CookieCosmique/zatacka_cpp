/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   RectangleCollider.hpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-22 23:55:17 by Bastien             |\________\          */
/*   Updated: 2016-03-22 23:55:17 by Bastien              \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef RECTANGLE_COLLIDER_HPP
# define RECTANGLE_COLLIDER_HPP

# include "Game/Collisions/Collider/Collider.hpp"

class	RectangleCollider : public Collider
{
private:
	const Rectangle	&_t;
public:
	RectangleCollider(const Rectangle &t);
	virtual	~RectangleCollider(void);
//	virtual bool	collide(const Polygon &p) const;
//	virtual bool	collide(const Circle &c) const;
	virtual bool	collide(const Triangle &t) const;
	virtual bool	collide(const Rectangle &t) const;
	virtual bool	collide(const EmptyShape &t) const;
};

#endif

