/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ColliderComponent.hpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:38:52 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLLIDERCOMPONENT
# define COLLIDERCOMPONENT

#include "Game/Collisions/Shape/Shape.hpp"
#include "Utility/FlatBox.hpp"
#include <map>

class Actor;

enum COLactive : bool 
{  
	COL_ACTIVE 	= true,
	COl_PASSIVE	= false
};

enum COLmask : int
{
	COL_PLAYER	= 0x0001,
	COL_LINES	= 0x0002
};

class ColliderComponent
{
private:
	int					_type;
	int					_toHit;
	COLactive			_active;
	Shape				&_shape;

	Actor				&_parent;

	std::map< void*, ColliderComponent& > _collided;
public:
	ColliderComponent(Actor &parent, int type, int toHit, COLactive active, Shape &shape);

	void	addType(int	type);
	void	removeType(int type);
	void	setType(int	type);

	void	addToHit(int hit);
	void	removeToHit(int hit);
	void	setToHit(int hit);

	Shape	&getShape(void);
	FlatBox	getZone(void);
	Actor	&getParent(void);

	void	doOnCollision(ColliderComponent &other);
	void	clearCollided(void);
	
	void	collideComponentStretch(ColliderComponent &other, const agl::Vec2 &delta);
	friend void	collideComponent(ColliderComponent &a, ColliderComponent &b);
	friend void	collideComponentWithShape(ColliderComponent &a, ColliderComponent &b, const Shape &shapeA, const Shape &shapeB);
};

#endif
