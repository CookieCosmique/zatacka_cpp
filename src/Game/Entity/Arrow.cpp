/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Arrow.cpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-31 07:43:21 by Bastien             |\________\          */
/*   Updated: 2016/06/01 17:01:15 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cmath>
#include "Game/Entity/Arrow.hpp"
#include "Game/Div/DivContainer.hpp"
#include "Game/Div/DivSet.hpp"
#include "Game/Div/DivToDo/DivToDoArrowCurr.hpp"
#include "Game/Div/DivToDo/DivToDoArrowOld.hpp"
#include "Game/Div/DivToDo/DivToDoArrowNew.hpp"
#include "Game/Div/DivToDo/DivToDoPopAPoint.hpp"
#include "Utility/FlatBox.hpp"
#include "Game/Game.hpp"

using namespace agl;

// Constructor - Destructor

Arrow::Arrow(Game &game, Camera &camera, ArrowShaderProgram &shader_program) :
		Entity(game, camera, 2, 2), _shaderProgram(shader_program),
		_triangle(), _colliderComponent((Actor&) *this, COL_PLAYER, COL_PLAYER | COL_LINES, COL_ACTIVE, (Shape&) _triangle)
{
	this->_position = Vec2(10, 10);
	this->_lastPosition = this->_position;
	this->_lastLinesPosition = this->_position;
	this->_direction = Vec2(1, 1).normalize();
	this->_vao = new ArrowVAO(this->_shaderProgram.getID(), this->_position, this->_direction);
	this->_speed = 150.0f;
	this->_speed_min = this->_speed;
	this->_speed_max = this->_speed * 3.5f;
	this->_acceleration = 0;
	this->_rotation = M_PI * 2.0f;
	this->_rotationState = 0;
	this->_color = 0x0000FF;
	this->_size = 18;
	this->_skin = 0;
	this->_grossoModdo = FlatBox(Vec2(0.0, 0.0), Vec2(25.0,25.0));
	this->_traveled = 0;
	this->_sinceLast = 0;
//	this->_lastLines = NULL;
	this->_num = 1;
	this->_willPop = false;
	this->_willPopBefore = false;
	this->_wontNext = false;

	this->_hole = false;
	this->_holeSize = 50.0f;
	this->_holeDist = 500.0f;
	this->_lastHole = 0;
	
	this->_divSet = game.getContainer().getEmptySet(new DivToDoArrowNew(*this), new DivToDoArrowOld(*this));

	logger::sslog(LOG_DEBUG) << "Arrow created: " << *this << logger::endlog();
}

Arrow::~Arrow(void)
{
	delete this->_vao;
}

// Member functions

void		Arrow::popPoint(void)
{
	if (!this->_hole)
	{
		this->_divSet->forEachCurr(new DivToDoPopAPoint(this->_position, *this));
		this->_sinceLast = this->_traveled;
		this->_lastLinesPosition = this->_position;
	}
}

void		Arrow::render(void) const
{
	this->_shaderProgram.use();
	this->_shaderProgram.updateColor(this->_color);
	this->_shaderProgram.updateSpeed(this->_speed / this->_speed_min);
	this->_shaderProgram.updateSize(this->_size);
	this->_shaderProgram.updateSkin(this->_skin);
	this->_shaderProgram.updateCamera(this->_camera);
	this->_vao->updatePosition(this->_position);
	this->_vao->updateDirection(this->_direction);
	this->_vao->render();
}

void		Arrow::update(const GameTime &game_time)
{
	float		t;
	int			rotation;
	float		dist;

//	if (this->_willPopBefore && !this->_wontNext)
	if (this->_willPopBefore || this->_willPop)
		this->popPoint();
	this->_willPop = false;
	this->_willPopBefore = false;

	this->_lastPosition = this->_position;
	t = game_time.getDiff() * game_time.getSpeed();

	if (this->_acceleration)
		this->_speed += (this->_speed_max - this->_speed) * 3.0f * t; // not about time
	else
		this->_speed -= (this->_speed - this->_speed_min) * 3.0f * t;
	if ((rotation = (-(this->_rotationState & 1) + ((this->_rotationState & 2) >> 1))))
		this->_direction.rotate(rotation * this->_rotation * t);

	this->_position += this->_direction * this->_speed * t;
//	logger::sslog(LOG_FINE) << "FRAME" << logger::endlog();
	this->_traveled += (this->_lastPosition - this->_position).length();

	if (this->_traveled - this->_sinceLast > 5)
		this->_willPop = rotation;

	dist = this->_traveled - this->_lastHole;
	if (this->_hole)
	{
		if (dist >= this->_holeSize)
		{
			this->stopHole();
			this->_lastHole = this->_traveled;
		}
	}
	else
	{
		if (dist >= this->_holeDist)
		{
			this->startHole();
			this->_lastHole = this->_traveled;
		}
	}

	this->_divSet->update(this->_grossoModdo + this->_position);
	this->_wontNext = false;
/*	if (this->_willPop)
	{
		this->popPoint();
		this->_wontNext = true;
	}*/
//	this->_willPop = false;
//	this->_willPopBefore = false;
	this->_divSet->forEachCross(new DivToDoArrowCurr(*this)); // ou curr , je ne suis pas sur ?

}

void		Arrow::startRotationRight(void)
{
	this->_rotationState |= 1;
	this->_willPopBefore = true;
}

void		Arrow::startRotationLeft(void)
{
	this->_rotationState |= 2;
	this->_willPopBefore = true;
}

void		Arrow::stopRotationRight(void)
{
	this->_rotationState &= ~1;
	this->_willPopBefore = true;
}

void		Arrow::stopRotationLeft(void)
{
	this->_rotationState &= ~2;
	this->_willPopBefore = true;
}

void		Arrow::startSpeed(void)
{
	this->_acceleration = 1;
}

void		Arrow::stopSpeed(void)
{
	this->_acceleration = 0;
}

void		Arrow::startHole(void)
{
	if (!this->_hole)
	{
		this->_divSet->forEachCurr(new DivToDoArrowOld(*this));
		this->_hole = true;
	}
}

void		Arrow::stopHole(void)
{
	if (this->_hole)
	{
		this->_divSet->forEachCurr(new DivToDoArrowNew(*this));
		this->_hole = false;
	}
}

void		Arrow::setLastLinesPosition(const Vec2 &newPos)
{
	(void) newPos;
//	this->_lastLinesPosition = newPos;
}

void		Arrow::willPop(void)
{
	this->_willPop = true;
}

// Accessors

const Vec2	&Arrow::getDirection(void) const
{
	return (this->_direction);
}

const Vec2	&Arrow::getLastPosition(void) const
{
	return (this->_lastPosition);
}

const Vec2	&Arrow::getLastLinesPosition(void) const
{
	return (this->_lastLinesPosition);
}

ColliderComponent	&Arrow::getColliderComponent(void)
{
	return (this->_colliderComponent);
}

bool		Arrow::isOn(void) const
{
	return (!this->_hole);
}
/*Lines		*Arrow::getLines(void) const
{
	return (this->_lastLines);
}*/

int			Arrow::getNum(void) const
{
	return (this->_num);
}

/*void		Arrow::setLines(Lines *lines)
{
	this->_lastLines = lines;
}*/


// Print

void		Arrow::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Arrow" << C_RESET << ": { " << logger::tab(1);
	Entity::objectPrint(o);
	o << ",\n";
	o << C_VAR << "shader program" << C_RESET << " = " << this->_shaderProgram << ",\n";
	o << C_VAR << "vao" << C_RESET << " = " << *this->_vao << ",\n";
	o << C_VAR << "position" << C_RESET << " = " << this->_position << ",\n";
	o << C_VAR << "direction" << C_RESET << " = " << this->_direction << ",\n";
	o << C_VAR << "speed" << C_RESET << " = " << this->_speed << ",\n";
	o << C_VAR << "color" << C_RESET << " = " << this->_color << ",\n";
	o << C_VAR << "size" << C_RESET << " = " << this->_size << ",\n";
	o << C_VAR << "skin" << C_RESET << " = " << this->_skin;
	o << logger::tab(-1) << "}";
}
