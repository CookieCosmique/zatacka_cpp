/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   AbstractDivContainer.hpp                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 16:51:44 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ABSTRACTDIVCONTAINER_HPP
# define ABSTRACTDIVCONTAINER_HPP

# include "Utility/FlatBox.hpp"
# include "Game/Div/DivFactory.hpp"

class AbstractDivContainer
{
protected:
	agl::Vec2	_dimensions;
	FlatBox		_format;	//< the down left is the down left corner of the container, and the up right is downleft + dimension
	FlatBox		_zone;		//< the zone covered by the container
public:
	AbstractDivContainer(const agl::Vec2 &dimensions);
	virtual ~AbstractDivContainer(void);

	agl::Vec2	getDimensions(void) const;
	FlatBox		getFormat(void) const;
	FlatBox		getZone(void) const;

};

#endif
