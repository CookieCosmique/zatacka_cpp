/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SceneComponent.hpp                              |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 15:06:41 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:28:49 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCENE_COMPONENT_HPP
# define SCENE_COMPONENT_HPP

# include "Vec2.hpp"
# include "SmartPointer.hpp"
# include "Spawnable.hpp"
# include "Scene.hpp"

class	Area;

class	SceneComponent : public virtual agl::Spawnable
{
public:
	enum	PositionType
	{
		POS_NORMAL,
		POS_RATIO
	};

private:
	agl::Vec2	getRatio(const agl::Vec2 &ratio) const;
	void		setPositionRatio(const agl::Vec2 &position, const agl::Vec2 &size,
			const agl::Vec2 &ratio);

protected:
	Area				*_area;

	agl::Scene			&_scene;

	agl::Vec2			_position;
	agl::Vec2			_size;

	virtual void		onModif(void) = 0;
	void				setPosition(const agl::Vec2 &position,
			const PositionType type);
	void				setSize(const agl::Vec2 &size,
			const PositionType type);

public:
	SceneComponent(agl::Scene &scene, Area *area,
			const agl::Vec2 &position, const agl::Vec2 &size,
			const PositionType pos_type = PositionType::POS_NORMAL,
			const PositionType size_type = PositionType::POS_NORMAL);
	virtual ~SceneComponent(void);

	virtual void		scale(const float coef);
	void				scaleArea(const float coef);
	virtual void		translate(const float x, const float y);
	void				translate(const agl::Vec2 &vec);

	const agl::Vec2			&getPosition(void) const;
	const agl::Vec2			&getSize(void) const;

	virtual void		objectPrint(std::ostream &o) const;
};

#endif
