/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   DivArrayIterator.hpp                            |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016-03-17 12:41:01 by Clhawliet            \|_______| atacka   */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIVARRAYITERATOR_HPP
# define DIVARRAYITERATOR_HPP

#include "Game/Div/DivTypeIterator.hpp"

template <typename T>
class DivContainerArray;

template <typename T>
class DivArrayIterator : public DivTypeIterator<T>
{
private:
	DivContainerArray<T>	&_container;
	int						_row;
	int						_col;
public:
	DivArrayIterator(DivContainerArray<T> &container, int row, int col);
	virtual	~DivArrayIterator(void) {};

	virtual void	right(void);
	virtual void	left(void);
	virtual void	up(void);
	virtual void	down(void);

	virtual void	rightN(const int n);
	virtual void	leftN(const int n);
	virtual void	upN(const int n);
	virtual void	downN(const int n);

	virtual T		&getDiv(void);

	virtual DivTypeIterator<T>	*copy(void);
};

#endif